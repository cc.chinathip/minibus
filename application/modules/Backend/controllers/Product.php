<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function product_all(){	
		$data['title'] = 'สินค้าทั้งหมด';
		$this->load->view('product/product_all');
	}
	
    public function type_all(){	
		$data['title'] = 'รุ่นรถทั้งหมด';
		$this->load->view('product/type_all');
	}

	public function color_all(){	
		$data['title'] = 'สีทั้งหมด';
		$this->load->view('product/color_all');
	}


	public function add_product(){	
		$data['title'] = 'เพิ่มสินค้า';
		
		if(isset($_POST['insert'])){
	
			//exit();
			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				/* 'detail' => $this->input->post('product_detail'),
				'detail_eng' => $this->input->post('product_detail_eng'), */
				'color_product' => implode(",",$this->input->post('product_color')),
				'price' => $this->input->post('price'),
				'qty' => $this->input->post('qty'),
				'img_product' =>Rename_file($_FILES['product_image']['name']),
				'type_product' => $this->input->post('type_product'),
				'created_by' => $this->input->post('created_by'),
				'status' => 'off',
				'Exterior' => $this->input->post('Exterior'),
				'Utility' => $this->input->post('Utility'),
				'Performance' => $this->input->post('Performance'),
				'Safety' => $this->input->post('Safety'),
			);

			$data = $this->db->insert('product',$data);
			
			
			$path = array(
				'upload_path' => './assets/images/product/',
				'redirect_path1' => 'product/add_product_form',
				'redirect_path2' => 'product/product_all',
				'name_image' => Rename_file($_FILES['product_image']['name']),
				'name_input_image' => 'product_image'
			);
			check_upload($path,'insert');

			
		}else{

			$this->load->view('product/add_product_form');
			
		}
		
	}

	public function update_product(){	
		$data['title'] = 'หน้าแก้ไขสินค้า';

		if(isset($_POST['update'])){
			
				if($_FILES['product_image']['name'] !== ''){

					$image =Rename_file($_FILES['product_image']['name']); 

				}else{
					$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
				}
				$data = array(
					'name_thai' => $this->input->post('name_thai'),
					'name_eng' => $this->input->post('name_eng'),
					/* 'detail' => $this->input->post('product_detail'),
					'detail_eng' => $this->input->post('product_detail_eng'), */
					'color_product' => implode(",",$this->input->post('product_color')),
					'price' => $this->input->post('price'),
					'qty' => $this->input->post('qty'),
					'img_product' => $image,
					'type_product' => $this->input->post('type_product'),
					'updated_at' => date('Y-m-d H:i:s'),
					'updated_by' => $this->input->post('updated_by'),
					//'status' => 'on',
					'Exterior' => $this->input->post('Exterior'),
					'Utility' => $this->input->post('Utility'),
					'Performance' => $this->input->post('Performance'),
					'Safety' => $this->input->post('Safety'),
				);

				$this->db->where('id',$this->input->post('id'));
				$data = $this->db->update('product',$data);

				
				
				$path = array(
					'upload_path' => './assets/images/product/',
					'redirect_path1' => 'product/product_all',
					'redirect_path2' => 'product/product_all',
					'name_image' =>  $image,
					'name_input_image' => 'product_image'
				);
				check_upload($path,'update');
				

				
		}else{
			$this->load->view('product/update_product_form');
		}

	}
    public function add_type(){	
		$data['title'] = 'เพิ่มรุ่นรถ';

		if(isset($_POST['insert'])){

			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				//'product_image' => $_FILES['product_image']['name'],
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);

			$data = $this->db->insert('type',$data);
			
			if($data AND $data == 1){
				
				$data = array('insert' => 'success');
				$this->load->view('product/type_all',$data);

			}else{	
				
				redirect('Backend/product/add_type','refresh');
				
				
			}
		}else{

			$this->load->view('product/add_type_form');
			
		}
		
	}


	public function update_type(){	
		$data['title'] = 'หน้าแก้ไขประเภทสินค้า';
		$this->load->view('product/update_type_form');

	}

	public function add_color(){	
		$data['title'] = 'เพิ่มสีสินค้า';
		
		if(isset($_POST['insert'])){

			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'img_color' =>Rename_file($_FILES['color_image']['name']),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);

			$data = $this->db->insert('color',$data);
			
			
			$path = array(
				'upload_path' => './assets/images/color/',
				'redirect_path1' => 'product/add_color_form',
				'redirect_path2' => 'product/color_all',
				'name_image' => Rename_file($_FILES['color_image']['name']),
				'name_input_image' => 'color_image'
			);
			check_upload($path,'insert');

			
		}else{

			$this->load->view('product/add_color_form');
			
		}
		
	}

	public function update_color(){	
		$data['title'] = 'หน้าแก้ไขสีสินค้า';

		if(isset($_POST['update'])){
			
				if($_FILES['color_image']['name'] !== ''){

					$image =Rename_file($_FILES['color_image']['name']); 

				}else{
					$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
				}
				$data = array(
					'name_thai' => $this->input->post('name_thai'),
					'name_eng' => $this->input->post('name_eng'),
					'img_color' => $image,
					'updated_at' => date('Y-m-d H:i:s'),
					'updated_by' => $this->input->post('updated_by'),
					//'status' => 'on',
				);

				$this->db->where('id',$this->input->post('id'));
				$data = $this->db->update('color',$data);

				
				
				$path = array(
					'upload_path' => './assets/images/color/',
					'redirect_path1' => 'product/color_all',
					'redirect_path2' => 'product/color_all',
					'name_image' =>  $image,
					'name_input_image' => 'color_image'
				);
				check_upload($path,'update');
				

				
		}else{
			$this->load->view('product/update_color_form');
		}

	}

	
}
