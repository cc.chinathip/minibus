<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index(){	
		$data['title'] = 'เข้าสู่ระบบ';
		$this->load->view('admin/login');
	}

	public function Dashboard(){	

		$data['title'] = 'หน้าหลัก';
		$this->load->view('index');
		
	}
	public function register(){	
		$data['title'] = 'สร้างรหัสเข้าใช้งานระบบหลังบ้าน';
		$this->load->view('admin/register_form');
	}

	public function update_profile(){	
		$data['title'] = 'แก้ไขข้อมูลส่วนตัว';
		$this->load->view('admin/update_profile_form');
	}

	public function update_password(){	
		$data['title'] = 'แก้ไขรหัสผ่าน';
		$this->load->view('admin/update_password_form');
	}

	public function data_all(){	
		$data['title'] = 'แอดมินทั้งหมด';
		$this->load->view('admin/admin_all');
	}
	

	public function check_login(){	
		//echo $_POST['password'];

		if(isset($_POST['username']) AND isset($_POST['password']) AND isset($_POST['type_user'])){

			$data = $this->Users_model->username_password_data($this->input->post('username'),$this->input->post('password'),$this->input->post('type_user'));

			
			//print_r($data);

			if($data){

				foreach($data as $data_oj){

					
					 $data['result'] = $this->session->set_userdata('session_userid', $data_oj->id);
					 $data['name_admin'] = $this->session->set_userdata('session_name_admin', $data_oj->name.' '.$data_oj->surname);
					 $data = array('login' => 'success');
				
				}

			}else{

				 $data['result'] = 'not found data';

			}

		}else{

			 $data['result'] = 'not found data';
		}
		
		$this->load->view('index',$data);
	}

	public function logout(){	
		unset($_SESSION['session_userid']);
		//$this->load->view('../views/index');
		$this->load->view('admin/login');
		
	}

	
}
