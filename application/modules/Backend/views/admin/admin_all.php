<?= $this->load->view('section/header'); ?>
  <!-- Page Wrapper -->
  <div id="wrapper">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
					<h1 class="h3 mb-2 text-gray-800">รายการแอดมิน</h1>
					<p class="mb-4"><a href="<?= base_url('backend/Admin/register'); ?>" class="btn btn-primary btn-user btn-block btn_add_kind" >สร้างรหัสเข้าใช้งาน</a></p>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
					<thead>
						<tr>
							<th>ลำดับ</th>
							<th>ชื่อ</th>
							<th>ชื่อผู้ใช้</th>
							<th>วันที่สร้าง</th>
						</tr>
					</thead>
					<tfoot>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ</th>
									<th>ชื่อผู้ใช้</th>
									<th>วันที่สร้าง</th>
								</tr>
						</tfoot>
   			 </table>

              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  
  <script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		//console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'POST',
				async:false,
				url:  api_url+'api/Users/data_user_all',
				data: {type_user:'admin'},
				xhrFields: {
				withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data)
					
				
					var html = '<tbody><tr>';
					 $.each(data.data, function(index,value ) {
					
						html += '<td>'+parseInt(index + 1)+'</td>';
						html += '<td>'+value.name+'</td>';
						html += '<td>'+value.username+'</td>';
						html += '<td>'+value.created_at+'</td></tr>';
						
					}); 
					html += '</tbody>';
					$('#dataTable').append(html);

					
					
				}
			}); 

			



			

		
	});

  </script>
  <?= $this->load->view('section/footer'); ?>
