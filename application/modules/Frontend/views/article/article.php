<?= $this->load->view('section/header'); ?>

<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Users/index'); ?>">หน้าหลัก</a></li>
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/News/news_list'); ?>">บทความและVDO</a></li>
		    <li class="breadcrumb-item active"><span>บทความและVDO</span></li> 
	  	</ol>
	</div>
</nav>
 
<div class="section section-article main pt-0">
	<div class="page-header wow fadeIn">
		<div class="container">
			<h2 class="title-xl">บทความและVDO</h2>
		</div>
	</div> 
 	<div class="page-header-info wow fadeIn">
		<div class="container">
			<p>
				อัพเดตเรื่องราว บทความต่างๆที่น่าสนใจ ความเคลื่อนไหวของเรา<br>
				ให้คุณได้ทราบทั้งหมด
			</p>
		</div>
	</div>  
  
	<div class="container"> 
		<div class="row title-group">
			<div class="col-12 left"><h2 class="title-md">บทความและVDO</h2></div> 
		</div>
		<div class="row space-0 wow fadeIn show_article">
			 
        </div><!--row-->
       <!--  <div class="col-lg-12">
				<nav class="pagination-box">
				  <ul class="pagination justify-content-center">
				    <li class="page-item"><a class="page-link arrow page-first" href="#" ><span class="icons icon-page-first"></span></a></li>
				    <li class="page-item"><a class="page-link arrow page-prev" href="#" ><span class="icons icon-page-prev"></span></a></li>
				    
				    <li class="page-item"><a class="page-link active" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item"><a class="page-link" href="#">4</a></li>
				    <li class="page-item"><a class="page-link" href="#">5</a></li>
				    <li class="page-item"><a class="page-link" href="#">6</a></li>
				    <li class="page-item"><a class="page-link dotdotdot" href="#">...</a></li>
				    <li class="page-item"><a class="page-link last" href="#">25</a></li>

				    <li class="page-item"><a class="page-link arrow page-next" href="#" ><span class="icons icon-page-next"></span></a></li>
				    <li class="page-item"><a class="page-link arrow page-last" href="#" ><span class="icons icon-page-last"></span></a></li>
				  </ul>
				</nav>
		</div> -->
	</div><!--container--> 
 
</div><!--section-article-->

<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Article/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.article)
					console.log('total',data.total)
					//console.log('datasssss',data.data.article[0].status)

					var container = $(".show_article");//positon show form
					var sources = (function() {
						var result = [];
								for (var i = 1; i < data.data.article.length; i++) {
									result.push(data.data.article[i]);
												
								}

								console.log('resultresult',result)
								return result;
					})();
					var options = {
						dataSource: sources,
						pageSize: 6, // total per page
						callback: function(response, pagination) {
						window.console && console.log(response, pagination);
						var html = '';
					 $.each(response, function(index,value ) {
						
						console.log('index',parseInt(index+1))
						
							if(value.link_youtube !=='' && value.link_youtube !== undefined){

								var link_a = 'data-fancybox href="'+value.link_youtube+'"';
							}else{

								var link_a = 'href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+'"';
							}

							html += '<div class="block_article col-lg-4 col-sm-6" >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo '+(value.link_youtube !=='' && value.link_youtube !== undefined ? 'video' : '') +'">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/article/') ?>'+value.img_article+');" '+link_a+'>'
									html += '<img class="image_size" src="<?= base_url('/assets/images/article/') ?>'+value.img_article+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-6">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

											html += '<div class="col-6">'
												html += '<div class="share-wrap">'
												html += '<span class="icons icon-share"></span>'
												html += '<span class="text">แชร์ไปยัง : </span>'
												html += '<a class="icons icon-share-facebook" href="#"></a> '
												html += '<a class="icons icon-share-line" href="#"></a>'
												html += '</div>'
											html += '</div>'

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'

					});
						container.prev().html(html);
				  }

				};

									container.addHook("beforeInit", function() {
										window.console && console.log("beforeInit...");
									});
									container.pagination(options);

									container.addHook("beforePageOnClick", function() {
										window.console && console.log("beforePageOnClick...");
									});
					
					
					//$('.show_article').append(html);
					
					
				}
			}); 

			
		
	});

  </script>
