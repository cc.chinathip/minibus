/* function preview_image_multiple(input, placeToInsertImagePreview) {
	//console.log("index", index);
	if (input.files && input.files[0]) {
		var filesAmount = input.files.length;

		for (i = 0; i < filesAmount; i++) {
			console.log("i", i);
			var reader = new FileReader();
			reader.onload = function(e) {
				//console.log("ppp", e.target.result);
				$($.parseHTML("<img class='image_preview'>"))
					.attr("src", e.target.result)
					.appendTo(placeToInsertImagePreview);
			};
			reader.readAsDataURL(input.files[i]);
			//console.log("image name", input.files[i]);
		}
	}
} */

$("img#target").attr("src", "");

function checkTypePDF(inputfile) {
  const file = inputfile;
  const fileType = file["type"];
  var validImageTypes = ["application/pdf"];

  //console.log("invalid file", inputfile.type);

  if (!validImageTypes.includes(fileType)) {
    // invalid file type code goes here.
    //console.log("invalid file");
    Swal.fire({
      title: "error!",
      text: "ไฟล์ใช้ได้เฉพาะนามสกุล pdf เท่านั้น",
      type: "error"
    });

    $(".image_preview").attr("src", "");

    $(".name_file").hide();
  } else {
    $(".name_file").show();
  }
}

function checkTypeImage(inputfile) {
  const file = inputfile;
  const fileType = file["type"];
  var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];

  //console.log("invalid file", inputfile.type);

  if (!validImageTypes.includes(fileType)) {
    // invalid file type code goes here.
    //console.log("invalid file");
    Swal.fire({
      title: "error!",
      text: "ไฟล์ใช้ได้เฉพาะนามสกุล jpg,jpeg,png เท่านั้น",
      type: "error"
    });

    $(".image_preview").attr("src", "");
  }
}
function setEmptySrcImg(ele_id) {
  $("img#" + ele_id + "").attr("src", "");
}
function preview_image_one(input, pdf = null) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#target").attr("src", e.target.result);
      //console.log("image name", input.files[0].name);

      //console.log("pp", input.files[0].type);

      if (pdf === "usepdf") {
        checkTypePDF(input.files[0]);
        $(".name_file").html(input.files[0].name);
      } else {
        checkTypeImage(input.files[0]);
      }
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function preview_image_new(input, ele_preview, pdf = null) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#" + ele_preview + "").attr("src", e.target.result);
      //console.log("image name", input.files[0].name);

      if (pdf === "usepdf") {
        checkTypePDF(input.files[0]);
        $(".name_file").html(input.files[0].name);
      } else {
        checkTypeImage(input.files[0]);
      }
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function delete_data_row(class_btn_del, controller_api, apikey, api_url) {
  $(class_btn_del).click(function() {
    var dataId = $(this).data("id");

    Swal.fire({
      title: "ยืนยันที่จะลบข้อมูลนี้หรือไม่",
      text: "ยืนยันลบข้อมูล!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก"
    }).then(result => {
      if (result.value == true) {
        $.ajax({
          //cache: true,
          type: "POST",
          async: false,
          url: api_url + "api/" + controller_api + "/delete",
          data: { id: dataId },
          xhrFields: {
            withCredentials: false
          },
          headers: {
            "X-Api-Key": apikey
          },
          success: function(data) {
            console.log("data2", data);
            if (data.status === true) {
              Swal.fire("ลบข้อมูลสำเร็จ!", "ข้อมูลถูกลบเรียบร้อย", "success");
              setTimeout(function() {
                window.location.reload();
              }, 2000);
            } else {
              Swal.fire("ลบข้อมูลล้มเหลว!", "ข้อมูลยังไม่ถูกลบ", "error");
            }
          }
        });
      }
    });
  });
}
function allowNumbersOnly(e) {
  var code = e.which ? e.which : e.keyCode;
  if (code > 31 && (code < 48 || code > 57)) {
    e.preventDefault();
  }
}
