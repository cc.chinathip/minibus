<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| Your site name,
|
*/
$config['site_name'] = 'backend-minibus';
$config['version'] = '2.8.0';
