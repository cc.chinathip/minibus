
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
			<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">เพิ่มสินค้า</h1>
              </div>
              <form class="user" id="productSubmit"  action="<?= base_url('backend/product/add_product'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
				  <label for="sel1">ชื่อสินค้า (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อสินค้าภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
				  <label for="sel1">ชื่อสินค้า (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อสินค้าภาษาอังกฤษ" >
                  </div>
				</div>

					<div class="form-group">
						<label for="sel1">ประเภทสินค้า</label>
						<select class="form-control" id="typeSelect" name="type_product" required>
						</select>
					</div>

					<div class="form-group">
						<label for="sel1">สีสินค้า</label>
						<select class="form-control" id="colorSelect" name="product_color[]" multiple="multiple" required>
						</select>
					</div>
				

					<div class="form-group row">
						<div class="col-sm-6 mb-3 mb-sm-0">
							<label for="sel1">จำนวนสินค้า</label>
							<input type="number" class="form-control form-control-user" id="qty" name="qty"   min="1"  placeholder="จำนวนสินค้า" required>
						</div>
						<div class="col-sm-6">
						<label for="sel1">ราคาสินค้า</label>
							<input type="text" class="form-control form-control-user" id="price" name="price" placeholder="ราคา" required onkeypress="allowNumbersOnly(event)">
						</div>
					</div>

                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="product_image" required accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
								</div>
								<!-- <div class="form-group">
								<label for="sel1">รายละเอียดสินค้า (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียดสินค้า..." class="form-control form-control-address" name="product_detail" required></textarea>
								</div>

								<div class="form-group">
								<label for="sel1">รายละเอียดสินค้า (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดสินค้า..." class="form-control form-control-address" name="product_detail_eng" ></textarea>
								</div> -->

								

								<div class="form-group">
								<label for="sel1">Exterior</label>
									<textarea  class="form-control form-control-address" name="Exterior" ></textarea>
								</div>

								<div class="form-group">
								<label for="sel1">Utility</label>
									<textarea  class="form-control form-control-address" name="Utility" ></textarea>
								</div>

								<div class="form-group">
								<label for="sel1">Performance</label>
									<textarea  class="form-control form-control-address" name="Performance" ></textarea>
								</div>

								<div class="form-group">
								<label for="sel1">Safety</label>
									<textarea  class="form-control form-control-address" name="Safety" ></textarea>
								</div>

								
								<input type="hidden"  name="created_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="insert" >

								<div class="form-group row">
									<div class="col-sm-4 mb-3 mb-sm-0">
											<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
									</div>
                  					<div class="col-sm-4">
											<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
                  					</div>
                				</div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>

		/* CKEDITOR.replace("product_detail");
		CKEDITOR.replace("product_detail_eng"); */
		CKEDITOR.replace("Exterior");
		CKEDITOR.replace("Utility");
		CKEDITOR.replace("Performance");
		CKEDITOR.replace("Safety");
		
	  $(document).ready(function() {
      
			

     
    $("#imgInp").change(function(){
        preview_image_one(this);

       
		});
    
	/* 	$( "#product_start" ).datepicker();
		$( "#product_end" ).datepicker(); */

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		//console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Type/all',
				//data: {type_user:'admin'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.type)
					var opt="<option value='' selected=\"selected\">---เลือกประเภทสินค้า---</option>";
					 $.each(data.data.type, function(index,value ) {
						 
						opt +="<option value='"+ value.name_thai +"'>"+value.name_thai+"</option>"
						//console.log(value.name_thai)
					}); 

					$("#typeSelect").html( opt );//เพิ่มค่าลงใน Select สินค้า

	
					
				}
			}); 

			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Color/all',
				//data: {type_user:'admin'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.type)
					//var opt="<option value='' selected=\"selected\">---เลือกสีสินค้า---</option>";
					var opt = '';
					 $.each(data.data.color, function(index,value ) {
						 
						opt +="<option value='"+ value.id +"'>"+value.name_thai+"</option>"
						//console.log(value.name_thai)
					}); 

					$("#colorSelect").html( opt );//เพิ่มค่าลงใน Select สินค้า

	
					
				}
			}); 

			$('#colorSelect').multiselect({

				/* maxWidth:550, */
				selectAll: true,
				placeholder: 'เลือกสีสินค้า',
				minHeight:100

			});

			
	});


	</script>
  <!-- Bootstrap core JavaScript-->
