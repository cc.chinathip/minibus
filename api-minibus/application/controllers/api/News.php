<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class News extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_news');
	}

	/**
	 * @api {get} /news/all Get all newss.
	 * @apiVersion 0.1.0
	 * @apiName AllNews 
	 * @apiGroup news
	 * @apiHeader {String} X-Api-Key Newss unique access-key.
	 * @apiPermission News Cant be Accessed permission name : api_news_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Newss.
	 * @apiParam {String} [Field="All Field"] Optional field of Newss : id, name_thai, name_eng, detail, img_news, created_at, created_by, updated_at, updated_by, status.
	 * @apiParam {String} [Start=0] Optional start index of Newss.
	 * @apiParam {String} [Limit=10] Optional limit data of Newss.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of news.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataNews News data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		//$this->is_allowed('api_news_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : null;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','img_news', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$newss = $this->model_api_news->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_news->count_all($filter, $field);

		$data['news'] = $newss;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data News',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /news/detail Detail News.
	 * @apiVersion 0.1.0
	 * @apiName DetailNews
	 * @apiGroup news
	 * @apiHeader {String} X-Api-Key Newss unique access-key.
	 * @apiPermission News Cant be Accessed permission name : api_news_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Newss.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of news.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NewsNotFound News data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_news_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','img_news', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$data['news'] = $this->model_api_news->find($id, $select_field);

		if ($data['news']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail News',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'News not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /news/add Add News.
	 * @apiVersion 0.1.0
	 * @apiName AddNews
	 * @apiGroup news
	 * @apiHeader {String} X-Api-Key Newss unique access-key.
	 * @apiPermission News Cant be Accessed permission name : api_news_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Newss. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Newss. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Newss.  
	 * @apiParam {String} Img_news Mandatory img_news of Newss. Input Img News Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Newss.  
	 * @apiParam {String} Created_by Mandatory created_by of Newss. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Newss.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Newss. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Newss. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_news_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_news', 'Img News', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_news' => $this->input->post('img_news'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'),
				'status' => $this->input->post('status'),
			];
			
			$save_news = $this->model_api_news->store($save_data);

			if ($save_news) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /news/update Update News.
	 * @apiVersion 0.1.0
	 * @apiName UpdateNews
	 * @apiGroup news
	 * @apiHeader {String} X-Api-Key Newss unique access-key.
	 * @apiPermission News Cant be Accessed permission name : api_news_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Newss. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Newss. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Newss.  
	 * @apiParam {String} Img_news Mandatory img_news of Newss. Input Img News Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Newss.  
	 * @apiParam {String} Created_by Mandatory created_by of Newss. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Newss.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Newss. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Newss. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of News.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_status_post()
	{
		//$this->is_allowed('api_news_update', false);

		
	/* 	$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_news', 'Img News', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]'); */
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				/* 'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_news' => $this->input->post('img_news'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'), */
				'status' => $this->input->post('status'),
			];
			
			$save_news = $this->model_api_news->change($this->post('id'), $save_data);

			if ($save_news) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /news/delete Delete News. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteNews
	 * @apiGroup news
	 * @apiHeader {String} X-Api-Key Newss unique access-key.
	 	 * @apiPermission News Cant be Accessed permission name : api_news_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Newss .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_news_delete', false);

		$news = $this->model_api_news->find($this->post('id'));

		if (!$news) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'News not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_news->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'News deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'News not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file News.php */
/* Location: ./application/controllers/api/News.php */