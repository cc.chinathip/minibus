<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function about_all(){	
		$data['title'] = 'ข้อมูลเกี่ยวกับบริษัท';
		$this->load->view('about/about_all');
	}
	
	public function add_about(){	
			$data['title'] = 'เพิ่มข้อมูลบริษัท';
			
			//$this->load->view('index');
			
				if(isset($_POST['insert'])){

					 $rename_img_more1 = Rename_file($_FILES['detail_more_image']['name']);
					 $rename_img_more2 = Rename_file($_FILES['detail_more_image2']['name']);
					 $rename_img_more3 = Rename_file($_FILES['detail_more_image3']['name']);

					 if($_FILES['detail_more_image']['name'] !== ''){

						//echo pathinfo($rename_img_more1, PATHINFO_FILENAME).'<br>';
						$cut_type1 = (explode(".",$rename_img_more1));
						
						$rename_img_more_new1 = $cut_type1[0].'.jpg';

						$path_more = array(
							'upload_path' => './assets/images/about/',
							'name_image' =>  $rename_img_more_new1,
							'name_input_image' => 'detail_more_image'
						);
						check_upload_more($path_more,'insert');

					 }else{
						$rename_img_more_new1 = '';
					}

					 if($_FILES['detail_more_image2']['name'] !== ''){

						 $cut_type2 = (explode(".",$rename_img_more2));

						//$cut_type_new = $cut_type[0].'1'.'.'.$cut_type[1];

						$rename_img_more_new2 =  $cut_type2[0].'1'.'.jpg';
						$path_more2 = array(
							'upload_path' => './assets/images/about/',
							'name_image' =>  $rename_img_more_new2,
							'name_input_image' => 'detail_more_image2'
						);
						check_upload_more($path_more2,'insert');

					 }else{
						 
						$rename_img_more_new2 = '';
					}

					 if($_FILES['detail_more_image3']['name'] !== ''){

						$cut_type3 = (explode(".",$rename_img_more3));

						$rename_img_more_new3 = $cut_type3[0].'2'.'.jpg';
						$path_more3 = array(
							'upload_path' => './assets/images/about/',
							'name_image' =>  $rename_img_more_new3,
							'name_input_image' => 'detail_more_image3'
						);
						check_upload_more($path_more3,'insert');

					 }else{
						 $rename_img_more_new3 = '';
					 }

					 $arr_img_more = array($rename_img_more_new1,$rename_img_more_new2,$rename_img_more_new3);
					

					if($this->input->post('typeSelect') === 'ประวัติองค์กร'){

						$vision = $this->input->post('detail_vision');
						$vision_eng = $this->input->post('detail_vision_eng');

					}else{
						$vision = '';
						$vision_eng = '';
					}

						$data = array(
							'name_thai' => $this->input->post('name_thai'),
							'name_eng' => $this->input->post('name_eng'),
							'detail' => $this->input->post('about_detail'),
							'detail_eng' => $this->input->post('about_detail_eng'),
							'img_about' => 'img-'.Rename_file($_FILES['about_image']['name']),
							'created_by' => $this->input->post('created_by'),
							'status' => 'off',
							'type' => $this->input->post('typeSelect'),
							'detail_vision' => $vision,
							//'detail_vision_eng' => $vision_eng,
							'detail_more' => $this->input->post('detail_more'),
							'detail_more2' => $this->input->post('detail_more2'),
							'detail_more3' => $this->input->post('detail_more3'),
							'image_more' => implode(",",$arr_img_more),
							'name_detail_more' => $this->input->post('name_detail_more'),
							'name_detail_more2' => $this->input->post('name_detail_more2'),
							'name_detail_more3' => $this->input->post('name_detail_more3'),

							
						);

						$data = $this->db->insert('about',$data);

				

						$path = array(
							'upload_path' => './assets/images/about/',
							'redirect_path1' => 'about/add_about_form',
							'redirect_path2' => 'about/about_all',
							'name_image' =>  'img-'.Rename_file($_FILES['about_image']['name']),
							'name_input_image' => 'about_image'
						);
						check_upload($path,'insert');

						
						
					
				}else{

					$this->load->view('about/add_about_form');
				}
	}

	public function update_about(){

		$data['title'] = 'หน้าแก้ไขข้อมูลบริษัท';
		
				if(isset($_POST['update'])){
					
					if($_FILES['about_image']['name'] !== ''){

						$image = 'img-'.Rename_file($image = $_FILES['about_image']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					}

					$rename_img_more1 = Rename_file($_FILES['detail_more_image']['name']);
					 $rename_img_more2 = Rename_file($_FILES['detail_more_image2']['name']);
					 $rename_img_more3 = Rename_file($_FILES['detail_more_image3']['name']);

					 if($_FILES['detail_more_image']['name'] !== ''){

						//echo pathinfo($rename_img_more1, PATHINFO_FILENAME).'<br>';
						$cut_type1 = (explode(".",$rename_img_more1));
						
						$rename_img_more_new1 = $cut_type1[0].'.jpg';
						$path_more = array(
							'upload_path' => './assets/images/about/',
							'name_image' =>  $rename_img_more_new1,
							'name_input_image' => 'detail_more_image'
						);
						check_upload_more($path_more,'update');

					 }else{
						$rename_img_more_new1 = $this->input->post('old_image_more');
					}

					 if($_FILES['detail_more_image2']['name'] !== ''){

						 $cut_type2 = (explode(".",$rename_img_more2));

						//$cut_type_new = $cut_type[0].'1'.'.'.$cut_type[1];

						$rename_img_more_new2 =  $cut_type2[0].'1'.'.jpg';
						$path_more2 = array(
							'upload_path' => './assets/images/about/',
							'name_image' =>  $rename_img_more_new2,
							'name_input_image' => 'detail_more_image2'
						);
						check_upload_more($path_more2,'update');

					 }else{
						 
						$rename_img_more_new2 = $this->input->post('old_image_more2');
					}

					 if($_FILES['detail_more_image3']['name'] !== ''){

						$cut_type3 = (explode(".",$rename_img_more3));

						$rename_img_more_new3 = $cut_type3[0].'2'.'.jpg';
						$path_more3 = array(
							'upload_path' => './assets/images/about/',
							'name_image' =>  $rename_img_more_new3,
							'name_input_image' => 'detail_more_image3'
						);
						check_upload_more($path_more3,'update');

					 }else{
						 $rename_img_more_new3 = $this->input->post('old_image_more3');
					 }

					 $arr_img_more = array($rename_img_more_new1,$rename_img_more_new2,$rename_img_more_new3);


					
					if($this->input->post('type') === 'ประวัติองค์กร'){

						$vision = $this->input->post('detail_vision');
						$vision_eng = $this->input->post('detail_vision_eng');

					}else{
						$vision = '';
						$vision_eng = '';
					}

					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
						'detail' => $this->input->post('about_detail'),
						'detail_eng' => $this->input->post('about_detail_eng'),
						'img_about' => $image,
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						'detail_vision' => $vision,
						'detail_vision_eng' => $vision_eng,
						'detail_more' => $this->input->post('detail_more'),
						'detail_more2' => $this->input->post('detail_more2'),
						'detail_more3' => $this->input->post('detail_more3'),
						'image_more' => implode(",",$arr_img_more),
						'name_detail_more' => $this->input->post('name_detail_more'),
						'name_detail_more2' => $this->input->post('name_detail_more2'),
						'name_detail_more3' => $this->input->post('name_detail_more3'),

						//'status' => 'on',
					);

					/* print_r($data);
					exit(); */

					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('about',$data);

				
			
					$path = array(
						'upload_path' => './assets/images/about/',
						'redirect_path1' => 'about/about_all',
						'redirect_path2' => 'about/about_all',
						'name_image' =>  $image,
						'name_input_image' => 'about_image'
					);
					check_upload($path,'update');

					
					
			}else{
				$this->load->view('about/update_about_form');
			}
	}
   
}
?>
