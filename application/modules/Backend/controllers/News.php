<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function news_all(){	
		$data['title'] = 'รุ่นรถทั้งหมด';
		//$this->load->view('index');
		$this->load->view('news/news_all');
	}
	
	public function add_news(){	
		$data['title'] = 'เพิ่มรุ่นรถ';
		//$this->load->view('index');
		if(isset($_POST['insert'])){

			
			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('news_detail'),
				'detail_eng' => $this->input->post('news_detail_eng'),
				'img_news' => Rename_file($_FILES['news_image']['name']),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);

			$data = $this->db->insert('news',$data);

			$path = array(
				'upload_path' => './assets/images/news/',
				'redirect_path1' => 'news/add_news_form',
				'redirect_path2' => 'news/news_all',
				'name_image' => Rename_file($_FILES['news_image']['name']),
				'name_input_image' => 'news_image'
			);
			check_upload($path,'insert');


		}else{

			$this->load->view('news/add_news_form');
			
		}
		
	}

	public function update_news(){

		$data['title'] = 'หน้าแก้ไขข่าวสาร';
		
				if(isset($_POST['update'])){
					
					if($_FILES['news_image']['name'] !== ''){

						$image = Rename_file($_FILES['news_image']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					}
					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
						'detail' => $this->input->post('news_detail'),
						'detail_eng' => $this->input->post('news_detail_eng'),
						'img_news' => $image,
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						//'status' => 'on',
					);

					/* print_r($data);
					exit(); */

					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('news',$data);
					
					
					$path = array(
						'upload_path' => './assets/images/news/',
						'redirect_path1' => 'news/news_all',
						'redirect_path2' => 'news/news_all',
						'name_image' =>  $image,
						'name_input_image' => 'news_image'
					);
					check_upload($path,'update');
			
					
			}else{
				$this->load->view('news/update_news_form');
			}
	}
   
}
?>
