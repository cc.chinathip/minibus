
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
			<div class="form-group row button_back_page"><a href="<?= base_url('backend/Admin/Dashboard'); ?>"><button type="button" class="btn btn-secondary">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>ย้อนกลับ
			</button></a></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลส่วนตัว</h1>
              </div>
              <form class="user" id="update_profile_form">
                <div class="form-group row">
                  <div class="col-sm-12 mb-3 mb-sm-0">
				  <label for="sel1">ชื่อ</label>
                    <input type="text" class="form-control form-control-user" id="name" name="name" placeholder="ชื่อ" required>
                  </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                    <label for="sel1">นามสกุล</label>
                        <input type="text" class="form-control form-control-user" id="surname" name="surname" placeholder="นามสกุล" required>
                    </div>
                </div>

                <div class="form-group row">
									<div class="col-sm-12 mb-3 mb-sm-0">
									<label for="sel1">ชื่อผู้ใช้</label>
                  	<input type="text" class="form-control form-control-user" id="username" name="username" placeholder="ชื่อผู้ใช้" required >
									</div>
									<!-- <div class="col-sm-6">
									<label for="sel1">รหัสผ่านใหม่</label>
                    <input type="text" class="form-control form-control-user" id="password" name="password" placeholder="รหัสผ่าน" required >
                  </div> -->
                </div>

                <input type="hidden" id="id" name="id" value="<?= $this->session->userdata("session_userid") ?>">
              
										<div class="form-group row">
												<div class="col-sm-4 mb-3 mb-sm-0">
														<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
												</div>
												<div class="col-sm-4">
														<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
												</div>
										</div>
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>

	  $(document).ready(function() {

		//$('#update_profile_form').on("submit", function(e){ 
		//e.preventDefault();
			var apikey =  "<?= API_KEY; ?>"; 
			var api_url = "<?= API_URL; ?>";
            var user_id = "<?= $this->session->userdata("session_userid") ?>";
            console.log('user_id',user_id)
			
				$.ajax({
					//cache: true,
					type:'GET',
					async:false,
					url:   api_url+'api/Users/detail',
					data: {id:user_id},
					xhrFields: {
					withCredentials: false
					},
					headers: {
						'X-Api-Key': apikey,
            
					},
					success: function(data) {
						console.log('data222',data.data.users.name)
                        $('#name').val(data.data.users.name)
                        $('#surname').val(data.data.users.surname)
                        $('#username').val(data.data.users.username)
                        //$('#password').val(data.data.users.password)

						
					}
				}); 

                $('#update_profile_form').on("submit", function(e){ 
                        e.preventDefault();
                        console.log('$("#update_profile_form").serialize()',$("#update_profile_form").serialize())
                        $.ajax({
                        //cache: true,
                        type:'POST',
                        async:false,
                        url:   api_url+'api/Users/update_profile_admin',
                        data: $("#update_profile_form").serialize(),
                        xhrFields: {
                            withCredentials: false
                        },
                        headers: {
                            'X-Api-Key': apikey,
                
                        },
                        success: function(data) {
                            console.log('update_profile_form',data)
                            if(data.status == true){

                                Swal.fire({
                                    title: 'success!',
                                    text: 'แก้ไขข้อมูลสำเร็จ',
                                    type: 'success',
                                })
                                setTimeout(function(){ 
                                    window.location.replace('<?= base_url('backend/Admin/Dashboard'); ?>') 
                                }, 2000);
                                
                            }else{
                                
                                Swal.fire({
                                    title: 'error!',
                                    text: 'แก้ไขข้อมูลล้มเหลว',
                                    type: 'error',
                                })
                            }
                        
                            
                        }
                    }); 
                });
		//});
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
