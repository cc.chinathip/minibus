<?= $this->load->view('section/header'); ?>
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li> 
		    <li class="breadcrumb-item active"><span>เกี่ยวกับเรา</span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">เกี่ยวกับเรา</h2>
	</div>
</div> 
  
<div class="section section-aboutus-header">
	<div class="container">
 		<ul class="nav nav-aboutus">
 			<!-- navbar -->
 		</ul>
	</div><!--container-->
</div><!--section-aboutus-header-->

<div class="section section-aboutus pt-0">
	<div class="container">
		<div class="aboutus-banner">
			<!-- image banner -->
		</div>

		<div class="article">
			<h2 class="text-center about_us_title"></h2>

			<div class="detail_about"></div>

		</div><!--article-->
	</div><!--container-->
</div><!--section-aboutus-->

<div class="section section-vision">
	<div class="vision-texture">
		<img class="item1 wow fadeInLeft" src="<?php echo base_url('/assets/frontend/img/thumb/vision-texture--1.png') ?>" alt="">
		<img class="item2 wow fadeInRight" src="<?php echo base_url('/assets/frontend/img/thumb/vision-texture--2.png') ?>" alt="">
	</div>
	<div class="container">
		<div class="article">
			<h2 class="title-lg wow fadeInUp" data-wow-delay="0.1s">วิสัยทัศน์</h2>
			<p class="wow fadeInUp italic vision_detail" data-wow-delay="0.3s" >
			</p>
		</div>
	</div><!--container-->
</div><!--section-vision-->

<div class="section section-mission pt-0">
	<div class="container wow fadeIn">
		<div class="section-header">
			<h2 class="title-lg text-center">พันธกิจ</h2>
		</div>
		<div class="row space-0 wow fadeIn detail_more">

			
			
		</div><!--row-->
	</div><!--container-->
</div><!--section-mission-->

<?= $this->load->view('section/footer'); ?>
<script>

	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/About/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					/* console.log('data',data.data.about)
					console.log('total',data.total) */
					//console.log('datasssss',data.data.about[0].status)
					var html = '';
					 $.each(data.data.about, function(index,value ) {
						
							if(value.type === 'ประวัติองค์กร'){

								var link = '<?= base_url('frontend/Aboutus/aboutus/'); ?>';
								var active = 'active aboutus_nevber';
								var class_aboutus = 'a_aboutus';

							}else{

								var link = '<?= base_url('frontend/Aboutus/aboutus_policy/'); ?>';
								var active = '';
							}
							html += '<li class="'+active+'"><a class="'+class_aboutus+'" href="'+link+value.id+'"><span>'+value.name_thai+'</span></a></li>'
						
							
					}); 
					
					$('.nav-aboutus').append(html);
					
					
				}
			}); 

			var aboutus_id = '<?= $aboutus_id ?>';
			//console.log('id'.id)
			if(aboutus_id !== '' && aboutus_id !== undefined){
				$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/About/detail',
				data: {id:aboutus_id},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data2222',data.data)
					
					$('.about_us_title').html(data.data.about.name_thai)
					$('.detail_about').html(data.data.about.detail)
					$('.vision_detail').html(data.data.about.detail_vision)
					$('.aboutus-banner').html('<img class="w-100" src="<?php echo base_url('/assets/images/about/') ?>'+data.data.about.img_about+'" alt="">')
						var explode_image = data.data.about.image_more.split(',');
						var html_more_detail ='<div class="col-lg-4 col-sm-6">'
							html_more_detail +='<div class="card card-info mission">'
							html_more_detail +='<div class="card-photo">'
							html_more_detail +='<div class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/images/about/') ?>'+explode_image[0]+');">'
							html_more_detail +='<img class="image_size" src="<?php echo base_url('/assets/images/about/') ?>'+explode_image[0]+'" alt="">'
							html_more_detail +='</div>'
							html_more_detail +='</div>'
							html_more_detail +='<div class="card-body">'
							html_more_detail +='<h2 class="title-sm">'+data.data.about.name_detail_more+'</h2>'
							html_more_detail +='<p class="text">'+data.data.about.detail_more+'</p>'
							html_more_detail +='</div>'
							html_more_detail +='</div>'
							html_more_detail +='</div>'

							html_more_detail += '<div class="col-lg-4 col-sm-6">'
							html_more_detail +='<div class="card card-info mission">'
							html_more_detail +='<div class="card-photo">'
							html_more_detail +='<div class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/images/about/') ?>'+explode_image[1]+');">'
							html_more_detail +='<img class="image_size" src="<?php echo base_url('/assets/images/about/') ?>'+explode_image[1]+'" alt="">'
							html_more_detail +='</div>'
							html_more_detail +='</div>'
							html_more_detail +='<div class="card-body">'
							html_more_detail +='<h2 class="title-sm">'+data.data.about.name_detail_more2+'</h2>'
							html_more_detail +='<p class="text">'+data.data.about.detail_more2+'</p>'
							html_more_detail +='</div>'
							html_more_detail +='</div>'
							html_more_detail +='</div>'

							html_more_detail += '<div class="col-lg-4 col-sm-6">'
							html_more_detail +='<div class="card card-info mission">'
							html_more_detail +='<div class="card-photo">'
							html_more_detail +='<div class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/images/about/') ?>'+explode_image[2]+');">'
							html_more_detail +='<img class="image_size" src="<?php echo base_url('/assets/images/about/') ?>'+explode_image[2]+'" alt="">'
							html_more_detail +='</div>'
							html_more_detail +='</div>'
							html_more_detail +='<div class="card-body">'
							html_more_detail +='<h2 class="title-sm">'+data.data.about.name_detail_more3+'</h2>'
							html_more_detail +='<p class="text">'+data.data.about.detail_more3+'</p>'
							html_more_detail +='</div>'
							html_more_detail +='</div>'
							html_more_detail +='</div>'

							$('.detail_more').append(html_more_detail);
				}
			});

			}
			
	});

  </script>
