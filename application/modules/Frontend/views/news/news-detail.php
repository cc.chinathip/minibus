<?= $this->load->view('section/header'); ?>
<!-- <?= $news_id ?> -->
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Users/index'); ?>">หน้าหลัก</a></li>
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/news/news'); ?>">ข่าวสาร</a></li>
		    <li class="breadcrumb-item active"><span class="link_now"></span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">ข่าวสาร</h2>
	</div>
</div> 
	<div class="page-header-info wow fadeIn">
	<div class="container">
		<p>
        อัพเดตเรื่องราว บทความต่างๆที่น่าสนใจ ความเคลื่อนไหวของเรา
ให้คุณได้ทราบทั้งหมด
		</p>
	</div>
</div> 

<div class="section section-banner">
	<div class="background blur background_img_news" ></div>
	<div class="container image_news_detail">
		
	</div>
</div><!--section-banner-->

<div class="section section-detail main p-0">
	<div class="container">
		<div class="news wow fadeIn">
			<p><div class="tags">ข่าวประชาสัมพันธ์</div></p>
			<h2>ข้อเสนอสุดพิเศษสำหรับแพนเทอร่า มอเตอร์ ใหม่</h2>
			<div class="row mt-3 mb-4 align-items-center">
				<div class="col-4">
					<span class="date">25 July 2019</span>
				</div>
				<div class="col-8">
					<div class="share-wrap">
						<span class="icons icon-share"></span>
						<span class="text">แชร์ไปยัง : </span>
						<a class="icons icon-share-facebook" href="#"></a>
						<a class="icons icon-share-line" href="#"></a>
					</div>
				</div>
			</div>

			<!-- <p>ข่าวสารรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ มีผลตั้งแต่วันที่ 25 กรกฏาคม 2562 – 30 กันยายน 2562 มีรายละเอียดดังนี้</p> -->

			<!-- <ul>
				<li class="mb-0">ดอกเบี้ยพิเศษ 1.99% (1)</li>
				<li class="mb-0">ฟรี ประกันภัยชั้นหนึ่ง นาน 1 ปี (2)</li>
				<li class="mb-0">ฟรี รับประกันคุณภาพ 5 ปี (3)</li>
				<li class="mb-0">ฟรี ค่าแรงเช็กระยะนาน 5 ปี (4)</li>
				<li class="mb-0">ฟรี อุปกรณ์เชื่อมต่อสัญญาณภาพและเสียงระบบดิจิตอล (HDMI WiFi Dongle) (5)</li>
			</ul> -->
 

			<ol class="detail_news">
				<!-- <li>
					สำหรับลูกค้าที่จองรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020) ตั้งแต่วันที่ 25 ก.ค. 62 – 30 ก.ย. 62 และออกรถภายในวันที่ 31 	ต.ค. 62 เลือกรับดอกเบี้ยพิเศษ
				    ในอัตราร้อยละ 1.99 สำหรับลูกค้าที่ชำระเงินดาวน์เริ่มต้นในอัตราร้อยละ 25 ของราคารถยนต์ และผ่อนชำระค่าเช่าซื้อทั้งหมดเป็นจำนวน 48 งวดเท่านั้น (เงื่อนไขและ
				    รายละเอียดการให้สินเชื่อเช่าซื้อโปรดสอบถามเพิ่มเติมได้ที่สถาบันการเงินที่ร่วมรายการ ได้แก่ มิตซู ลิสซิ่ง ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) ธนาคารธนชาติ จำกัด
				    (มหาชน) บริษัท ลีสซิ่งกสิกรไทย จำกัด ธนาคารไทยพาณิชย์ จำกัด (มหาชน) ธนาคาร ไอซีบีซี (ไทย) จำกัด (มหาชน) ธนาคารทิสโก้ จำกัด (มหาชน) และ ธนาคาร
				    เกียรตินาคิน จำกัด (มหาชน)

				</li>

				<li>
					สำหรับรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต และ มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020)<br>
					รับฟรี ค่าเบี้ยประกันภัยชั้นหนึ่งไดมอนด์ โพรเทคชั่น เป็นระยะเวลา 1 ปี มูลค่าสูงสุด 28,499 บาท ทั้งนี้ เงื่อนไขการรับประกันภัยและทุนประกันภัยเป็นไปตามกรมธรรม์
    				ประกันภัยที่บริษัทรับประกันกำหนด และเงื่อนไขของกรมธรรม์แต่ละฉบับจะมีความแตกต่างกันตามมูลค่าของรถยนต์ที่เอาประกันภัย
				</li>

				<li>
					รับฟรี การรับประกันคุณภาพรถยนต์ (Diamond Warranty) 5 ปี หรือ 100,000 กิโลเมตร (แล้วแต่ระยะใดจะถึงก่อน) ระยะเวลาการรับประกันของชิ้นส่วนและอุปกรณ์
    แต่ละชนิดอาจแตกต่างกันตามที่ระบุไว้ในสมุดรับบริการและคู่มือการใช้รถ ซึ่งรับประกันโดยบริษัท มิตซูบิชิ มอเตอร์ส (ประเทศไทย) จำกัด โปรดศึกษารายละเอียดการ
    รับประกันเพิ่มเติมในคู่มือรถ โดยลูกค้าสามารถเข้ารับบริการที่ศูนย์บริการมาตรฐานของผู้จำหน่ายมิตซูบิชิที่ได้รับการแต่งตั้งเท่านั้น
				</li>

				<li>รับรายการฟรีค่าแรงเช็กระยะนาน 5 ปี หรือ 100,000 กิโลเมตร (แล้วแต่ระยะใดจะถึงก่อน) มูลค่าสูงสุด 8,150 บาท อัตราค่าแรงที่นำมาคำนวณอ้างอิงจากอัตราค่าแรง
    กลาง บริการฟรีเฉพาะค่าแรงเช็กระยะตามที่กำหนดไว้ในบัตรตรวจเช็กระยะฟรีในสมุดรับบริการและคู่มือการใช้รถ ซึ่งรถยนต์ของลูกค้าจะได้รับการตรวจสอบและบำรุง
    รักษาตามรายการที่ระบุไว้ โดยลูกค้าสามารถเข้ารับบริการที่ศูนย์บริการมาตรฐานของผู้จำหน่ายมิตซูบิชิที่ได้รับการแต่งตั้งเท่านั้น</li>

    			<li>สำหรับลูกค้าที่จองรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020) รุ่น 2WD 2.4D GT-Premium และ 4WD 2.4D GT-Premium ตั้งแต่วันที่ 25 ก.ค. 62 ถึง
    31 ต.ค.62 และออกรถภายในวันที่ 30 พ.ย. 62 รับฟรี อุปกรณ์เชื่อมต่อสัญญาณภาพและเสียงระบบดิจิตอล (HDMI WiFi Dongle) จำนวน 1 ชิ้น มูลค่า 1,690 บาท
    ในวันออกรถ ณ ศูนย์บริการรถยนต์มิตซูบิชิที่ท่านออกรถ</li> -->
			</ol>
		</div>

		<div class="news-footer">
			<button class="btn btn-red btn-back has-arrow left" onclick="goBack()">
				<span class="icon"><span class="arrow-left"></span></span>
				<span class="text">ย้อนกลับ</span>
			</button>
		</div>
	</div><!--container-->
</div><!--section-detail-->


<div class="section section-related">
	<div class="background" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/texture-gray.jpg') ?>);"></div>
	<div class="container">
		<h2 class="title-md">ข่าวประชาสัมพันธ์อื่นๆ ที่เกี่ยวข้อง</h2>

		<div class="row space-0 wow fadeIn show_news">

			
			
			
		</div><!--row-->
	</div><!--container-->
</div><!--section-related-->

<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/News/detail',
				data: {id:<?= $news_id ?>},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data',data.data.news.created_at)
					$('.date').html(data.data.news.created_at)
					$('.detail_news').html(data.data.news.detail)
					$('.image_news_detail').html('<img class="w-100 wow fadeIn" data-wow-delay="0.5s" src="<?= base_url('/assets/images/news/') ?>'+data.data.news.img_news+'" alt="">')
					$('.background_img_news').css('background-image', 'url(<?= base_url('/assets/images/news/') ?>'+data.data.news.img_news+')');
					$('.link_now').html(data.data.news.name_thai)
				}
			});

			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/News/all',
				data: {orderby:'RANDOM'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.news)
					console.log('total',data.total)
					//console.log('datasssss',data.data.news[0].status)
					var html = '';
					 $.each(data.data.news, function(index,value ) {
						 var setNumberImg = 6;
						if(index < 4){
							html += '<div class="block_news col-lg-3 col-sm-6" >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/news/') ?>'+value.img_news+');" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/news/') ?>'+value.img_news+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-12">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

										

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'
						}

							 
						//}
						
						
					}); 
					
					
					$('.show_news').append(html);
					
				
					
				}
			}); 
	   });
</script>
