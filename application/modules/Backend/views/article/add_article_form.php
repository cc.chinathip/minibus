
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">เพิ่มบทความ</h1>
              </div>
              <form class="user" id="articleSubmit"  action="<?= base_url('backend/article/add_article'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อบทความภาษาไทย</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อบทความภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ชื่อบทความภาษาอังกฤษ</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อบทความภาษาอังกฤษ" >
                  </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="article_image" required accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-12 mb-3 mb-sm-0">
                    <label for="sel1">ลิงค์ youtube</label>
                      <input type="text" class="form-control form-control-user" id="link_youtube" name="link_youtube" placeholder="ลิงค์วีดีโอจาก youtube" ><p>
                    </div>
                </div>

								<!-- <div class="form-group">
                    <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="file-to-upload"
                                aria-describedby="inputGroupFileAddon01" name="article_video"  accept="video/mp4">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกไฟล์วีดีโอ</label>
                            </div>
                    </div><p>

                    <div id="video-demo-container">
                      <video id="main-video"  controls>
                        <source type="video/mp4" />
                      </video>
									
                    </div>

								</div> -->


								<div class="form-group">
                <label for="sel1">รายละเอียดบทความ (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียดบทความ..." class="form-control form-control-address" name="article_detail" required></textarea>
								</div>

								<div class="form-group">
                <label for="sel1">รายละเอียดบทความ (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดบทความ..." class="form-control form-control-address" name="article_detail_eng" required></textarea>
								</div>

								
								<input type="hidden"  name="created_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="insert" >

								<div class="form-group row">
                  <div class="col-sm-4 mb-3 mb-sm-0">
											<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
                  </div>
                  <div class="col-sm-4">
											<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
                  </div>
                </div>
									


              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		CKEDITOR.replace("article_detail");
		CKEDITOR.replace("article_detail_eng");
		

	  $(document).ready(function() {
      
      $("#imgInp").change(function(){
          preview_image_one(this);
        
      });
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
