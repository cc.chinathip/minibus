<?= $this->load->view('section/header'); ?>
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li>
		    <li class="breadcrumb-item active"><span>ข่าวสาร</span></li> 
	  	</ol>
	</div>
</nav>
 
<div class="section section-news main pt-0">
	<div class="page-header wow fadeIn">
		<div class="container">
			<h2 class="title-xl">ข่าวสาร</h2>
		</div>
	</div> 
 	<div class="page-header-info wow fadeIn">
		<div class="container">
			<p>
				อัพเดตเรื่องราว บทความต่างๆที่น่าสนใจ ความเคลื่อนไหวของเรา<br>
				ให้คุณได้ทราบทั้งหมด
			</p>
		</div>
	</div>  

	<div class="news-box"> 
		<div class="container"> 
			<div class="row title-group">
				<div class="col-8 left"><h2 class="title-md">ข่าวประชาสัมพันธ์</h2></div>
				<div class="col-4 right"><a href="<?= base_url('frontend/News/news'); ?>" class="btn btn-blue"><span>ดูทั้งหมด</span></a></div>
			</div>
			<div class="row space-0 wow fadeIn show_news">
				 
			</div><!--row-->
		</div><!--container-->
	</div><!--news-box-->

	<div class="news-box"> 
		<div class="container"> 
			<div class="row title-group">
				<div class="col-8 left"><h2 class="title-md">กิจกรรม</h2></div>
				<div class="col-4 right"><a href="<?= base_url('frontend/Activity/activity'); ?>" class="btn btn-blue"><span>ดูทั้งหมด</span></a></div>
			</div>
			<div class="row space-0 wow fadeIn show_activity">
				 
				
				 
			</div><!--row-->
		</div><!--container-->
	</div><!--news-box-->

	<div class="news-box"> 
		<div class="container"> 
			<div class="row title-group">
				<div class="col-8 left"><h2 class="title-md">บทความและVDO</h2></div>
				<div class="col-4 right"><a href="<?= base_url('frontend/Article/article'); ?>" class="btn btn-blue"><span>ดูทั้งหมด</span></a></div>
			</div>
			<div class="row space-0 wow fadeIn show_article">
				 
				
				 
			</div><!--row-->
		</div><!--container-->
	</div><!--news-box-->
</div><!--section-news-->
 
<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/News/all',
				data: {filter:'on',field:'status',limit:'3'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.news)
					console.log('total',data.total)
					//console.log('datasssss',data.data.news[0].status)
					var html = '';
					 $.each(data.data.news, function(index,value ) {
						
						console.log('index',parseInt(index+1))
						
							var number_at = parseInt(index+1);

							html += '<div class="block_news col-lg-4 col-sm-6 '+(number_at > 3 ? 'Img_more' : '') +' " >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/news/') ?>'+value.img_news+');" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/news/') ?>'+value.img_news+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-6">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

											html += '<div class="col-6">'
												html += '<div class="share-wrap">'
												html += '<span class="icons icon-share"></span>'
												html += '<span class="text">แชร์ไปยัง : </span>'
												html += '<a class="icons icon-share-facebook" href="#"></a> '
												html += '<a class="icons icon-share-line" href="#"></a>'
												html += '</div>'
											html += '</div>'

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'

							 
						//}
						
						
					}); 
					
					$('.show_news').append(html);
					
				
				}
            }); 
            
            $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Activity/all',
				data: {filter:'on',field:'status',limit:'3'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.activity)
					console.log('total',data.total)
					//console.log('datasssss',data.data.activity[0].status)
					var html = '';
					 $.each(data.data.activity, function(index,value ) {
						
						console.log('index',parseInt(index+1))
						
							var number_at = parseInt(index+1);

							html += '<div class="block_activity col-lg-4 col-sm-6 '+(number_at > 3 ? 'Img_more' : '') +' " >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/activity/') ?>'+value.img_activity+');" href="<?= base_url('/frontend/activity/activity_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/activity/') ?>'+value.img_activity+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/activity/activity_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-6">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/activity/activity_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

											html += '<div class="col-6">'
												html += '<div class="share-wrap">'
												html += '<span class="icons icon-share"></span>'
												html += '<span class="text">แชร์ไปยัง : </span>'
												html += '<a class="icons icon-share-facebook" href="#"></a> '
												html += '<a class="icons icon-share-line" href="#"></a>'
												html += '</div>'
											html += '</div>'

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'

							 
						//}
						
						
					}); 
					
					$('.show_activity').append(html);
					
				
				}
			}); 
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Article/all',
				data: {filter:'on',field:'status',limit:'3'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.article)
					console.log('total',data.total)
					//console.log('datasssss',data.data.article[0].status)
					var html = '';
					 $.each(data.data.article, function(index,value ) {
						
						console.log('index',parseInt(index+1))
						
							var number_at = parseInt(index+1);

							html += '<div class="block_article col-lg-4 col-sm-6 '+(number_at > 3 ? 'Img_more' : '') +' " >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/article/') ?>'+value.img_article+');" href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/article/') ?>'+value.img_article+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-6">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

											html += '<div class="col-6">'
												html += '<div class="share-wrap">'
												html += '<span class="icons icon-share"></span>'
												html += '<span class="text">แชร์ไปยัง : </span>'
												html += '<a class="icons icon-share-facebook" href="#"></a> '
												html += '<a class="icons icon-share-line" href="#"></a>'
												html += '</div>'
											html += '</div>'

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'

							 
						//}
						
						
					}); 
					
					$('.show_article').append(html);
					
				
				}
			}); 

			
		
	});

  </script>
