<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class aboutus extends MX_Controller {

    public function __construct()
	{
		parent::__construct();
		//$this->load->model('Users_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function aboutus($aboutus_id=null){	
		$data['title'] = 'เกี่ยวกับเรา';
		$data['aboutus_id'] = $aboutus_id;

		$this->load->view('aboutus/aboutus',$data);
		//$this->load->view('product/car-model-diesel');
		
    }
    

	public function aboutus_policy($aboutus_id=null){	
		$data['title'] = 'เกี่ยวกับเรา';
		$data['aboutus_id'] = $aboutus_id;

		$this->load->view('aboutus/aboutus-policy',$data);
		//$this->load->view('product/car-model-diesel');
		
    }

    
    
}
