-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 25, 2019 at 04:56 PM
-- Server version: 5.5.60-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_minibus`
--

-- --------------------------------------------------------

--
-- Table structure for table `aauth_groups`
--

CREATE TABLE `aauth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_groups`
--

INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
(1, 'Admin', 'Superadmin Group'),
(2, 'Public', 'Public Group'),
(3, 'Default', 'Default Access Group'),
(4, 'Member', 'Member Access Group');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_group_to_group`
--

CREATE TABLE `aauth_group_to_group` (
  `group_id` int(11) UNSIGNED NOT NULL,
  `subgroup_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_login_attempts`
--

CREATE TABLE `aauth_login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(39) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perms`
--

CREATE TABLE `aauth_perms` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_perms`
--

INSERT INTO `aauth_perms` (`id`, `name`, `definition`) VALUES
(1, 'menu_dashboard', NULL),
(2, 'menu_crud_builder', NULL),
(3, 'menu_api_builder', NULL),
(4, 'menu_page_builder', NULL),
(5, 'menu_form_builder', NULL),
(6, 'menu_menu', NULL),
(7, 'menu_auth', NULL),
(8, 'menu_user', NULL),
(9, 'menu_group', NULL),
(10, 'menu_access', NULL),
(11, 'menu_permission', NULL),
(12, 'menu_api_documentation', NULL),
(13, 'menu_web_documentation', NULL),
(14, 'menu_settings', NULL),
(15, 'user_list', NULL),
(16, 'user_update_status', NULL),
(17, 'user_export', NULL),
(18, 'user_add', NULL),
(19, 'user_update', NULL),
(20, 'user_update_profile', NULL),
(21, 'user_update_password', NULL),
(22, 'user_profile', NULL),
(23, 'user_view', NULL),
(24, 'user_delete', NULL),
(25, 'blog_list', NULL),
(26, 'blog_export', NULL),
(27, 'blog_add', NULL),
(28, 'blog_update', NULL),
(29, 'blog_view', NULL),
(30, 'blog_delete', NULL),
(31, 'form_list', NULL),
(32, 'form_export', NULL),
(33, 'form_add', NULL),
(34, 'form_update', NULL),
(35, 'form_view', NULL),
(36, 'form_manage', NULL),
(37, 'form_delete', NULL),
(38, 'crud_list', NULL),
(39, 'crud_export', NULL),
(40, 'crud_add', NULL),
(41, 'crud_update', NULL),
(42, 'crud_view', NULL),
(43, 'crud_delete', NULL),
(44, 'rest_list', NULL),
(45, 'rest_export', NULL),
(46, 'rest_add', NULL),
(47, 'rest_update', NULL),
(48, 'rest_view', NULL),
(49, 'rest_delete', NULL),
(50, 'group_list', NULL),
(51, 'group_export', NULL),
(52, 'group_add', NULL),
(53, 'group_update', NULL),
(54, 'group_view', NULL),
(55, 'group_delete', NULL),
(56, 'permission_list', NULL),
(57, 'permission_export', NULL),
(58, 'permission_add', NULL),
(59, 'permission_update', NULL),
(60, 'permission_view', NULL),
(61, 'permission_delete', NULL),
(62, 'access_list', NULL),
(63, 'access_add', NULL),
(64, 'access_update', NULL),
(65, 'menu_list', NULL),
(66, 'menu_add', NULL),
(67, 'menu_update', NULL),
(68, 'menu_delete', NULL),
(69, 'menu_save_ordering', NULL),
(70, 'menu_type_add', NULL),
(71, 'page_list', NULL),
(72, 'page_export', NULL),
(73, 'page_add', NULL),
(74, 'page_update', NULL),
(75, 'page_view', NULL),
(76, 'page_delete', NULL),
(77, 'blog_list', NULL),
(78, 'blog_export', NULL),
(79, 'blog_add', NULL),
(80, 'blog_update', NULL),
(81, 'blog_view', NULL),
(82, 'blog_delete', NULL),
(83, 'setting', NULL),
(84, 'setting_update', NULL),
(85, 'dashboard', NULL),
(86, 'extension_list', NULL),
(87, 'extension_activate', NULL),
(88, 'extension_deactivate', NULL),
(89, 'about_add', ''),
(90, 'about_update', ''),
(91, 'about_view', ''),
(92, 'about_delete', ''),
(93, 'about_list', ''),
(94, 'activity_add', ''),
(95, 'activity_update', ''),
(96, 'activity_view', ''),
(97, 'activity_delete', ''),
(98, 'activity_list', ''),
(99, 'article_add', ''),
(100, 'article_update', ''),
(101, 'article_view', ''),
(102, 'article_delete', ''),
(103, 'article_list', ''),
(104, 'api_users_all', ''),
(105, 'api_users_detail', ''),
(106, 'api_users_add', ''),
(107, 'api_users_update', ''),
(108, 'api_users_delete', ''),
(109, 'users_add', ''),
(110, 'users_update', ''),
(111, 'users_view', ''),
(112, 'users_delete', ''),
(113, 'users_list', ''),
(114, 'api_type_all', ''),
(115, 'api_type_detail', ''),
(116, 'api_type_add', ''),
(117, 'api_type_update', ''),
(118, 'api_type_delete', ''),
(119, 'api_news_all', ''),
(120, 'api_news_detail', ''),
(121, 'api_news_add', ''),
(122, 'api_news_update', ''),
(123, 'api_news_delete', ''),
(124, 'api_article_all', ''),
(125, 'api_article_detail', ''),
(126, 'api_article_add', ''),
(127, 'api_article_update', ''),
(128, 'api_article_delete', ''),
(129, 'api_about_all', ''),
(130, 'api_about_detail', ''),
(131, 'api_about_add', ''),
(132, 'api_about_update', ''),
(133, 'api_about_delete', '');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_group`
--

CREATE TABLE `aauth_perm_to_group` (
  `perm_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_user`
--

CREATE TABLE `aauth_perm_to_user` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_pms`
--

CREATE TABLE `aauth_pms` (
  `id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(225) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT NULL,
  `pm_deleted_receiver` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user`
--

CREATE TABLE `aauth_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_users`
--

CREATE TABLE `aauth_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `oauth_uid` text,
  `oauth_provider` varchar(100) DEFAULT NULL,
  `pass` varchar(64) NOT NULL,
  `username` varchar(100) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `avatar` text NOT NULL,
  `banned` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `top_secret` varchar(16) DEFAULT NULL,
  `ip_address` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_users`
--

INSERT INTO `aauth_users` (`id`, `email`, `oauth_uid`, `oauth_provider`, `pass`, `username`, `full_name`, `avatar`, `banned`, `last_login`, `last_activity`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `top_secret`, `ip_address`) VALUES
(1, 'cc.chinathip@gmail.com', NULL, NULL, '564fec61575e0557f033d6248877cabc8a1749df771ea8af9714a063213a76d1', 'ccchinathip', 'ccchinathip', '', 0, '2019-10-23 21:12:11', '2019-10-23 21:12:11', '2019-09-26 16:49:25', NULL, NULL, NULL, NULL, NULL, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_to_group`
--

CREATE TABLE `aauth_user_to_group` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_user_to_group`
--

INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
(1, 1),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_variables`
--

CREATE TABLE `aauth_user_variables` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `img_about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `detail_vision` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_vision_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_more` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_more2` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_more3` text COLLATE utf8_unicode_ci NOT NULL,
  `image_more` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_detail_more` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_detail_more2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_detail_more3` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name_thai`, `name_eng`, `detail`, `detail_eng`, `img_about`, `created_by`, `created_at`, `updated_by`, `updated_at`, `status`, `type`, `detail_vision`, `detail_vision_eng`, `detail_more`, `detail_more2`, `detail_more3`, `image_more`, `name_detail_more`, `name_detail_more2`, `name_detail_more3`) VALUES
(76, 'ประวัติองค์กร', 'ประวัติองค์กร', '<p>บริษัท แพนเทอรา มอเตอร์ส จำกัด&nbsp;คือบริษัทผู้ผลิตรถบัส NGV ที่มีชื่อเสียง และได้รับมาตรฐานสูงสุดในประเทศจีน จนได้รับรางวัลมากมาย เช่น</p>\r\n\r\n<ul>\r\n	<li>ปี 2007 ได้รับรางวัลที่ 2 ประเภทรถโดยสารประหยัดพลังงานเชื้อเพลิง</li>\r\n	<li>ปี 2008 ได้รับรางวัลจากกระทรวงการก่อสร้างโรงงานผลิตรถบัสที่ดีที่สุด และรางวัลองค์กรที่มีเทคโนโลยีสูงในมณฑลหูเป่ย</li>\r\n	<li>ปี 2009 ได้รับรางวัลผลิตภัณฑ์รถบัสคุณภาพที่ออกโดยสมาคมหูเป่ย</li>\r\n	<li>ปี 2010 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n	<li>ปี 2013 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n</ul>\r\n\r\n<p>ซึ่งรางวัลทั้งหมดที่กล่าวมานั้น เป็นเครื่องการันตีได้ว่าเราคือ &ldquo;ผู้นำการผลิตรถบัสโดยสารในด้านการประหยัดพลังงานเชื้อเพลิงอย่างแท้จริง&rdquo; ซึ่งปัจจุบันมีเครือข่ายอยู่ อยู่หลายประเทศ ก่อตั้งโดยทีมงานที่มีวิสัยทัศน์กว้างไกล และได้เล็งเห็นเรื่องการใช้รถบัสประหยัดพลังงานเชื้อเพลิงเป็นสิ่งสำคัญ จึงได้ระดมทุนกว่า 150 ล้านบาท เพื่อตกลงร่วมมือทางการค้า และดำเนินธุรกิจตามที่ได้ลงนามใน MOU ที่ประเทศจีน เมื่อวันที่ 27 กรกฎาคม 2553 และได้รับการแต่งตั้งให้เป็นตัวแทนจำหน่าย รถโดยสารขนาดใหญ่ และรถบัสทุกประเภท ภายใต้ยี่ห้อ&nbsp;<strong>&ldquo;BONLUCK&rdquo;</strong></p>\r\n\r\n<p>นอกจากนี้ บริษัทฯ สนองตอบต่อความต้องการของลูกค้า โดยได้สร้างทางเลือกให้กับผู้ประกอบการรถโดยสาร รถทัวร์ โดยการนำเข้าแชทซีส์ Yaxing พร้อมเครื่องยนต์คุณภาพดีมาทำการประกอบตัวถังภายในประเทศ เป็นการลดภาษีการนำเข้าส่งผลให้ราคาจำหน่ายถูกลง อีกทั้งยังเป็นการสร้างงานภายในประเทศ อีกด้วย โดยการจัดหาอะไหล่ทางบริษัทได้เล็งเห็นความสำคัญของเรื่องนี้ จึงได้มีการนำเข้าอะไหล่สำรองและคู่มือจากประเทศจีน พร้อมด้วยทีมช่างซ่อมบำรุงมาก ประสบการณ์ ที่ได้รับการอบรมจากต่างประเทศ มาให้บริการ อย่างทันท่วงที</p>\r\n', '<p>บริษัท แพนเทอรา มอเตอร์ส จำกัด&nbsp;คือบริษัทผู้ผลิตรถบัส NGV ที่มีชื่อเสียง และได้รับมาตรฐานสูงสุดในประเทศจีน จนได้รับรางวัลมากมาย เช่น</p>\r\n\r\n<ul>\r\n	<li>ปี 2007 ได้รับรางวัลที่ 2 ประเภทรถโดยสารประหยัดพลังงานเชื้อเพลิง</li>\r\n	<li>ปี 2008 ได้รับรางวัลจากกระทรวงการก่อสร้างโรงงานผลิตรถบัสที่ดีที่สุด และรางวัลองค์กรที่มีเทคโนโลยีสูงในมณฑลหูเป่ย</li>\r\n	<li>ปี 2009 ได้รับรางวัลผลิตภัณฑ์รถบัสคุณภาพที่ออกโดยสมาคมหูเป่ย</li>\r\n	<li>ปี 2010 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n	<li>ปี 2013 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n</ul>\r\n\r\n<p>ซึ่งรางวัลทั้งหมดที่กล่าวมานั้น เป็นเครื่องการันตีได้ว่าเราคือ &ldquo;ผู้นำการผลิตรถบัสโดยสารในด้านการประหยัดพลังงานเชื้อเพลิงอย่างแท้จริง&rdquo; ซึ่งปัจจุบันมีเครือข่ายอยู่ อยู่หลายประเทศ ก่อตั้งโดยทีมงานที่มีวิสัยทัศน์กว้างไกล และได้เล็งเห็นเรื่องการใช้รถบัสประหยัดพลังงานเชื้อเพลิงเป็นสิ่งสำคัญ จึงได้ระดมทุนกว่า 150 ล้านบาท เพื่อตกลงร่วมมือทางการค้า และดำเนินธุรกิจตามที่ได้ลงนามใน MOU ที่ประเทศจีน เมื่อวันที่ 27 กรกฎาคม 2553 และได้รับการแต่งตั้งให้เป็นตัวแทนจำหน่าย รถโดยสารขนาดใหญ่ และรถบัสทุกประเภท ภายใต้ยี่ห้อ&nbsp;<strong>&ldquo;BONLUCK&rdquo;</strong></p>\r\n\r\n<p>นอกจากนี้ บริษัทฯ สนองตอบต่อความต้องการของลูกค้า โดยได้สร้างทางเลือกให้กับผู้ประกอบการรถโดยสาร รถทัวร์ โดยการนำเข้าแชทซีส์ Yaxing พร้อมเครื่องยนต์คุณภาพดีมาทำการประกอบตัวถังภายในประเทศ เป็นการลดภาษีการนำเข้าส่งผลให้ราคาจำหน่ายถูกลง อีกทั้งยังเป็นการสร้างงานภายในประเทศ อีกด้วย โดยการจัดหาอะไหล่ทางบริษัทได้เล็งเห็นความสำคัญของเรื่องนี้ จึงได้มีการนำเข้าอะไหล่สำรองและคู่มือจากประเทศจีน พร้อมด้วยทีมช่างซ่อมบำรุงมาก ประสบการณ์ ที่ได้รับการอบรมจากต่างประเทศ มาให้บริการ อย่างทันท่วงที</p>\r\n', 'banner-aboutus--2.jpg', 'chinathip pondumpai', '2019-10-21 17:02:22', 'chinathip pondumpai', '2019-10-23 18:56:16', 'on', 'ประวัติองค์กร', '<p>เราคือผู้นำในการจำหน่ายรถบัส รถมินิบัส<br />\r\nรถนำเที่ยว ที่ได้มาตรฐานจากกรมขนส่งทางบก ขสมก.</p>\r\n', '', '<p>ด้วยจำนวนพนักงานด้านการขาย และการบริการ 18,400 คนทั่วโลก เราจึงพร้อมให้บริการกับการ ดำเนินธุรกิจของลูกค้าในกว่า 100 ประเทศทั่วโลก</p>\r\n', '<p>ด้วยจำนวนพนักงานกว่า 3,500 คนที่ทำงานด้าน วิเคราะห์และพัฒนา เป้าหมายของเราคือการ พัฒนาผลิตภัณฑ์ที่มีคุณภาพ รวมทั้งรูปแบบ ที่พร้อมตอบโจทย์การใช้งานเฉพาะทางของลูกค้า และการลดเวลาในกระบวนการผลิตให้น้อยที่สุด</p>\r\n', '<p>แผนกจัดซื้อกลางมีสำนักงานท้องถิ่นในละตินอเมริกา อินเดีย รัสเซีย และจีน โรงงานผลิตชิ้นส่วนอะไหล่ ของเราอยู่ที่ Opglabbeek ในเบลเยียม สิงคโปร์และ ที่ Vinhedo ในบราซิล</p>\r\n', 'cbf744a724fa2453af11aedfc9dbe1da.jpg,cbf744a724fa2453af11aedfc9dbe1da1.jpg,cbf744a724fa2453af11aedfc9dbe1da2.jpg', 'พนักงาน 45,000 คน', 'การวิจัยและการพัฒนา', 'การบริการทั่วโลก'),
(77, 'นโยบายและการบริหาร', '555666', '<p>บริษัท แพนเทอรา มอเตอร์ส จำกัด&nbsp;คือบริษัทผู้ผลิตรถบัส NGV ที่มีชื่อเสียง และได้รับมาตรฐานสูงสุดในประเทศจีน จนได้รับรางวัลมากมาย เช่น</p>\r\n\r\n<ul>\r\n	<li>ปี 2007 ได้รับรางวัลที่ 2 ประเภทรถโดยสารประหยัดพลังงานเชื้อเพลิง</li>\r\n	<li>ปี 2008 ได้รับรางวัลจากกระทรวงการก่อสร้างโรงงานผลิตรถบัสที่ดีที่สุด และรางวัลองค์กรที่มีเทคโนโลยีสูงในมณฑลหูเป่ย</li>\r\n	<li>ปี 2009 ได้รับรางวัลผลิตภัณฑ์รถบัสคุณภาพที่ออกโดยสมาคมหูเป่ย</li>\r\n	<li>ปี 2010 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n	<li>ปี 2013 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n</ul>\r\n\r\n<p>ซึ่งรางวัลทั้งหมดที่กล่าวมานั้น เป็นเครื่องการันตีได้ว่าเราคือ &ldquo;ผู้นำการผลิตรถบัสโดยสารในด้านการประหยัดพลังงานเชื้อเพลิงอย่างแท้จริง&rdquo; ซึ่งปัจจุบันมีเครือข่ายอยู่ อยู่หลายประเทศ ก่อตั้งโดยทีมงานที่มีวิสัยทัศน์กว้างไกล และได้เล็งเห็นเรื่องการใช้รถบัสประหยัดพลังงานเชื้อเพลิงเป็นสิ่งสำคัญ จึงได้ระดมทุนกว่า 150 ล้านบาท เพื่อตกลงร่วมมือทางการค้า และดำเนินธุรกิจตามที่ได้ลงนามใน MOU ที่ประเทศจีน เมื่อวันที่ 27 กรกฎาคม 2553 และได้รับการแต่งตั้งให้เป็นตัวแทนจำหน่าย รถโดยสารขนาดใหญ่ และรถบัสทุกประเภท ภายใต้ยี่ห้อ&nbsp;<strong>&ldquo;BONLUCK&rdquo;</strong></p>\r\n\r\n<p>นอกจากนี้ บริษัทฯ สนองตอบต่อความต้องการของลูกค้า โดยได้สร้างทางเลือกให้กับผู้ประกอบการรถโดยสาร รถทัวร์ โดยการนำเข้าแชทซีส์ Yaxing พร้อมเครื่องยนต์คุณภาพดีมาทำการประกอบตัวถังภายในประเทศ เป็นการลดภาษีการนำเข้าส่งผลให้ราคาจำหน่ายถูกลง อีกทั้งยังเป็นการสร้างงานภายในประเทศ อีกด้วย โดยการจัดหาอะไหล่ทางบริษัทได้เล็งเห็นความสำคัญของเรื่องนี้ จึงได้มีการนำเข้าอะไหล่สำรองและคู่มือจากประเทศจีน พร้อมด้วยทีมช่างซ่อมบำรุงมาก ประสบการณ์ ที่ได้รับการอบรมจากต่างประเทศ มาให้บริการ อย่างทันท่วงที</p>\r\n', '<p>บริษัท แพนเทอรา มอเตอร์ส จำกัด&nbsp;คือบริษัทผู้ผลิตรถบัส NGV ที่มีชื่อเสียง และได้รับมาตรฐานสูงสุดในประเทศจีน จนได้รับรางวัลมากมาย เช่น</p>\r\n\r\n<ul>\r\n	<li>ปี 2007 ได้รับรางวัลที่ 2 ประเภทรถโดยสารประหยัดพลังงานเชื้อเพลิง</li>\r\n	<li>ปี 2008 ได้รับรางวัลจากกระทรวงการก่อสร้างโรงงานผลิตรถบัสที่ดีที่สุด และรางวัลองค์กรที่มีเทคโนโลยีสูงในมณฑลหูเป่ย</li>\r\n	<li>ปี 2009 ได้รับรางวัลผลิตภัณฑ์รถบัสคุณภาพที่ออกโดยสมาคมหูเป่ย</li>\r\n	<li>ปี 2010 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n	<li>ปี 2013 ได้รับรางวัล CHINA INTERNATIONAL GOLD และรางวัลพลังงานดีเด่นจาก CHINA</li>\r\n</ul>\r\n\r\n<p>ซึ่งรางวัลทั้งหมดที่กล่าวมานั้น เป็นเครื่องการันตีได้ว่าเราคือ &ldquo;ผู้นำการผลิตรถบัสโดยสารในด้านการประหยัดพลังงานเชื้อเพลิงอย่างแท้จริง&rdquo; ซึ่งปัจจุบันมีเครือข่ายอยู่ อยู่หลายประเทศ ก่อตั้งโดยทีมงานที่มีวิสัยทัศน์กว้างไกล และได้เล็งเห็นเรื่องการใช้รถบัสประหยัดพลังงานเชื้อเพลิงเป็นสิ่งสำคัญ จึงได้ระดมทุนกว่า 150 ล้านบาท เพื่อตกลงร่วมมือทางการค้า และดำเนินธุรกิจตามที่ได้ลงนามใน MOU ที่ประเทศจีน เมื่อวันที่ 27 กรกฎาคม 2553 และได้รับการแต่งตั้งให้เป็นตัวแทนจำหน่าย รถโดยสารขนาดใหญ่ และรถบัสทุกประเภท ภายใต้ยี่ห้อ&nbsp;<strong>&ldquo;BONLUCK&rdquo;</strong></p>\r\n\r\n<p>นอกจากนี้ บริษัทฯ สนองตอบต่อความต้องการของลูกค้า โดยได้สร้างทางเลือกให้กับผู้ประกอบการรถโดยสาร รถทัวร์ โดยการนำเข้าแชทซีส์ Yaxing พร้อมเครื่องยนต์คุณภาพดีมาทำการประกอบตัวถังภายในประเทศ เป็นการลดภาษีการนำเข้าส่งผลให้ราคาจำหน่ายถูกลง อีกทั้งยังเป็นการสร้างงานภายในประเทศ อีกด้วย โดยการจัดหาอะไหล่ทางบริษัทได้เล็งเห็นความสำคัญของเรื่องนี้ จึงได้มีการนำเข้าอะไหล่สำรองและคู่มือจากประเทศจีน พร้อมด้วยทีมช่างซ่อมบำรุงมาก ประสบการณ์ ที่ได้รับการอบรมจากต่างประเทศ มาให้บริการ อย่างทันท่วงที</p>\r\n', '59b5be5a43f868ef3251dece77b85c90.jpg', 'chinathip pondumpai', '2019-10-21 17:03:25', 'chinathip pondumpai', '2019-10-22 11:32:07', 'on', 'นโยบายและการบริหาร', '', '', '', '', '', '', '', '', ''),
(227, 'dd', '', '', '', 'img-31fe2c41ea8eae9589ff64ebad9cd6f9.jpg', 'chinathip pondumpai', '2019-10-23 13:49:11', '', '0000-00-00 00:00:00', 'off', 'ประวัติองค์กร', '', '', '', '', '', ',,', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `activity_start` date NOT NULL,
  `activity_end` date NOT NULL,
  `img_activity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `name_thai`, `name_eng`, `detail`, `detail_eng`, `activity_start`, `activity_end`, `img_activity`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`) VALUES
(1, '55', '55', '<p>77755</p>\r\n', '', '2019-10-25', '2019-10-26', 'company.jpg', '2019-10-03 10:52:32', 'surachai uold', '2019-10-07 13:47:58', 'surachai uold', 'on'),
(2, '444', '44', '<p>444</p>\r\n', '', '2019-10-09', '2019-10-09', 'google_company.jpeg', '2019-10-07 06:37:32', 'surachai uold', '0000-00-00 00:00:00', '', 'off'),
(3, 'dd', 'd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', '2019-10-08', '2019-10-09', 'd69c56fe394249dcbde69047909d2aed.png', '2019-10-24 08:24:22', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(4, 'dd', 'd', '<p>dd</p>\r\n', '<p>d</p>\r\n', '2019-10-16', '2019-10-06', 'b532ebc8e0893457c4c78d42ceaa88ca.jpeg', '2019-10-24 08:24:32', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(5, 'dd', 'd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', '2019-10-12', '2019-10-23', '8e82bec82b854751e758637884b026df.png', '2019-10-24 08:24:44', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(6, 'dd', 'd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', '2019-10-11', '2019-10-12', '875e7ebafa08ba44297e79698ffb3908.jpeg', '2019-10-24 08:24:57', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(7, 'dd', 'dd', '<p>dd</p>\r\n', '<p>d</p>\r\n', '2019-10-12', '2019-10-12', '65908d7d4caa29d6a17b9f3935957a66.jpg', '2019-10-24 08:25:09', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `img_article` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `name_thai`, `name_eng`, `detail`, `detail_eng`, `img_article`, `video`, `link_youtube`, `created_by`, `created_at`, `updated_by`, `updated_at`, `status`) VALUES
(147, '444', '44', '<p>44</p>\r\n', '', '2b2eabb02b3be664c9d3337b94f7ed85.jpg', '0120802c0d76a5094c326dea36ac3326.mp4', '', 'surachai uold', '2019-10-09 04:12:39', 'surachai uold', '2019-10-09 11:24:46', 'on'),
(148, '555', '555', '<p>555</p>\r\n', '', 'f301ed2a89bd63cbe59689955ecb7f10.png', NULL, '', 'surachai uold', '2019-10-09 04:29:07', '', '0000-00-00 00:00:00', 'on'),
(149, '3333', '33', '<p>333</p>\r\n', '<p>33</p>\r\n', 'd7785ef893b91d83b8de4af8271ce753.jpg', 'd7785ef893b91d83b8de4af8271ce753.mp4', '', 'chinathip pondumpai', '2019-10-10 17:12:54', '', '0000-00-00 00:00:00', 'on'),
(150, 'sss', 'sss', '<p>sss</p>\r\n', '<p>ss</p>\r\n', 'cf070e547b7c33a1d76b43474600b3c4.jpg', NULL, 'https://www.youtube.com/watch?v=VkfMKWY1aT0', 'chinathip pondumpai', '2019-10-23 18:58:58', 'chinathip pondumpai', '2019-10-24 01:59:43', 'on'),
(151, 'hhhh', 'hhh', '<p>hhh</p>\r\n', '<p>hh</p>\r\n', '781372da4e84312cbdb3f90f2158b02c.png', NULL, 'https://www.youtube.com/watch?v=VkfMKWY1aT0', 'chinathip pondumpai', '2019-10-24 08:18:37', '', '0000-00-00 00:00:00', 'on'),
(152, '55', '55', '<p>55</p>\r\n', '<p>55</p>\r\n', 'd3d8f8e9183e864c9249486b7546878c.jpg', NULL, 'https://www.youtube.com/watch?v=VkfMKWY1aT0', 'chinathip pondumpai', '2019-10-24 08:18:48', '', '0000-00-00 00:00:00', 'on'),
(153, '555', '5', '<p>55</p>\r\n', '<p>55</p>\r\n', 'f0ecf37908ebf98613348e2e92becd58.jpg', NULL, '55', 'chinathip pondumpai', '2019-10-24 08:18:59', '', '0000-00-00 00:00:00', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img_banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_img` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `name_thai`, `name_eng`, `img_banner`, `link_img`, `position`, `created_by`, `created_at`, `updated_by`, `updated_at`, `status`) VALUES
(1, '99988', '55588', 'ba2a44beaa6ae5218c3c6ba622e88f18.jpeg', 'banner', 'หน้าแรก', 'chinathip pondumpai', '2019-10-10 04:03:02', 'chinathip pondumpai', '2019-10-10 11:25:17', 'off'),
(3, '444', '444', 'c80dcc8cfb49d4c542563867d1ea2fc4.jpg', '77', '44', 'chinathip pondumpai', '2019-10-10 11:13:54', 'chinathip pondumpai', '2019-10-10 18:14:13', 'on'),
(4, '11', '11', '36d58c6d774162bf28e0d0134162cf6a.png', '11', '11', 'chinathip pondumpai', '2019-10-10 11:33:00', 'chinathip pondumpai', '2019-10-10 18:33:06', 'on'),
(5, '1412', '22', 'c50bd5ba22f283dbd00ebefa7adf280c.jpg', '22', '2', 'chinathip pondumpai', '2019-10-10 11:51:11', 'chinathip pondumpai', '2019-10-10 18:51:18', 'off'),
(6, '444', '44', '0e657953760de4ad782f63a6d39a34e6.jpg', '4', '4', 'chinathip pondumpai', '2019-10-10 14:02:44', '', '0000-00-00 00:00:00', 'on'),
(7, 'ss', 'ss', '6cf5bf25b6a8053bf0221dcd70bc06d4.jpg', 'ss', 's', 'chinathip pondumpai', '2019-10-10 14:45:31', 'chinathip pondumpai', '2019-10-10 21:45:47', 'off'),
(8, '11', '11', '917a4ffd8e870c91337c4fa0b0f4fa98.jpg', '11', '11', 'chinathip pondumpai', '2019-10-10 14:47:29', '', '0000-00-00 00:00:00', 'on'),
(9, '22', '222', 'c00bbaa155e72d0f46b8c84e0c15f3bb.jpg', '222', '22', 'chinathip pondumpai', '2019-10-10 17:08:28', '', '0000-00-00 00:00:00', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` text NOT NULL,
  `tags` text NOT NULL,
  `category` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL,
  `author` varchar(100) NOT NULL,
  `viewers` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `slug`, `content`, `image`, `tags`, `category`, `status`, `author`, `viewers`, `created_at`, `updated_at`) VALUES
(1, 'Hello Wellcome To Cicool Builder', 'Hello-Wellcome-To-Ciool-Builder', 'greetings from our team I hope to be happy! ', 'wellcome.jpg', 'greetings', '1', 'publish', 'admin', 0, '2019-09-26 16:49:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `category_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`category_id`, `category_name`, `category_desc`) VALUES
(1, 'Technology', ''),
(2, 'Lifestyle', '');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date_booking` datetime NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` int(11) UNSIGNED NOT NULL,
  `captcha_time` int(10) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cc_options`
--

CREATE TABLE `cc_options` (
  `id` int(11) UNSIGNED NOT NULL,
  `option_name` varchar(200) NOT NULL,
  `option_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_options`
--

INSERT INTO `cc_options` (`id`, `option_name`, `option_value`) VALUES
(1, 'active_theme', 'cicool'),
(2, 'favicon', 'default.png'),
(3, 'site_name', 'minibus');

-- --------------------------------------------------------

--
-- Table structure for table `cc_session`
--

CREATE TABLE `cc_session` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name_thai`, `name_eng`, `img_color`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`) VALUES
(3, 'Gray', 'Gray', '00f06a876699cea590eccc53a741607b.jpg', '2019-10-09 06:24:37', 'surachai uold', '2019-10-25 07:42:10', 'chinathip pondumpai', 'on'),
(4, 'White Diamond', 'White Diamond', '0df94fd2c79b3e809925dbd624ae8a9b.jpeg', '2019-10-09 06:33:24', 'surachai uold', '2019-10-25 15:57:54', 'chinathip pondumpai', 'on'),
(12, 'Black', 'Black', '1dcda816d587e2221373417281b5f5b4.png', '2019-10-25 00:42:30', 'chinathip pondumpai', '2019-10-25 16:02:12', 'chinathip pondumpai', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crud`
--

CREATE TABLE `crud` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `primary_key` varchar(200) NOT NULL,
  `page_read` varchar(20) DEFAULT NULL,
  `page_create` varchar(20) DEFAULT NULL,
  `page_update` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud`
--

INSERT INTO `crud` (`id`, `title`, `subject`, `table_name`, `primary_key`, `page_read`, `page_create`, `page_update`) VALUES
(1, 'About', 'About', 'about', 'id', 'yes', 'yes', 'yes'),
(2, 'Activity', 'Activity', 'activity', 'id', 'yes', 'yes', 'yes'),
(3, 'Article', 'Article', 'article', 'id', 'yes', 'yes', 'yes'),
(4, 'Users', 'Users', 'users', 'id', 'yes', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `crud_custom_option`
--

CREATE TABLE `crud_custom_option` (
  `id` int(11) UNSIGNED NOT NULL,
  `crud_field_id` int(11) NOT NULL,
  `crud_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `option_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `crud_field`
--

CREATE TABLE `crud_field` (
  `id` int(11) UNSIGNED NOT NULL,
  `crud_id` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `input_type` varchar(200) NOT NULL,
  `show_column` varchar(10) DEFAULT NULL,
  `show_add_form` varchar(10) DEFAULT NULL,
  `show_update_form` varchar(10) DEFAULT NULL,
  `show_detail_page` varchar(10) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `relation_table` varchar(200) DEFAULT NULL,
  `relation_value` varchar(200) DEFAULT NULL,
  `relation_label` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_field`
--

INSERT INTO `crud_field` (`id`, `crud_id`, `field_name`, `field_label`, `input_type`, `show_column`, `show_add_form`, `show_update_form`, `show_detail_page`, `sort`, `relation_table`, `relation_value`, `relation_label`) VALUES
(1, 1, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(2, 1, 'detail', 'detail', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(3, 2, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(4, 2, 'name', 'name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(5, 2, 'detail', 'detail', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(6, 2, 'img_activity', 'img_activity', 'file', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7, 2, 'date_start', 'date_start', 'timestamp', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(8, 2, 'date_end', 'date_end', 'datetime', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(9, 3, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(10, 3, 'name', 'name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(11, 3, 'detail', 'detail', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(12, 3, 'date_start', 'date_start', 'timestamp', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(13, 3, 'img_article', 'img_article', 'file', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(14, 3, 'video', 'video', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(15, 4, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(16, 4, 'name', 'name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(17, 4, 'surname', 'surname', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(18, 4, 'username', 'username', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(19, 4, 'password', 'password', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(20, 4, 'email', 'email', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(21, 4, 'address', 'address', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 7, '', '', ''),
(22, 4, 'tel', 'tel', 'input', 'yes', 'yes', 'yes', 'yes', 8, '', '', ''),
(23, 4, 'facebook_id', 'facebook_id', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 9, '', '', ''),
(24, 4, 'line_id', 'line_id', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 10, '', '', ''),
(25, 4, 'type_user', 'type_user', 'input', 'yes', 'yes', 'yes', 'yes', 11, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `crud_field_validation`
--

CREATE TABLE `crud_field_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `crud_field_id` int(11) NOT NULL,
  `crud_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_field_validation`
--

INSERT INTO `crud_field_validation` (`id`, `crud_field_id`, `crud_id`, `validation_name`, `validation_value`) VALUES
(1, 2, 1, 'required', ''),
(2, 4, 2, 'required', ''),
(3, 4, 2, 'max_length', '50'),
(4, 5, 2, 'required', ''),
(5, 6, 2, 'required', ''),
(6, 6, 2, 'max_length', '255'),
(7, 7, 2, 'required', ''),
(8, 8, 2, 'required', ''),
(9, 10, 3, 'required', ''),
(10, 10, 3, 'max_length', '50'),
(11, 11, 3, 'required', ''),
(12, 12, 3, 'required', ''),
(13, 13, 3, 'required', ''),
(14, 13, 3, 'max_length', '255'),
(15, 14, 3, 'required', ''),
(16, 14, 3, 'max_length', '255'),
(17, 16, 4, 'required', ''),
(18, 16, 4, 'max_length', '50'),
(19, 17, 4, 'required', ''),
(20, 17, 4, 'max_length', '50'),
(21, 18, 4, 'required', ''),
(22, 18, 4, 'max_length', '50'),
(23, 19, 4, 'required', ''),
(24, 19, 4, 'max_length', '50'),
(25, 20, 4, 'required', ''),
(26, 20, 4, 'max_length', '50'),
(27, 21, 4, 'required', ''),
(28, 22, 4, 'required', ''),
(29, 22, 4, 'max_length', '10'),
(30, 23, 4, 'required', ''),
(31, 24, 4, 'required', ''),
(32, 25, 4, 'required', ''),
(33, 25, 4, 'max_length', '30');

-- --------------------------------------------------------

--
-- Table structure for table `crud_input_type`
--

CREATE TABLE `crud_input_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(200) NOT NULL,
  `relation` varchar(20) NOT NULL,
  `custom_value` int(11) NOT NULL,
  `validation_group` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_input_type`
--

INSERT INTO `crud_input_type` (`id`, `type`, `relation`, `custom_value`, `validation_group`) VALUES
(1, 'input', '0', 0, 'input'),
(2, 'textarea', '0', 0, 'text'),
(3, 'select', '1', 0, 'select'),
(4, 'editor_wysiwyg', '0', 0, 'editor'),
(5, 'password', '0', 0, 'password'),
(6, 'email', '0', 0, 'email'),
(7, 'address_map', '0', 0, 'address_map'),
(8, 'file', '0', 0, 'file'),
(9, 'file_multiple', '0', 0, 'file_multiple'),
(10, 'datetime', '0', 0, 'datetime'),
(11, 'date', '0', 0, 'date'),
(12, 'timestamp', '0', 0, 'timestamp'),
(13, 'number', '0', 0, 'number'),
(14, 'yes_no', '0', 0, 'yes_no'),
(15, 'time', '0', 0, 'time'),
(16, 'year', '0', 0, 'year'),
(17, 'select_multiple', '1', 0, 'select_multiple'),
(18, 'checkboxes', '1', 0, 'checkboxes'),
(19, 'options', '1', 0, 'options'),
(20, 'true_false', '0', 0, 'true_false'),
(21, 'current_user_username', '0', 0, 'user_username'),
(22, 'current_user_id', '0', 0, 'current_user_id'),
(23, 'custom_option', '0', 1, 'custom_option'),
(24, 'custom_checkbox', '0', 1, 'custom_checkbox'),
(25, 'custom_select_multiple', '0', 1, 'custom_select_multiple'),
(26, 'custom_select', '0', 1, 'custom_select');

-- --------------------------------------------------------

--
-- Table structure for table `crud_input_validation`
--

CREATE TABLE `crud_input_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `validation` varchar(200) NOT NULL,
  `input_able` varchar(20) NOT NULL,
  `group_input` text NOT NULL,
  `input_placeholder` text NOT NULL,
  `call_back` varchar(10) NOT NULL,
  `input_validation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_input_validation`
--

INSERT INTO `crud_input_validation` (`id`, `validation`, `input_able`, `group_input`, `input_placeholder`, `call_back`, `input_validation`) VALUES
(1, 'required', 'no', 'input, file, number, text, datetime, select, password, email, editor, date, yes_no, time, year, select_multiple, options, checkboxes, true_false, address_map, custom_option, custom_checkbox, custom_select_multiple, custom_select, file_multiple', '', '', ''),
(2, 'max_length', 'yes', 'input, number, text, select, password, email, editor, yes_no, time, year, select_multiple, options, checkboxes, address_map', '', '', 'numeric'),
(3, 'min_length', 'yes', 'input, number, text, select, password, email, editor, time, year, select_multiple, address_map', '', '', 'numeric'),
(4, 'valid_email', 'no', 'input, email', '', '', ''),
(5, 'valid_emails', 'no', 'input, email', '', '', ''),
(6, 'regex', 'yes', 'input, number, text, datetime, select, password, email, editor, date, yes_no, time, year, select_multiple, options, checkboxes', '', 'yes', 'callback_valid_regex'),
(7, 'decimal', 'no', 'input, number, text, select', '', '', ''),
(8, 'allowed_extension', 'yes', 'file, file_multiple', 'ex : jpg,png,..', '', 'callback_valid_extension_list'),
(9, 'max_width', 'yes', 'file, file_multiple', '', '', 'numeric'),
(10, 'max_height', 'yes', 'file, file_multiple', '', '', 'numeric'),
(11, 'max_size', 'yes', 'file, file_multiple', '... kb', '', 'numeric'),
(12, 'max_item', 'yes', 'file_multiple', '', '', 'numeric'),
(13, 'valid_url', 'no', 'input, text', '', '', ''),
(14, 'alpha', 'no', 'input, text, select, password, editor, yes_no', '', '', ''),
(15, 'alpha_numeric', 'no', 'input, number, text, select, password, editor', '', '', ''),
(16, 'alpha_numeric_spaces', 'no', 'input, number, text,select, password, editor', '', '', ''),
(17, 'valid_number', 'no', 'input, number, text, password, editor, true_false', '', 'yes', ''),
(18, 'valid_datetime', 'no', 'input, datetime, text', '', 'yes', ''),
(19, 'valid_date', 'no', 'input, datetime, date, text', '', 'yes', ''),
(20, 'valid_max_selected_option', 'yes', 'select_multiple, custom_select_multiple, custom_checkbox, checkboxes', '', 'yes', 'numeric'),
(21, 'valid_min_selected_option', 'yes', 'select_multiple, custom_select_multiple, custom_checkbox, checkboxes', '', 'yes', 'numeric'),
(22, 'valid_alpha_numeric_spaces_underscores', 'no', 'input, text,select, password, editor', '', 'yes', ''),
(23, 'matches', 'yes', 'input, number, text, password, email', 'any field', 'no', 'callback_valid_alpha_numeric_spaces_underscores'),
(24, 'valid_json', 'no', 'input, text, editor', '', 'yes', ' '),
(25, 'valid_url', 'no', 'input, text, editor', '', 'no', ' '),
(26, 'exact_length', 'yes', 'input, text, number', '0 - 99999*', 'no', 'numeric'),
(27, 'alpha_dash', 'no', 'input, text', '', 'no', ''),
(28, 'integer', 'no', 'input, text, number', '', 'no', ''),
(29, 'differs', 'yes', 'input, text, number, email, password, editor, options, select', 'any field', 'no', 'callback_valid_alpha_numeric_spaces_underscores'),
(30, 'is_natural', 'no', 'input, text, number', '', 'no', ''),
(31, 'is_natural_no_zero', 'no', 'input, text, number', '', 'no', ''),
(32, 'less_than', 'yes', 'input, text, number', '', 'no', 'numeric'),
(33, 'less_than_equal_to', 'yes', 'input, text, number', '', 'no', 'numeric'),
(34, 'greater_than', 'yes', 'input, text, number', '', 'no', 'numeric'),
(35, 'greater_than_equal_to', 'yes', 'input, text, number', '', 'no', 'numeric'),
(36, 'in_list', 'yes', 'input, text, number, select, options', '', 'no', 'callback_valid_multiple_value'),
(37, 'valid_ip', 'no', 'input, text', '', 'no', '');

-- --------------------------------------------------------

--
-- Table structure for table `dealer`
--

CREATE TABLE `dealer` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address_thai` text COLLATE utf8_unicode_ci NOT NULL,
  `address_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `img_dealer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dealer`
--

INSERT INTO `dealer` (`id`, `name_thai`, `name_eng`, `address_thai`, `address_eng`, `img_dealer`, `tel`, `created_by`, `created_at`, `updated_by`, `updated_at`, `status`) VALUES
(4, 'บริษัท วี.เอ็ม.เอส. คาร์ เซลล์ จำกัด (สำนักงานใหญ่', 'บริษัท วี.เอ็ม.เอส. คาร์ เซลล์ จำกัด (สำนักงานใหญ่', '<p>70 ถนนนวมินทร์ แขวงคลองกุ่ม เขตบึงกุ่ม กรุงเทพมหานคร 10240</p>\r\n', '<p>70 ถนนนวมินทร์ แขวงคลองกุ่ม เขตบึงกุ่ม กรุงเทพมหานคร 10240</p>\r\n', '', '02222', 'chinathip pondumpai', '2019-10-24 01:44:38', 'chinathip pondumpai', '2019-10-24 09:03:38', 'on'),
(5, 'บริษัท มิตซูวิภาวดี จำกัด (สำนักงานใหญ่)', 'บริษัท มิตซูวิภาวดี จำกัด (สำนักงานใหญ่)', '<p>4 ซอยวิภาวดีรังสิต 32 ถนนวิภาวดีรังสิต แขวงจตุจักร เขตจตุจักร กรุงเทพมหานคร 10900</p>\r\n', '<p>4 ซอยวิภาวดีรังสิต 32 ถนนวิภาวดีรังสิต แขวงจตุจักร เขตจตุจักร กรุงเทพมหานคร 10900</p>\r\n', '', '02222', 'chinathip pondumpai', '2019-10-24 01:45:43', 'chinathip pondumpai', '2019-10-24 08:48:24', 'on'),
(6, 'บริษัท กวงไถ่ มอเตอร์กรุ๊ป จำกัด (สำนักงานใหญ่)', 'บริษัท กวงไถ่ มอเตอร์กรุ๊ป จำกัด (สำนักงานใหญ่)', '<p>855 ซอย ลาดพร้าว 87 (จันทราสุข) ถนนรามอินทราอาจณรงค์ แขวงคลองจั่น เขตบางกะปิ กรุงเทพมหานคร 10240</p>\r\n', '', '', '02222', 'chinathip pondumpai', '2019-10-24 01:46:20', '', '0000-00-00 00:00:00', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `form_custom_attribute`
--

CREATE TABLE `form_custom_attribute` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `attribute_value` text NOT NULL,
  `attribute_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `form_custom_option`
--

CREATE TABLE `form_custom_option` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `option_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `form_field`
--

CREATE TABLE `form_field` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `input_type` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `placeholder` text,
  `auto_generate_help_block` varchar(10) DEFAULT NULL,
  `help_block` text,
  `relation_table` varchar(200) DEFAULT NULL,
  `relation_value` varchar(200) DEFAULT NULL,
  `relation_label` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `form_field_validation`
--

CREATE TABLE `form_field_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL,
  `is_private_key` tinyint(1) NOT NULL,
  `ip_addresses` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, '8F3ADDAC269063D77D2CE094C21D6E25', 0, 0, 0, NULL, '2019-09-26 09:49:24');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) UNSIGNED NOT NULL,
  `label` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `icon_color` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `menu_type_id` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `label`, `type`, `icon_color`, `link`, `sort`, `parent`, `icon`, `menu_type_id`, `active`) VALUES
(1, 'MAIN NAVIGATION', 'label', '', 'administrator/dashboard', 1, 0, '', 1, 1),
(2, 'Dashboard', 'menu', '', 'administrator/dashboard', 2, 0, 'fa-dashboard', 1, 1),
(3, 'CRUD Builder', 'menu', '', 'administrator/crud', 3, 0, 'fa-table', 1, 1),
(4, 'API Builder', 'menu', '', 'administrator/rest', 4, 0, 'fa-code', 1, 1),
(5, 'Page Builder', 'menu', '', 'administrator/page', 5, 0, 'fa-file-o', 1, 1),
(6, 'Form Builder', 'menu', '', 'administrator/form', 6, 0, 'fa-newspaper-o', 1, 1),
(7, 'Blog', 'menu', '', 'administrator/blog', 7, 0, 'fa-file-text-o', 1, 1),
(8, 'Menu', 'menu', '', 'administrator/menu', 8, 0, 'fa-bars', 1, 1),
(9, 'Auth', 'menu', '', '', 9, 0, 'fa-shield', 1, 1),
(10, 'User', 'menu', '', 'administrator/user', 10, 9, '', 1, 1),
(11, 'Groups', 'menu', '', 'administrator/group', 11, 9, '', 1, 1),
(12, 'Access', 'menu', '', 'administrator/access', 12, 9, '', 1, 1),
(13, 'Permission', 'menu', '', 'administrator/permission', 13, 9, '', 1, 1),
(14, 'API Keys', 'menu', '', 'administrator/keys', 14, 9, '', 1, 1),
(15, 'Extension', 'menu', '', 'administrator/extension', 15, 0, 'fa-puzzle-piece', 1, 1),
(16, 'OTHER', 'label', '', '', 16, 0, '', 1, 1),
(17, 'Settings', 'menu', 'text-red', 'administrator/setting', 17, 0, 'fa-circle-o', 1, 1),
(18, 'Web Documentation', 'menu', 'text-blue', 'administrator/doc/web', 18, 0, 'fa-circle-o', 1, 1),
(19, 'API Documentation', 'menu', 'text-yellow', 'administrator/doc/api', 19, 0, 'fa-circle-o', 1, 1),
(20, 'Home', 'menu', '', '/', 1, 0, '', 2, 1),
(21, 'Blog', 'menu', '', 'blog', 4, 0, '', 2, 1),
(22, 'Dashboard', 'menu', '', 'administrator/dashboard', 5, 0, '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_type`
--

CREATE TABLE `menu_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_type`
--

INSERT INTO `menu_type` (`id`, `name`, `definition`) VALUES
(1, 'side menu', NULL),
(2, 'top menu', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `img_news` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name_thai`, `name_eng`, `detail`, `detail_eng`, `img_news`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`) VALUES
(3, 'ข้อเสนอสุดพิเศษสำหรับมิตซูบิชิ ปาเจโรสปอร์ต ใหม่', '555', '<p>55</p>\r\n', '', '1b3e3f5720279496e3c1451c41b251f0.jpg', '2019-10-07 06:36:09', 'surachai uold', '2019-10-17 15:52:33', 'chinathip pondumpai', 'on'),
(4, 'เราดูแลคุณแค่ขับ พร้อมรับหน้าฝนอย่างมั่นใจ', '', '', '', 'ab310936381f6001a89d764000142513.jpg', '2019-10-17 09:00:12', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(5, 'มิตซูบิชิ แฮปปี้ แฟน ลุ้นทองถึงใจ แจกใหญ่ทุกเดือน', '', '<p>โปรโมชั่นรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ มีผลตั้งแต่วันที่ 25 กรกฏาคม 2562 &ndash; 30 กันยายน 2562 มีรายละเอียดดังนี้</p>\r\n\r\n<ul>\r\n	<li>ดอกเบี้ยพิเศษ 1.99% (1)</li>\r\n	<li>ฟรี ประกันภัยชั้นหนึ่ง นาน 1 ปี (2)</li>\r\n	<li>ฟรี รับประกันคุณภาพ 5 ปี (3)</li>\r\n	<li>ฟรี ค่าแรงเช็กระยะนาน 5 ปี (4)</li>\r\n	<li>ฟรี อุปกรณ์เชื่อมต่อสัญญาณภาพและเสียงระบบดิจิตอล (HDMI WiFi Dongle) (5)</li>\r\n</ul>\r\n\r\n<ol>\r\n	<li>สำหรับลูกค้าที่จองรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020) ตั้งแต่วันที่ 25 ก.ค. 62 &ndash; 30 ก.ย. 62 และออกรถภายในวันที่ 31 ต.ค. 62 เลือกรับดอกเบี้ยพิเศษ ในอัตราร้อยละ 1.99 สำหรับลูกค้าที่ชำระเงินดาวน์เริ่มต้นในอัตราร้อยละ 25 ของราคารถยนต์ และผ่อนชำระค่าเช่าซื้อทั้งหมดเป็นจำนวน 48 งวดเท่านั้น (เงื่อนไขและ รายละเอียดการให้สินเชื่อเช่าซื้อโปรดสอบถามเพิ่มเติมได้ที่สถาบันการเงินที่ร่วมรายการ ได้แก่ มิตซู ลิสซิ่ง ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) ธนาคารธนชาติ จำกัด (มหาชน) บริษัท ลีสซิ่งกสิกรไทย จำกัด ธนาคารไทยพาณิชย์ จำกัด (มหาชน) ธนาคาร ไอซีบีซี (ไทย) จำกัด (มหาชน) ธนาคารทิสโก้ จำกัด (มหาชน) และ ธนาคาร เกียรตินาคิน จำกัด (มหาชน)</li>\r\n	<li>สำหรับรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต และ มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020)<br />\r\n	รับฟรี ค่าเบี้ยประกันภัยชั้นหนึ่งไดมอนด์ โพรเทคชั่น เป็นระยะเวลา 1 ปี มูลค่าสูงสุด 28,499 บาท ทั้งนี้ เงื่อนไขการรับประกันภัยและทุนประกันภัยเป็นไปตามกรมธรรม์ ประกันภัยที่บริษัทรับประกันกำหนด และเงื่อนไขของกรมธรรม์แต่ละฉบับจะมีความแตกต่างกันตามมูลค่าของรถยนต์ที่เอาประกันภัย</li>\r\n	<li>รับฟรี การรับประกันคุณภาพรถยนต์ (Diamond Warranty) 5 ปี หรือ 100,000 กิโลเมตร (แล้วแต่ระยะใดจะถึงก่อน) ระยะเวลาการรับประกันของชิ้นส่วนและอุปกรณ์ แต่ละชนิดอาจแตกต่างกันตามที่ระบุไว้ในสมุดรับบริการและคู่มือการใช้รถ ซึ่งรับประกันโดยบริษัท มิตซูบิชิ มอเตอร์ส (ประเทศไทย) จำกัด โปรดศึกษารายละเอียดการ รับประกันเพิ่มเติมในคู่มือรถ โดยลูกค้าสามารถเข้ารับบริการที่ศูนย์บริการมาตรฐานของผู้จำหน่ายมิตซูบิชิที่ได้รับการแต่งตั้งเท่านั้น</li>\r\n	<li>รับรายการฟรีค่าแรงเช็กระยะนาน 5 ปี หรือ 100,000 กิโลเมตร (แล้วแต่ระยะใดจะถึงก่อน) มูลค่าสูงสุด 8,150 บาท อัตราค่าแรงที่นำมาคำนวณอ้างอิงจากอัตราค่าแรง กลาง บริการฟรีเฉพาะค่าแรงเช็กระยะตามที่กำหนดไว้ในบัตรตรวจเช็กระยะฟรีในสมุดรับบริการและคู่มือการใช้รถ ซึ่งรถยนต์ของลูกค้าจะได้รับการตรวจสอบและบำรุง รักษาตามรายการที่ระบุไว้ โดยลูกค้าสามารถเข้ารับบริการที่ศูนย์บริการมาตรฐานของผู้จำหน่ายมิตซูบิชิที่ได้รับการแต่งตั้งเท่านั้น</li>\r\n	<li>สำหรับลูกค้าที่จองรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020) รุ่น 2WD 2.4D GT-Premium และ 4WD 2.4D GT-Premium ตั้งแต่วันที่ 25 ก.ค. 62 ถึง 31 ต.ค.62 และออกรถภายในวันที่ 30 พ.ย. 62 รับฟรี อุปกรณ์เชื่อมต่อสัญญาณภาพและเสียงระบบดิจิตอล (HDMI WiFi Dongle) จำนวน 1 ชิ้น มูลค่า 1,690 บาท ในวันออกรถ ณ ศูนย์บริการรถยนต์มิตซูบิชิที่ท่านออกรถ</li>\r\n</ol>\r\n', '', '6ae535ce197246f53d2d4af97b58b156.jpg', '2019-10-17 09:01:13', 'chinathip pondumpai', '2019-10-21 21:55:31', 'chinathip pondumpai', 'on'),
(6, 'sss', '', '<p>ss</p>\r\n', '<p>ss</p>\r\n', '9bff9698609d92a0e2f7b29c58858b7f.jpg', '2019-10-23 17:23:34', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(7, 'dd', 'dd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', 'b0094680e38c0718235beb6fe96db0b1.png', '2019-10-24 04:47:09', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(8, 'dd', 'dd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', '7a297d7bc5860e2295112870e52916ea.png', '2019-10-24 04:47:18', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(9, 'dd', 'dd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', '2e987f563ccea0786e7d93c2805b41c9.png', '2019-10-24 04:47:18', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(10, 'dd', 'dd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', '7826a822226cee7a4db2f3e3083f2787.png', '2019-10-24 04:47:27', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(11, 'dd', 'dd', '<p>dd</p>\r\n', '<p>d</p>\r\n', '16e43d8b014e373c28d0c99d73443d00.jpg', '2019-10-24 04:47:36', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(12, 'd', 'dd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', '1d1034c01a465530b3f2a2c26f69a5c7.png', '2019-10-24 04:47:47', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(13, 'dd', 'dd', '<p>dd</p>\r\n', '<p>d</p>\r\n', '49ab3459f4339850686715d4891cb1d0.jpg', '2019-10-24 04:47:56', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on'),
(14, 'dfdf', 'dfdfgdg', '<p>dgdg</p>\r\n', '<p>dgdg</p>\r\n', 'efda58be2cdf6a1c0fc4da757aa65729.png', '2019-10-24 05:01:36', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `qty_buy` int(10) NOT NULL,
  `date_buy` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type_orders` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `fresh_content` text NOT NULL,
  `keyword` text,
  `description` text,
  `link` varchar(200) DEFAULT NULL,
  `template` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_block_element`
--

CREATE TABLE `page_block_element` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image_preview` varchar(200) NOT NULL,
  `block_name` varchar(200) NOT NULL,
  `content_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `type_payment` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `date_pay` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `price_pay` int(10) NOT NULL,
  `slip` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `color_product` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `img_product` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_product` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Exterior` text COLLATE utf8_unicode_ci NOT NULL,
  `Utility` text COLLATE utf8_unicode_ci NOT NULL,
  `Performance` text COLLATE utf8_unicode_ci NOT NULL,
  `Safety` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name_thai`, `name_eng`, `detail`, `detail_eng`, `color_product`, `price`, `qty`, `img_product`, `type_product`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`, `Exterior`, `Utility`, `Performance`, `Safety`) VALUES
(25, 'Panthera Motors Minibus', 'Panthera Motors Minibus', '', '', '12,4,3', 1950000, 50, 'add86894f7b053e6ad1fde5e2efab6c5.png', 'รุ่นเครื่องยนต์ดีเซล', '2019-10-22 05:16:08', 'chinathip pondumpai', '2019-10-22 14:11:31', 'chinathip pondumpai', 'on', '<p>ดีไซน์ใหม่ ทันสมัย โดดเด่นทุกมุมมอง พร้อมการออกแบบ<br />\r\nอันเป็นเอกลักษณ์ด้วย&nbsp;ADVANCED SHIELD DESIGN</p>\r\n\r\n<p>รถโดยสารอเนกประสงค์&nbsp;ความลงตัวของการเดินทาง</p>\r\n', '<p>ห้องโดยสารกว้างขวางสะดวกสบายตลอดการเดินทาง</p>\r\n\r\n<p>เพื่อให้ทุกการเดินทางเป็นไปได้อย่างราบรื่น</p>\r\n', '<p>ขุมพลังแรง<br />\r\nสะดวกสบายตลอดการเดินทาง</p>\r\n\r\n<p>กำลังสูงสุด 100 กิโลวัตต์ (136)&nbsp;แรงม้าที่ 3,000 รอบ/นาที<br />\r\nแรงบิดสูงสุด 353 นิวตัน-เมตรที่ 1,600 รอบ/นาที</p>\r\n', '<p>เหนือกว่าด้วยระบบความปลอดภัยรอบคัน&nbsp;เพิ่มความมั่นใจสูงสุดในทุกเส้นทาง</p>\r\n\r\n<p>เทคโนโลยีมาตรฐานความปลอดภัย ที่มีคุณภาพ</p>\r\n'),
(26, 'Panthera Motors Minibus', 'Panthera Motors Minibus', '', '', '12,4', 1750000, 50, 'a2692a1c28155f1296736da07ec970dd.png', 'รุ่นเครื่องยนตร์ ngv', '2019-10-22 05:16:53', 'chinathip pondumpai', '2019-10-22 14:06:16', 'chinathip pondumpai', 'on', '<p>ดีไซน์ใหม่ ทันสมัย โดดเด่นทุกมุมมอง พร้อมการออกแบบ<br />\r\nอันเป็นเอกลักษณ์ด้วย&nbsp;ADVANCED SHIELD DESIGN</p>\r\n\r\n<p>รถโดยสารอเนกประสงค์&nbsp;ความลงตัวของการเดินทาง</p>\r\n', '<p>ห้องโดยสารกว้างขวางสะดวกสบายตลอดการเดินทาง</p>\r\n\r\n<p>เพื่อให้ทุกการเดินทางเป็นไปได้อย่างราบรื่น</p>\r\n', '<p>ขุมพลังแรง<br />\r\nสะดวกสบายตลอดการเดินทาง</p>\r\n\r\n<p>กำลังสูงสุด 100 กิโลวัตต์ (136)&nbsp;แรงม้าที่ 3,000 รอบ/นาที<br />\r\nแรงบิดสูงสุด 353 นิวตัน-เมตรที่ 1,600 รอบ/นาที</p>\r\n', '<p>เหนือกว่าด้วยระบบความปลอดภัยรอบคัน&nbsp;เพิ่มความมั่นใจสูงสุดในทุกเส้นทาง</p>\r\n\r\n<p>เทคโนโลยีมาตรฐานความปลอดภัย ที่มีคุณภาพ</p>\r\n'),
(28, 'eee', '', '', '', '3', 10, 11, '9637d71289f119d3d7b0154b72096918.jpg', 'รุ่นเครื่องยนต์ดีเซล', '2019-10-23 15:32:09', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'off', '', '', '', ''),
(29, '999', '44', '', '', '4', 1000, 10, '048cbee4f491d53cd58a452b8d1d8bc1.jpg', 'รุ่นเครื่องยนตร์ ngv', '2019-10-24 10:40:19', 'chinathip pondumpai', '0000-00-00 00:00:00', '', 'off', '<p>10</p>\r\n', '<p>10</p>\r\n', '<p>0</p>\r\n', '<p>10</p>\r\n'),
(30, 'ddd', 'd', '', '', '12,4,3', 10, 2, 'd7ddf0660169ed017a608d40174c577b.jpg', 'รุ่นเครื่องยนตร์ ngv', '2019-10-25 02:17:31', 'chinathip pondumpai', '2019-10-25 14:25:00', 'chinathip pondumpai', 'off', '<p>f</p>\r\n', '<p>f</p>\r\n', '<p>f</p>\r\n', '<p>f</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `promotion_start` date NOT NULL,
  `promotion_end` date NOT NULL,
  `img_promotion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `name_thai`, `name_eng`, `detail`, `detail_eng`, `promotion_start`, `promotion_end`, `img_promotion`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`) VALUES
(4, '444', '444', '<p>444</p>\r\n', '', '2019-10-03', '2019-10-05', 'Doraemon.jpg', '2019-10-03 09:43:51', 'surachai uold', '2019-10-10 11:17:19', 'chinathip pondumpai', 'on'),
(7, 'uu', 'uuu', '<p>11</p>\r\n', '', '2019-10-09', '2019-10-10', 'horses-2904536__340.jpg', '2019-10-06 14:49:28', 'surachai uold', '2019-10-06 22:22:59', 'surachai uold', 'on'),
(8, '55533', '5533', '<p>555333</p>\r\n', '', '2019-10-11', '2019-10-10', 'human.jpg', '2019-10-06 14:53:12', 'surachai uold', '2019-10-06 22:07:37', 'surachai uold', 'on'),
(9, '111', '11', '<p>111</p>\r\n', '', '2019-10-11', '2019-10-12', 'google_company.jpeg', '2019-10-06 15:26:18', 'surachai uold', '2019-10-08 10:54:14', 'surachai uold', 'on'),
(10, '12212', '1212', '<p>15</p>\r\n', '', '2019-10-09', '2019-10-10', 'กระรอกตกใจ.jpg', '2019-10-08 03:54:37', 'surachai uold', '0000-00-00 00:00:00', '', 'on'),
(11, '55', '55', '<p>111</p>\r\n', '<p>1</p>\r\n', '2019-10-10', '2019-10-02', '43c782ad9b80bf04a396d34af2908ccf.jpg', '2019-10-10 17:06:52', 'chinathip pondumpai', '2019-10-11 00:07:45', 'chinathip pondumpai', 'on'),
(12, 'ข้อเสนอสุดพิเศษ วันนี้ ดอกเบี้ยเพียง 1.99%', '', '<p>โปรโมชั่นรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ มีผลตั้งแต่วันที่ 25 กรกฏาคม 2562 &ndash; 30 กันยายน 2562 มีรายละเอียดดังนี้&nbsp;</p>\r\n\r\n<ul>\r\n	<li>&nbsp;ดอกเบี้ยพิเศษ&nbsp;1.99%&nbsp;(1)</li>\r\n	<li>\r\n	<p>ฟรี&nbsp;ประกันภัยชั้นหนึ่ง&nbsp;นาน&nbsp;1&nbsp;ปี&nbsp;(2)</p>\r\n	</li>\r\n	<li>\r\n	<p>ฟรี&nbsp;รับประกันคุณภาพ&nbsp;5&nbsp;ปี&nbsp;(3)</p>\r\n	</li>\r\n	<li>\r\n	<p>ฟรี&nbsp;ค่าแรงเช็กระยะนาน&nbsp;5&nbsp;ปี&nbsp;(4)</p>\r\n	</li>\r\n	<li>\r\n	<p>ฟรี&nbsp;อุปกรณ์เชื่อมต่อสัญญาณภาพและเสียงระบบดิจิตอล&nbsp;(HDMI&nbsp;WiFi&nbsp;Dongle)&nbsp;(5)</p>\r\n	</li>\r\n</ul>\r\n\r\n<ol>\r\n	<li>สำหรับลูกค้าที่จองรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020) ตั้งแต่วันที่ 25 ก.ค. 62 &ndash; 30 ก.ย. 62 และออกรถภายในวันที่ 31 ต.ค. 62 เลือกรับดอกเบี้ยพิเศษ ในอัตราร้อยละ 1.99 สำหรับลูกค้าที่ชำระเงินดาวน์เริ่มต้นในอัตราร้อยละ 25 ของราคารถยนต์ และผ่อนชำระค่าเช่าซื้อทั้งหมดเป็นจำนวน 48 งวดเท่านั้น (เงื่อนไขและ รายละเอียดการให้สินเชื่อเช่าซื้อโปรดสอบถามเพิ่มเติมได้ที่สถาบันการเงินที่ร่วมรายการ ได้แก่ มิตซู ลิสซิ่ง ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน) ธนาคารธนชาติ จำกัด (มหาชน) บริษัท ลีสซิ่งกสิกรไทย จำกัด ธนาคารไทยพาณิชย์ จำกัด (มหาชน) ธนาคาร ไอซีบีซี (ไทย) จำกัด (มหาชน) ธนาคารทิสโก้ จำกัด (มหาชน) และ ธนาคาร เกียรตินาคิน จำกัด (มหาชน)</li>\r\n	<li>สำหรับรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต และ มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020)<br />\r\n	รับฟรี ค่าเบี้ยประกันภัยชั้นหนึ่งไดมอนด์ โพรเทคชั่น เป็นระยะเวลา 1 ปี มูลค่าสูงสุด 28,499 บาท ทั้งนี้ เงื่อนไขการรับประกันภัยและทุนประกันภัยเป็นไปตามกรมธรรม์ ประกันภัยที่บริษัทรับประกันกำหนด และเงื่อนไขของกรมธรรม์แต่ละฉบับจะมีความแตกต่างกันตามมูลค่าของรถยนต์ที่เอาประกันภัย</li>\r\n	<li>รับฟรี การรับประกันคุณภาพรถยนต์ (Diamond Warranty) 5 ปี หรือ 100,000 กิโลเมตร (แล้วแต่ระยะใดจะถึงก่อน) ระยะเวลาการรับประกันของชิ้นส่วนและอุปกรณ์ แต่ละชนิดอาจแตกต่างกันตามที่ระบุไว้ในสมุดรับบริการและคู่มือการใช้รถ ซึ่งรับประกันโดยบริษัท มิตซูบิชิ มอเตอร์ส (ประเทศไทย) จำกัด โปรดศึกษารายละเอียดการ รับประกันเพิ่มเติมในคู่มือรถ โดยลูกค้าสามารถเข้ารับบริการที่ศูนย์บริการมาตรฐานของผู้จำหน่ายมิตซูบิชิที่ได้รับการแต่งตั้งเท่านั้น</li>\r\n	<li>รับรายการฟรีค่าแรงเช็กระยะนาน 5 ปี หรือ 100,000 กิโลเมตร (แล้วแต่ระยะใดจะถึงก่อน) มูลค่าสูงสุด 8,150 บาท อัตราค่าแรงที่นำมาคำนวณอ้างอิงจากอัตราค่าแรง กลาง บริการฟรีเฉพาะค่าแรงเช็กระยะตามที่กำหนดไว้ในบัตรตรวจเช็กระยะฟรีในสมุดรับบริการและคู่มือการใช้รถ ซึ่งรถยนต์ของลูกค้าจะได้รับการตรวจสอบและบำรุง รักษาตามรายการที่ระบุไว้ โดยลูกค้าสามารถเข้ารับบริการที่ศูนย์บริการมาตรฐานของผู้จำหน่ายมิตซูบิชิที่ได้รับการแต่งตั้งเท่านั้น</li>\r\n	<li>สำหรับลูกค้าที่จองรถยนต์มิตซูบิชิ ปาเจโร สปอร์ต ใหม่ (รุ่นปี 2020) รุ่น 2WD 2.4D GT-Premium และ 4WD 2.4D GT-Premium ตั้งแต่วันที่ 25 ก.ค. 62 ถึง 31 ต.ค.62 และออกรถภายในวันที่ 30 พ.ย. 62 รับฟรี อุปกรณ์เชื่อมต่อสัญญาณภาพและเสียงระบบดิจิตอล (HDMI WiFi Dongle) จำนวน 1 ชิ้น มูลค่า 1,690 บาท ในวันออกรถ ณ ศูนย์บริการรถยนต์มิตซูบิชิที่ท่านออกรถ</li>\r\n</ol>\r\n', '<p>dgdg</p>\r\n', '2019-10-25', '2019-10-31', 'f69bd70ad430539eab3c40b46a91b74b.jpg', '2019-10-17 01:54:56', 'chinathip pondumpai', '2019-10-17 13:53:04', 'chinathip pondumpai', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `rest`
--

CREATE TABLE `rest` (
  `id` int(11) UNSIGNED NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `primary_key` varchar(200) NOT NULL,
  `x_api_key` varchar(20) DEFAULT NULL,
  `x_token` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest`
--

INSERT INTO `rest` (`id`, `subject`, `table_name`, `primary_key`, `x_api_key`, `x_token`) VALUES
(1, 'Users', 'users', 'id', 'no', 'no'),
(2, 'Type', 'type', 'id', 'no', 'no'),
(3, 'News', 'news', 'id', 'no', 'no'),
(4, 'Article', 'article', 'id', 'no', 'no'),
(5, 'About', 'about', 'id', 'no', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `rest_field`
--

CREATE TABLE `rest_field` (
  `id` int(11) UNSIGNED NOT NULL,
  `rest_id` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `input_type` varchar(200) NOT NULL,
  `show_column` varchar(10) DEFAULT NULL,
  `show_add_api` varchar(10) DEFAULT NULL,
  `show_update_api` varchar(10) DEFAULT NULL,
  `show_detail_api` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest_field`
--

INSERT INTO `rest_field` (`id`, `rest_id`, `field_name`, `field_label`, `input_type`, `show_column`, `show_add_api`, `show_update_api`, `show_detail_api`) VALUES
(1, 1, 'id', NULL, 'input', 'yes', '', '', 'yes'),
(2, 1, 'name', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(3, 1, 'surname', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(4, 1, 'username', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(5, 1, 'password', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(6, 1, 'email', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(7, 1, 'address', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(8, 1, 'tel', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(9, 1, 'facebook_id', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(10, 1, 'line_id', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(11, 1, 'type_user', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(12, 2, 'id', NULL, 'input', 'yes', '', '', 'yes'),
(13, 2, 'name_thai', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(14, 2, 'name_eng', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(15, 2, 'image_type', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(16, 2, 'created_at', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(17, 2, 'created_by', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(18, 2, 'status', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(19, 3, 'id', NULL, 'input', 'yes', '', '', 'yes'),
(20, 3, 'name_thai', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(21, 3, 'name_eng', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(22, 3, 'detail', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(23, 3, 'img_news', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(24, 3, 'created_at', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(25, 3, 'created_by', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(26, 3, 'updated_at', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(27, 3, 'updated_by', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(28, 3, 'status', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(29, 4, 'id', NULL, 'input', 'yes', '', '', 'yes'),
(30, 4, 'name_thai', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(31, 4, 'name_eng', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(32, 4, 'detail', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(33, 4, 'img_article', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(34, 4, 'video', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(35, 4, 'created_by', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(36, 4, 'created_at', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(37, 4, 'updated_by', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(38, 4, 'updated_at', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(39, 5, 'id', NULL, 'input', 'yes', '', '', 'yes'),
(40, 5, 'name_thai', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(41, 5, 'name_eng', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(42, 5, 'detail', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(43, 5, 'img_about', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(44, 5, 'created_by', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(45, 5, 'created_at', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(46, 5, 'updated_by', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(47, 5, 'updated_at', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(48, 5, 'status', NULL, 'input', 'yes', 'yes', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `rest_field_validation`
--

CREATE TABLE `rest_field_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `rest_field_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest_field_validation`
--

INSERT INTO `rest_field_validation` (`id`, `rest_field_id`, `rest_id`, `validation_name`, `validation_value`) VALUES
(1, 2, 1, 'required', ''),
(2, 2, 1, 'max_length', '50'),
(3, 3, 1, 'required', ''),
(4, 3, 1, 'max_length', '50'),
(5, 4, 1, 'required', ''),
(6, 4, 1, 'max_length', '50'),
(7, 5, 1, 'required', ''),
(8, 5, 1, 'max_length', '50'),
(9, 6, 1, 'required', ''),
(10, 6, 1, 'max_length', '50'),
(11, 7, 1, 'required', ''),
(12, 8, 1, 'required', ''),
(13, 8, 1, 'max_length', '10'),
(14, 9, 1, 'required', ''),
(15, 10, 1, 'required', ''),
(16, 11, 1, 'required', ''),
(17, 11, 1, 'max_length', '30'),
(18, 13, 2, 'required', ''),
(19, 13, 2, 'max_length', '50'),
(20, 14, 2, 'required', ''),
(21, 14, 2, 'max_length', '50'),
(22, 15, 2, 'required', ''),
(23, 15, 2, 'max_length', '255'),
(24, 16, 2, 'required', ''),
(25, 17, 2, 'required', ''),
(26, 17, 2, 'max_length', '30'),
(27, 18, 2, 'required', ''),
(28, 18, 2, 'max_length', '10'),
(29, 20, 3, 'required', ''),
(30, 20, 3, 'max_length', '50'),
(31, 21, 3, 'required', ''),
(32, 21, 3, 'max_length', '50'),
(33, 22, 3, 'required', ''),
(34, 23, 3, 'required', ''),
(35, 23, 3, 'max_length', '255'),
(36, 24, 3, 'required', ''),
(37, 25, 3, 'required', ''),
(38, 25, 3, 'max_length', '50'),
(39, 26, 3, 'required', ''),
(40, 27, 3, 'required', ''),
(41, 27, 3, 'max_length', '50'),
(42, 28, 3, 'required', ''),
(43, 28, 3, 'max_length', '10'),
(44, 30, 4, 'required', ''),
(45, 30, 4, 'max_length', '50'),
(46, 31, 4, 'required', ''),
(47, 31, 4, 'max_length', '50'),
(48, 32, 4, 'required', ''),
(49, 33, 4, 'required', ''),
(50, 33, 4, 'max_length', '255'),
(51, 34, 4, 'required', ''),
(52, 34, 4, 'max_length', '255'),
(53, 35, 4, 'required', ''),
(54, 35, 4, 'max_length', '50'),
(55, 36, 4, 'required', ''),
(56, 37, 4, 'required', ''),
(57, 37, 4, 'max_length', '50'),
(58, 38, 4, 'required', ''),
(59, 40, 5, 'required', ''),
(60, 40, 5, 'max_length', '50'),
(61, 41, 5, 'required', ''),
(62, 41, 5, 'max_length', '50'),
(63, 42, 5, 'required', ''),
(64, 43, 5, 'required', ''),
(65, 43, 5, 'max_length', '255'),
(66, 44, 5, 'required', ''),
(67, 44, 5, 'max_length', '50'),
(68, 45, 5, 'required', ''),
(69, 46, 5, 'required', ''),
(70, 46, 5, 'max_length', '50'),
(71, 47, 5, 'required', ''),
(72, 48, 5, 'required', ''),
(73, 48, 5, 'max_length', '10');

-- --------------------------------------------------------

--
-- Table structure for table `rest_input_type`
--

CREATE TABLE `rest_input_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(200) NOT NULL,
  `relation` varchar(20) NOT NULL,
  `validation_group` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest_input_type`
--

INSERT INTO `rest_input_type` (`id`, `type`, `relation`, `validation_group`) VALUES
(1, 'input', '0', 'input'),
(2, 'timestamp', '0', 'timestamp'),
(3, 'file', '0', 'file');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(10) NOT NULL,
  `name_thai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_eng` text COLLATE utf8_unicode_ci NOT NULL,
  `img_service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `name_thai`, `name_eng`, `detail`, `detail_eng`, `img_service`, `video`, `link_youtube`, `created_by`, `created_at`, `updated_by`, `updated_at`, `status`) VALUES
(1, 'ประสบการณ์จากผู้ใช้จริง : คุณกิตติพงษ์ เรือนวงษ์ อาชีพวิศวกร', '555', '<p>44455</p>\r\n', '<p>222</p>\r\n', 'e0342545688beebd9df2e60d5b16e23f.jpg', '0e4ae7b5cf4c1a4a8ed34c430f834f12.mp4', 'https://www.youtube.com/watch?v=VkfMKWY1aT0', 'surachai uold', '2019-10-09 04:44:19', 'chinathip pondumpai', '2019-10-17 17:03:11', 'on'),
(2, 'ประสบการณ์จากผู้ใช้จริง : คุณสุรสีห์ เสือคง อาชีพวิศวกร', '', '', '', 'ccc183c7477dac5714b8c701a302efd8.jpg', '', 'https://www.youtube.com/watch?v=VkfMKWY1aT0', 'chinathip pondumpai', '2019-10-17 09:55:05', 'chinathip pondumpai', '2019-10-17 16:58:23', 'on'),
(4, 'd', 'dd', '<p>dd</p>\r\n', '<p>dd</p>\r\n', 'a6483255ed06d9ab3fb3a1ddbbd9f36a.jpg', NULL, 'd', 'chinathip pondumpai', '2019-10-23 18:21:37', '', '0000-00-00 00:00:00', 'on'),
(5, 'd55', 'dd55', '<p>dd555</p>\r\n', '<p>dd55</p>\r\n', '13b59d4941bf5666fc03eb5716620e89.jpg', NULL, 'd55', 'chinathip pondumpai', '2019-10-23 18:22:32', 'chinathip pondumpai', '2019-10-24 01:23:41', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(2) NOT NULL,
  `name_thai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name_eng` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name_thai`, `name_eng`, `image_type`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`) VALUES
(146, 'รุ่นเครื่องยนต์ดีเซล', '234', 'Doraemon2', '2019-10-22 05:55:56', 'surachai uold', '2019-10-22 12:27:46', 'chinathip pondumpai', 'on'),
(198, 'รุ่นเครื่องยนตร์ ngv', '33334', '', '2019-10-22 05:27:38', 'surachai uold', '2019-10-22 12:27:37', 'chinathip pondumpai', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` text COLLATE utf8_unicode_ci NOT NULL,
  `line_id` text COLLATE utf8_unicode_ci NOT NULL,
  `type_user` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `username`, `password`, `email`, `address`, `tel`, `facebook_id`, `line_id`, `type_user`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`) VALUES
(44, 'chinathip', 'pondumpai', '555', '55', '', '', '', '', '', 'admin', '2019-09-30 06:21:11', '', '0000-00-00 00:00:00', '', ''),
(45, 'chinathip', 'pondumpai', '66', '666', '', '', '', '', '', 'admin', '2019-09-30 06:21:33', '', '0000-00-00 00:00:00', '', ''),
(47, '555', '5', '5', '555', '55', '555', '55', '555', '555', 'user', '2019-09-29 17:00:00', '', '0000-00-00 00:00:00', '', ''),
(48, 'เมนูด้านบน', 'dd', 'dd', 'd', '', '', '', '', '', 'admin', '2019-09-30 17:51:15', '', '0000-00-00 00:00:00', '', ''),
(49, 'dd', 'ddd', 'dd', 'd', '', '', '', '', '', 'admin', '2019-09-30 17:52:13', '', '0000-00-00 00:00:00', '', ''),
(50, 'กกก', 'กกก', 'chiop', '1234', '', '', '', '', '', 'admin', '2019-10-09 16:01:30', '', '0000-00-00 00:00:00', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_group_to_group`
--
ALTER TABLE `aauth_group_to_group`
  ADD PRIMARY KEY (`group_id`,`subgroup_id`);

--
-- Indexes for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perm_to_user`
--
ALTER TABLE `aauth_perm_to_user`
  ADD PRIMARY KEY (`user_id`,`perm_id`);

--
-- Indexes for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_user`
--
ALTER TABLE `aauth_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_users`
--
ALTER TABLE `aauth_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_user_to_group`
--
ALTER TABLE `aauth_user_to_group`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`);

--
-- Indexes for table `cc_options`
--
ALTER TABLE `cc_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud`
--
ALTER TABLE `crud`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_custom_option`
--
ALTER TABLE `crud_custom_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_field`
--
ALTER TABLE `crud_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_field_validation`
--
ALTER TABLE `crud_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_input_type`
--
ALTER TABLE `crud_input_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_input_validation`
--
ALTER TABLE `crud_input_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dealer`
--
ALTER TABLE `dealer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_custom_attribute`
--
ALTER TABLE `form_custom_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_custom_option`
--
ALTER TABLE `form_custom_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_field`
--
ALTER TABLE `form_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_field_validation`
--
ALTER TABLE `form_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_type`
--
ALTER TABLE `menu_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_block_element`
--
ALTER TABLE `page_block_element`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rest`
--
ALTER TABLE `rest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rest_field`
--
ALTER TABLE `rest_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rest_field_validation`
--
ALTER TABLE `rest_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rest_input_type`
--
ALTER TABLE `rest_input_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aauth_user`
--
ALTER TABLE `aauth_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aauth_users`
--
ALTER TABLE `aauth_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=228;

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cc_options`
--
ALTER TABLE `cc_options`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crud`
--
ALTER TABLE `crud`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `crud_custom_option`
--
ALTER TABLE `crud_custom_option`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crud_field`
--
ALTER TABLE `crud_field`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `crud_field_validation`
--
ALTER TABLE `crud_field_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `crud_input_type`
--
ALTER TABLE `crud_input_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `crud_input_validation`
--
ALTER TABLE `crud_input_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `dealer`
--
ALTER TABLE `dealer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_custom_attribute`
--
ALTER TABLE `form_custom_attribute`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_custom_option`
--
ALTER TABLE `form_custom_option`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_field`
--
ALTER TABLE `form_field`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_field_validation`
--
ALTER TABLE `form_field_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `menu_type`
--
ALTER TABLE `menu_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_block_element`
--
ALTER TABLE `page_block_element`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `rest`
--
ALTER TABLE `rest`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rest_field`
--
ALTER TABLE `rest_field`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `rest_field_validation`
--
ALTER TABLE `rest_field_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `rest_input_type`
--
ALTER TABLE `rest_input_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
