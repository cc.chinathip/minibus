
<?= $this->load->view('section/header'); ?>
  <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขโปรโมชั่น</h1>
              </div>
              <form class="user" id="promotionSubmit"  action="<?= base_url('backend/Promotion/update_promotion'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อโปรโมชั่น (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อโปรโมชั่นภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ชื่อโปรโมชั่น (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อโปรโมชั่นภาษาอังกฤษ" >
                  </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="promotion_image">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
								</div>
			
								<div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
											<label for="inputGroupFile01">วันที่เริ่มโปรโมชั่น</label>
											<input type="datetime" class="form-control form-control-user" id="promotion_start" name="promotion_start" required>
										
                  </div>
                  <div class="col-sm-6">
									<label for="inputGroupFile01">วันที่สิ้นสุดโปรโมชั่น</label>
											<input type="datetime" class="form-control form-control-user" id="promotion_end" name="promotion_end" required>
                  </div>
                </div>

                <div class="form-group">
                <label for="sel1">รายละเอียดโปรโมชั่น (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียดโปรโมชั่น..." class="form-control form-control-address" name="promotion_detail" id="promotion_detail"></textarea>
								</div>

								<div class="form-group">
                <label for="sel1">รายละเอียดโปรโมชั่น (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดโปรโมชั่น..." class="form-control form-control-address" name="promotion_detail_eng" id="promotion_detail_eng"></textarea>
								</div>
							
								<input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="update" >
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
                                <input type="hidden"  id="old_image"  name="old_image" >
			
									<div class="form-group row">
											<div class="col-sm-4 mb-3 mb-sm-0">
													<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
											</div>
											<div class="col-sm-4">
													<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
											</div>
									</div>
								
								
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		CKEDITOR.replace("promotion_detail");
		CKEDITOR.replace("promotion_detail_eng");
		
	  $(document).ready(function() {
      
	    

      
   
    $("#imgInp").change(function(){
        preview_image_one(this);

       
	});


    $("#promotion_start").datepicker({
      dateFormat: 'yy-mm-dd' 
    });
		$("#promotion_end").datepicker({
      dateFormat: 'yy-mm-dd' 
    });
    
        var apikey =  "<?= API_KEY; ?>"; 
		    var api_url = "<?= API_URL; ?>";

        $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Promotion/detail',
                data:"id="+'<?= $_GET['id'] ?>',
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.promotion)
                    $('#name_thai').val(data.data.promotion.name_thai)
                    $('#name_eng').val(data.data.promotion.name_eng)
                  
                    $('#target').attr('src','<?= base_url('/assets/images/promotion/') ?>'+data.data.promotion.img_promotion);
                   
					$('#promotion_detail').val(data.data.promotion.detail);
					$('#promotion_detail_eng').val(data.data.promotion.detail_eng);
                    $('#promotion_start').val(data.data.promotion.promotion_start)
                    $('#promotion_end').val(data.data.promotion.promotion_end)
                    console.log('data.data.promotion.promotion_end',data.data.promotion.promotion_end)
                    $('#old_image').val(data.data.promotion.img_promotion);
				}
			});

		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
