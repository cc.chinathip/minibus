<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function activity_all(){	
		$data['title'] = 'ข่าวสารทั้งหมด';
		//$this->load->view('index');
		$this->load->view('activity/activity_all');
	}
	
	public function add_activity(){	
		$data['title'] = 'เพิ่มข่าวสาร';
		//$this->load->view('index');
		if(isset($_POST['insert'])){

			
			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('activity_detail'),
				'detail_eng' => $this->input->post('activity_detail_eng'),
				'activity_start' => $this->input->post('activity_start'),
				'activity_end' => $this->input->post('activity_end'),
				'img_activity' =>Rename_file($_FILES['activity_image']['name']),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);

			$data = $this->db->insert('activity',$data);

			$path = array(
				'upload_path' => './assets/images/activity/',
				'redirect_path1' => 'activity/add_activity_form',
				'redirect_path2' => 'activity/activity_all',
				'name_image' => Rename_file($_FILES['activity_image']['name']),
				'name_input_image' => 'activity_image'
			);
			check_upload($path,'insert');



		}else{

			$this->load->view('activity/add_activity_form');

		}
		
	}

	public function update_activity(){

		$data['title'] = 'หน้าแก้ไขโปรโมชั่น';
		
				if(isset($_POST['update'])){
					
					if($_FILES['activity_image']['name'] !== ''){

						$image = Rename_file($_FILES['activity_image']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					}
					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
						'detail' => $this->input->post('activity_detail'),
						'detail_eng' => $this->input->post('activity_detail_eng'),
						'activity_start' => $this->input->post('activity_start'),
						'activity_end' => $this->input->post('activity_end'),
						'img_activity' => $image,
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						//'status' => 'on',
					);

					/* print_r($data);
					exit(); */

					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('activity',$data);

	
				
					$path = array(
						'upload_path' => './assets/images/activity/',
						'redirect_path1' => 'activity/activity_all',
						'redirect_path2' => 'activity/activity_all',
						'name_image' =>  $image,
						'name_input_image' => 'activity_image'
					);
					check_upload($path,'update');
			
					
			}else{
				$this->load->view('activity/update_activity_form');
			}
	}
   
}
?>
