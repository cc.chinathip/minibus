
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขประเภทสินค้า</h1>
              </div>
              <form class="user" id="UpdateTypeSubmit" method="post">
                <div class="form-group row">
                  <div class="col-sm-12 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อประเภทสินค้าภาษาไทย</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อประเภทสินค้าภาษาไทย" required>
                  </div>
                 
								</div>
								<div class="form-group row">
									<div class="col-sm-12">
                  <label for="sel1">ชื่อประเภทสินค้าภาษาอังกฤษ</label>
											<input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อประเภทสินค้าภาษาอังกฤษ" >
										</div>
								</div>
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
								<input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<!-- <input type="hidden"  name="update" > -->
											<div class="form-group row">
														<div class="col-sm-4 mb-3 mb-sm-0">
																<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
														</div>
														<div class="col-sm-4">
																<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
														</div>
												</div>
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>

	  $(document).ready(function() {
      
        var apikey =  "<?= API_KEY; ?>"; 
        var api_url = "<?= API_URL; ?>";

            $.ajax({
                    //cache: true,
                    type:'GET',
                    async:false,
                    url:  api_url+'api/Type/detail',
                    data:"id="+'<?= $_GET['id'] ?>',
                    xhrFields: {
                        withCredentials: false
                    },
                    headers: {
                        'X-Api-Key': apikey,
                    },
                    success: function(data) {
                        //console.log('data',data.data.type.name_thai)

                        $('#name_thai').val(data.data.type.name_thai)
                        $('#name_eng').val(data.data.type.name_eng)

                            //console.log('re',$("#UpdateTypeSubmit").serialize())
                        
                    }
             });

             $('#UpdateTypeSubmit').on("submit", function(e){ 
                //console.log('re',$("#UpdateTypeSubmit").serialize())
                    
                e.preventDefault();
                $.ajax({
                        //cache: true,
                        type:'POST',
                        async:false,
                        url:  api_url+'api/Type/update_type',
                        data: $("#UpdateTypeSubmit").serialize(),
                        xhrFields: {
                            withCredentials: false
                        },
                        headers: {
                            'X-Api-Key': apikey,
                        },
                        success: function(data) {
                            console.log('data2',data.status)
                            if(data.status === true){
                              Swal.fire({
                                title: 'success!',
                                text: 'แก้ไขข้อมูลสำเร็จ',
                                type: 'success',
                              });
                              setTimeout(function(){ 
                                window.location.replace('<?= base_url('backend/product/type_all'); ?>') 
                                }, 2000);
                              
                            }else{

                              Swal.fire({
                                title: 'error!',
                                text: 'แก้ไขข้อมูลล้มเหลว',
                                type: 'error',
                              });
                              
                            }
                        }
                });
            });
});
   


   

    



    





	</script>
  <!-- Bootstrap core JavaScript-->
