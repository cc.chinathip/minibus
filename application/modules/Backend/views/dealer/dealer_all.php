<?= $this->load->view('section/header'); ?>
<?php 
//print_r($upload_data);

if(isset($insert) AND $insert == 'success'){
	echo "<script>
							Swal.fire({
								title: 'success!',
								text: 'เพิ่มผู้จำหน่ายสำเร็จ',
								type: 'success',
						})</script>"; 
 
	redirect('backend/dealer/dealer_all','refresh');
}


if(isset($update) AND $update == 'success'){
	echo "<script>
							Swal.fire({
								title: 'success!',
								text: 'แก้ไขผู้จำหน่ายสำเร็จ',
								type: 'success',
						})</script>"; 
 
	redirect('backend/dealer/dealer_all','refresh');

}

?>

  <!-- Page Wrapper -->
  <div id="wrapper">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">รายการผู้จำหน่าย</h1>
          <p class="mb-4"><a href="<?= base_url('backend/dealer/add_dealer'); ?>" class="btn btn-primary btn-user btn-block btn_add_kind" >เพิ่มผู้จำหน่าย</a></p>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ</th>
								<!-- <th>รูปภาพ</th> -->
                                <th>ที่อยู่</th>
								<th>เบอร์โทร</th>
								<th>วันที่สร้าง</th>
								<th>ผู้สร้าง</th>
								<th>แก้ไขล่าสุด</th>
								<th>ผู้แก้ไข</th>
								<th>สถานะการใช้งาน</th>
								<th>จัดการ</th>
							</tr>
						</thead>
						<tfoot>
											<tr>
											<th>ลำดับ</th>
											<th>ชื่อ</th>
											<!-- <th>รูปภาพ</th> -->
                                            <th>ที่อยู่</th>
											<th>เบอร์โทร</th>
											<th>วันที่สร้าง</th>
											<th>ผู้สร้าง</th>
											<th>แก้ไขล่าสุด</th>
											<th>ผู้แก้ไข</th>
											<th>สถานะการใช้งาน</th>
											<th>จัดการ</th>
											</tr>
						</tfoot>
   			 </table>

              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  
  <script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		//console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/dealer/all',
				//data: {type_user:'admin'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.dealer)
					
					
				
					var html = '<tbody><tr>';
					 $.each(data.data.dealer, function(index,value ) {

						//console.log('index',index)
					
						html += '<td>'+parseInt(index + 1)+'</td>';
						html += '<td>'+value.name_thai+'</td>';
                        //html += '<td><img style="width: 200px;" src="'+'<?= base_url('/assets/images/dealer/') ?>'+value.img_dealer+'"/></td>';
                        html += '<td>'+value.address_thai+'</td>';
						html += '<td>'+value.tel+'</td>';
						html += '<td>'+value.created_at+'</td>';
						html += '<td>'+value.created_by+'</td>';
						html += '<td>'+value.updated_at+'</td>';
						html += '<td>'+value.updated_by+'</td>';
						html += '<td><input id="togBtn_'+parseInt(index + 1)+'" type="checkbox" checked data-toggle="toggle" /></td>';
						html += '<td><a href="<?= base_url('backend/dealer/update_dealer?id='); ?>'+value.id+'" class="btn btn-primary a-btn-slide-text">';
						html += '<i class="far fa-edit"></i><span><strong>แก้ไข</strong></span></a> ';
						html += '<button class="btn btn-danger del" data-id="'+value.id+'"><i class="fa fa-trash" aria-hidden="true"></i>ลบ</button>';
						html += "</td>";
						html += '</tr>';
						
					}); 
					html += '</tbody>';
					$('#dataTable').append(html);
					switch_button(data.data.dealer,'dealer', api_url, apikey)
					

					
					
				}
			}); 

			delete_data_row('.del','dealer',apikey,api_url)
	});

  </script>
  <?= $this->load->view('section/footer'); ?>
