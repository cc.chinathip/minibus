<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function orders_all($type_order){	
        $data['title'] = 'รายการสั่งศื้อ';
        
        
        if($type_order === '1'){

            $data['type_order'] = 'ซื้อ';

        }else if($type_order === '2'){

            $data['type_order'] = 'ทดลอง';
        }else if($type_order === '3'){

            $data['type_order'] = 'จอง';
        }else if($type_order === '4'){

			$data['type_order'] = 'ลิซซิ่ง';
			
        }else if($type_order === '5'){

            $data['type_order'] = 'โบรชัวร์';
        }
        
        $this->load->view('orders/orders_all',$data);
		
	}
	
}
?>
