<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| About Controller
*| --------------------------------------------------------------------------
*| About site
*|
*/
class About extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_about');
	}

	/**
	* show all Abouts
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('about_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['abouts'] = $this->model_about->get($filter, $field, $this->limit_page, $offset);
		$this->data['about_counts'] = $this->model_about->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/about/index/',
			'total_rows'   => $this->model_about->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('About List');
		$this->render('backend/standart/administrator/about/about_list', $this->data);
	}
	
	/**
	* Add new abouts
	*
	*/
	public function add()
	{
		$this->is_allowed('about_add');

		$this->template->title('About New');
		$this->render('backend/standart/administrator/about/about_add', $this->data);
	}

	/**
	* Add New Abouts
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('about_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'detail' => $this->input->post('detail'),
			];

			
			$save_about = $this->model_about->store($save_data);

			if ($save_about) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_about;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/about/edit/' . $save_about, 'Edit About'),
						anchor('administrator/about', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/about/edit/' . $save_about, 'Edit About')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/about');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/about');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Abouts
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('about_update');

		$this->data['about'] = $this->model_about->find($id);

		$this->template->title('About Update');
		$this->render('backend/standart/administrator/about/about_update', $this->data);
	}

	/**
	* Update Abouts
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('about_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'detail' => $this->input->post('detail'),
			];

			
			$save_about = $this->model_about->change($id, $save_data);

			if ($save_about) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/about', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/about');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/about');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Abouts
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('about_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'about'), 'success');
        } else {
            set_message(cclang('error_delete', 'about'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Abouts
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('about_view');

		$this->data['about'] = $this->model_about->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('About Detail');
		$this->render('backend/standart/administrator/about/about_view', $this->data);
	}
	
	/**
	* delete Abouts
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$about = $this->model_about->find($id);

		
		
		return $this->model_about->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('about_export');

		$this->model_about->export('about', 'about');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('about_export');

		$this->model_about->pdf('about', 'about');
	}
}


/* End of file about.php */
/* Location: ./application/controllers/administrator/About.php */