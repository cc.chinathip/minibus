<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Amphur extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_amphur');
	}

	/**
	 * @api {get} /amphur/all Get all amphurs.
	 * @apiVersion 0.1.0
	 * @apiName AllAmphur 
	 * @apiGroup amphur
	 * @apiHeader {String} X-Api-Key Amphurs unique access-key.
	 * @apiPermission Amphur Cant be Accessed permission name : api_amphur_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Amphurs.
	 * @apiParam {String} [Field="All Field"] Optional field of Amphurs : AMPHUR_ID, AMPHUR_CODE, AMPHUR_NAME, POSTCODE, GEO_ID, PROVINCE_ID.
	 * @apiParam {String} [Start=0] Optional start index of Amphurs.
	 * @apiParam {String} [Limit=10] Optional limit data of Amphurs.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of amphur.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataAmphur Amphur data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_amphur_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['AMPHUR_ID', 'AMPHUR_CODE', 'AMPHUR_NAME', 'POSTCODE', 'GEO_ID', 'PROVINCE_ID'];
		$amphurs = $this->model_api_amphur->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_amphur->count_all($filter, $field);

		$data['amphur'] = $amphurs;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Amphur',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /amphur/detail Detail Amphur.
	 * @apiVersion 0.1.0
	 * @apiName DetailAmphur
	 * @apiGroup amphur
	 * @apiHeader {String} X-Api-Key Amphurs unique access-key.
	 * @apiPermission Amphur Cant be Accessed permission name : api_amphur_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Amphurs.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of amphur.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError AmphurNotFound Amphur data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_amphur_detail', false);

		$this->requiredInput(['AMPHUR_ID']);

		$id = $this->get('AMPHUR_ID');

		$select_field = ['AMPHUR_ID', 'AMPHUR_CODE', 'AMPHUR_NAME', 'POSTCODE', 'GEO_ID', 'PROVINCE_ID'];
		$data['amphur'] = $this->model_api_amphur->find($id, $select_field);

		if ($data['amphur']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Amphur',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Amphur not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /amphur/add Add Amphur.
	 * @apiVersion 0.1.0
	 * @apiName AddAmphur
	 * @apiGroup amphur
	 * @apiHeader {String} X-Api-Key Amphurs unique access-key.
	 * @apiPermission Amphur Cant be Accessed permission name : api_amphur_add
	 *
 	 * @apiParam {String} AMPHUR_CODE Mandatory AMPHUR_CODE of Amphurs. Input AMPHUR CODE Max Length : 4. 
	 * @apiParam {String} AMPHUR_NAME Mandatory AMPHUR_NAME of Amphurs. Input AMPHUR NAME Max Length : 150. 
	 * @apiParam {String} POSTCODE Mandatory POSTCODE of Amphurs. Input POSTCODE Max Length : 5. 
	 * @apiParam {String} GEO_ID Mandatory GEO_ID of Amphurs. Input GEO ID Max Length : 5. 
	 * @apiParam {String} PROVINCE_ID Mandatory PROVINCE_ID of Amphurs. Input PROVINCE ID Max Length : 5. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_amphur_add', false);

		$this->form_validation->set_rules('AMPHUR_CODE', 'AMPHUR CODE', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('AMPHUR_NAME', 'AMPHUR NAME', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('POSTCODE', 'POSTCODE', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('GEO_ID', 'GEO ID', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('PROVINCE_ID', 'PROVINCE ID', 'trim|required|max_length[5]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'AMPHUR_CODE' => $this->input->post('AMPHUR_CODE'),
				'AMPHUR_NAME' => $this->input->post('AMPHUR_NAME'),
				'POSTCODE' => $this->input->post('POSTCODE'),
				'GEO_ID' => $this->input->post('GEO_ID'),
				'PROVINCE_ID' => $this->input->post('PROVINCE_ID'),
			];
			
			$save_amphur = $this->model_api_amphur->store($save_data);

			if ($save_amphur) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /amphur/update Update Amphur.
	 * @apiVersion 0.1.0
	 * @apiName UpdateAmphur
	 * @apiGroup amphur
	 * @apiHeader {String} X-Api-Key Amphurs unique access-key.
	 * @apiPermission Amphur Cant be Accessed permission name : api_amphur_update
	 *
	 * @apiParam {String} AMPHUR_CODE Mandatory AMPHUR_CODE of Amphurs. Input AMPHUR CODE Max Length : 4. 
	 * @apiParam {String} AMPHUR_NAME Mandatory AMPHUR_NAME of Amphurs. Input AMPHUR NAME Max Length : 150. 
	 * @apiParam {String} POSTCODE Mandatory POSTCODE of Amphurs. Input POSTCODE Max Length : 5. 
	 * @apiParam {String} GEO_ID Mandatory GEO_ID of Amphurs. Input GEO ID Max Length : 5. 
	 * @apiParam {String} PROVINCE_ID Mandatory PROVINCE_ID of Amphurs. Input PROVINCE ID Max Length : 5. 
	 * @apiParam {Integer} AMPHUR_ID Mandatory AMPHUR_ID of Amphur.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_amphur_update', false);

		
		$this->form_validation->set_rules('AMPHUR_CODE', 'AMPHUR CODE', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('AMPHUR_NAME', 'AMPHUR NAME', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('POSTCODE', 'POSTCODE', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('GEO_ID', 'GEO ID', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('PROVINCE_ID', 'PROVINCE ID', 'trim|required|max_length[5]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'AMPHUR_CODE' => $this->input->post('AMPHUR_CODE'),
				'AMPHUR_NAME' => $this->input->post('AMPHUR_NAME'),
				'POSTCODE' => $this->input->post('POSTCODE'),
				'GEO_ID' => $this->input->post('GEO_ID'),
				'PROVINCE_ID' => $this->input->post('PROVINCE_ID'),
			];
			
			$save_amphur = $this->model_api_amphur->change($this->post('AMPHUR_ID'), $save_data);

			if ($save_amphur) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /amphur/delete Delete Amphur. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteAmphur
	 * @apiGroup amphur
	 * @apiHeader {String} X-Api-Key Amphurs unique access-key.
	 	 * @apiPermission Amphur Cant be Accessed permission name : api_amphur_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Amphurs .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_amphur_delete', false);

		$amphur = $this->model_api_amphur->find($this->post('AMPHUR_ID'));

		if (!$amphur) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Amphur not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_amphur->remove($this->post('AMPHUR_ID'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Amphur deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Amphur not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Amphur.php */
/* Location: ./application/controllers/api/Amphur.php */