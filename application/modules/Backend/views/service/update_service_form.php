
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขบริการหลังการขาย</h1>
              </div>
              <form class="user" id="serviceSubmit"  action="<?= base_url('backend/service/update_service'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
				  <label for="sel1">ชื่อบริการหลังการขาย (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อบริการหลังการขายภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
				  <label for="sel1">ชื่อบริการหลังการขาย (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อบริการหลังการขายภาษาอังกฤษ" >
                  </div>
                </div>
                		<div class="form-group">
												<div class="input-group">
																<div class="custom-file">
																		<input type="file" class="custom-file-input" id="imgInp"
																		aria-describedby="inputGroupFileAddon01" name="service_image">
																		<label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
																</div>
												</div><p>

												<div class="input-group">
														<img class="image_preview" id="target" src="#"/>
												</div>
										
										</div>

										
										<!-- <div class="form-group">
												<div class="input-group">
															
																<div class="custom-file">
																		<input type="file" class="custom-file-input" id="file-to-upload"
																		aria-describedby="inputGroupFileAddon01" name="service_video"  accept="video/mp4">
																		<label class="custom-file-label" for="inputGroupFile01">เลือกไฟล์วีดีโอ</label>
																</div>
												</div><p>

												<div id="video-demo-container-update">
												</div>

												<div id="video-demo-container">
													<video id="main-video" controls>
														<source type="video/mp4"  />
													</video>
												</div>

									</div> -->

									<div class="form-group">
										<div class="col-sm-12 mb-3 mb-sm-0">
											<label for="sel1">ลิงค์ youtube</label>
											<input type="text" class="form-control form-control-user" id="link_youtube" name="link_youtube" placeholder="ลิงค์วีดีโอจาก youtube" ><p>
										</div>
									</div>


									<!-- 	<div id="video-demo-container3">
                      <video id="main-video" controls>
                        <source type="video/mp4"  />
                      </video>
                      <canvas id="video-canvas3"></canvas>
                    </div> -->

                    

							
								<div class="form-group">
								<label for="sel1">รายละเอียดบริการหลังการขาย (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียดบริการหลังการขาย..." class="form-control form-control-address" name="service_detail" id="service_detail"></textarea>
								</div>

								<div class="form-group">
								<label for="sel1">รายละเอียดบริการหลังการขาย (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดบริการหลังการขาย..." class="form-control form-control-address" name="service_detail_eng" id="service_detail_eng"></textarea>
								</div>
																<input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
																<input type="hidden"  name="update" >
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
                                <input type="hidden"  id="old_image"  name="old_image" >
																<input type="hidden"  id="old_video"  name="old_video" >

								<div class="form-group row">
                  <div class="col-sm-4 mb-3 mb-sm-0">
											<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
                  </div>
                  <div class="col-sm-4">
											<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
                  </div>
                </div>
									


              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		CKEDITOR.replace("service_detail");
		CKEDITOR.replace("service_detail_eng");
		

	  $(document).ready(function() {
      
			

    //$('#main-video-update').attr('src','')

    $("#imgInp").change(function(){
        preview_image_one(this);

       
		});
    
        var apikey =  "<?= API_KEY; ?>"; 
		    var api_url = "<?= API_URL; ?>";

        $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/service/detail',
                data:"id="+'<?= $_GET['id'] ?>',
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {

										console.log('data',data.data.service)
                    $('#name_thai').val(data.data.service.name_thai)
                    $('#name_eng').val(data.data.service.name_eng)
                    $('#target').attr('src','<?= base_url('/assets/images/service/') ?>'+data.data.service.img_service);
										$('#service_detail').val(data.data.service.detail);
										$('#service_detail_eng').val(data.data.service.detail_eng);
                    $('#service_start').val(data.data.service.service_start)
                    $('#service_end').val(data.data.service.service_end)
                    
                    $('#old_image').val(data.data.service.img_service);
					$('#link_youtube').val(data.data.service.link_youtube)
										$('#old_video').val(data.data.service.video);

										console.log('data.data.service.video',data.data.service.video)

										if(data.data.service.video !== null || data.data.service.video !== undefined){

											var url_video = '<?= base_url('/assets/videos/service/') ?>'+data.data.service.video;

										}
										
										var taghtmlvdo = '<video id="main-video-update" controls>';
												taghtmlvdo += '<source type="video/mp4" src="'+url_video+'" />';
												taghtmlvdo += '</video>';

										$('#video-demo-container-update').html(taghtmlvdo);

										if(data.data.service.video === null || data.data.service.video === undefined){

											$('#main-video-update').css('display','none')

										}
										
									
				}
			});

		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
