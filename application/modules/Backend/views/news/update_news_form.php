
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขข่าวสาร</h1>
              </div>
              <form class="user" id="newsSubmit"  action="<?= base_url('backend/news/update_news'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อข่าวสาร (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อข่าวสารภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ชื่อข่าวสาร (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อข่าวสารภาษาอังกฤษ" >
                  </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="news_image"  accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
								</div>
								<div class="form-group">
                <label for="sel1">รายละเอียดข่าวสาร (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียดข่าวสาร..." class="form-control form-control-address" name="news_detail" id="news_detail" required></textarea>
								</div>

								<div class="form-group">
								<label for="sel1">รายละเอียดข่าวสาร (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดข่าวสาร..." class="form-control form-control-address" name="news_detail_eng" id="news_detail_eng" required></textarea>
								</div>
						
								<input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="update" >
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
                                <input type="hidden"  id="old_image"  name="old_image" >
									<div class="form-group row">
												<div class="col-sm-4 mb-3 mb-sm-0">
														<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
												</div>
												<div class="col-sm-4">
														<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
												</div>
										</div>
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		 CKEDITOR.replace("news_detail");
		 CKEDITOR.replace("news_detail_eng");
		 
	  $(document).ready(function() {
      
			

      
   
    $("#imgInp").change(function(){
        preview_image_one(this);

       
		});
    
        var apikey =  "<?= API_KEY; ?>"; 
		    var api_url = "<?= API_URL; ?>";

        $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/News/detail',
                data:"id="+'<?= $_GET['id'] ?>',
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.news)
                    $('#name_thai').val(data.data.news.name_thai)
                    $('#name_eng').val(data.data.news.name_eng)
                  
                    $('#target').attr('src','<?= base_url('/assets/images/news/') ?>'+data.data.news.img_news);
                   
					$('#news_detail').val(data.data.news.detail);
					$('#news_detail_eng').val(data.data.news.detail_eng);
                    console.log('data.data.news.news_end',data.data.news.news_end)
                    $('#old_image').val(data.data.news.img_news);
				}
			});


		
	});
	
	</script>
  <!-- Bootstrap core JavaScript-->
