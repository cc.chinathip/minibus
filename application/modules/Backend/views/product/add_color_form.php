
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-color-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
			<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">เพิ่มสีสินค้า</h1>
              </div>
              <form class="user" id="colorSubmit"  action="<?= base_url('backend/Product/add_color'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-12 mb-3 mb-sm-0">
				  	<label for="sel1">ชื่อสีสินค้าภาษาไทย</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อสีสินค้าภาษาไทย" required>
                  </div>
                 
								</div>
								<div class="form-group row">
									<div class="col-sm-12">
											<label for="sel1">ชื่อสีสินค้าภาษาอังกฤษ</label>
											<input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อสีสินค้าภาษาอังกฤษ" >
										</div>
								</div>
								<div class="form-group">
									<div class="input-group">
											
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="imgInp"
												aria-describedby="inputGroupFileAddon01" name="color_image">
												<label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
											</div>
									</div><p>
									<div class="input-group">
										<img class="image_preview" id="target" src="#"/>
									</div>
								</div>
							
								<input type="hidden"  name="created_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="insert" >
											<div class="form-group row">
														<div class="col-sm-4 mb-3 mb-sm-0">
																<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
														</div>
														<div class="col-sm-4">
																<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
														</div>
												</div>
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>

	  $(document).ready(function() {
      
			

	
    $("#imgInp").change(function(){
        preview_image_one(this);

       
		});
    
		/* $('#colorSubmit').on("submit", function(e){ 

		console.log('ppp',$("#imgInp").attr("name"))
		e.preventDefault();
			var apikey =  "<?= API_KEY; ?>"; 
			var api_url = "<?= API_URL; ?>";
            var getValueThai = $('#name_thai').val();
            var getValueEng = $('#name_eng').val();
						var getValueImg = $('#target').data("name");
            var getValueUser = sessionStorage.getItem("name");

				$.ajax({
					//cache: true,
					type:'POST',
					async:false,
					url:   api_url+'api/Type/save_type',
					//data: $("#colorSubmit").serialize(),
          data:{ name_thai: getValueThai, name_eng: getValueEng ,image_type: getValueImg,created_by: getValueUser},
					xhrFields: {
					withCredentials: false
					},
					headers: {
						'X-Api-Key': apikey,
					},
					success: function(data) {
						console.log('data',data)

						if(data.status == true){

							Swal.fire({
								title: 'success!',
								text: 'เพิ่มสีสินค้าสำเร็จ',
								type: 'success',
							})

							$('.reset_data').click();
							$('#target').attr('src', '');

							
						}else{
							Swal.fire({
								title: 'error!',
								text: 'ล้มเหลว',
								type: 'error',
							})
						}
					}
				}); 
	

		});
		 */
	});


	</script>
  <!-- Bootstrap core JavaScript-->
