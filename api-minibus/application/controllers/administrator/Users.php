<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Users Controller
*| --------------------------------------------------------------------------
*| Users site
*|
*/
class Users extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_users');
	}

	/**
	* show all Userss
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('users_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['userss'] = $this->model_users->get($filter, $field, $this->limit_page, $offset);
		$this->data['users_counts'] = $this->model_users->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/users/index/',
			'total_rows'   => $this->model_users->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Users List');
		$this->render('backend/standart/administrator/users/users_list', $this->data);
	}
	
	/**
	* Add new userss
	*
	*/
	public function add()
	{
		$this->is_allowed('users_add');

		$this->template->title('Users New');
		$this->render('backend/standart/administrator/users/users_add', $this->data);
	}

	/**
	* Add New Userss
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('users_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('surname', 'Surname', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('facebook_id', 'Facebook Id', 'trim|required');
		$this->form_validation->set_rules('line_id', 'Line Id', 'trim|required');
		$this->form_validation->set_rules('type_user', 'Type User', 'trim|required|max_length[30]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'address' => $this->input->post('address'),
				'tel' => $this->input->post('tel'),
				'facebook_id' => $this->input->post('facebook_id'),
				'line_id' => $this->input->post('line_id'),
				'type_user' => $this->input->post('type_user'),
			];

			
			$save_users = $this->model_users->store($save_data);

			if ($save_users) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_users;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/users/edit/' . $save_users, 'Edit Users'),
						anchor('administrator/users', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/users/edit/' . $save_users, 'Edit Users')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/users');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/users');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Userss
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('users_update');

		$this->data['users'] = $this->model_users->find($id);

		$this->template->title('Users Update');
		$this->render('backend/standart/administrator/users/users_update', $this->data);
	}

	/**
	* Update Userss
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('users_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('surname', 'Surname', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('facebook_id', 'Facebook Id', 'trim|required');
		$this->form_validation->set_rules('line_id', 'Line Id', 'trim|required');
		$this->form_validation->set_rules('type_user', 'Type User', 'trim|required|max_length[30]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'address' => $this->input->post('address'),
				'tel' => $this->input->post('tel'),
				'facebook_id' => $this->input->post('facebook_id'),
				'line_id' => $this->input->post('line_id'),
				'type_user' => $this->input->post('type_user'),
			];

			
			$save_users = $this->model_users->change($id, $save_data);

			if ($save_users) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/users', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/users');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/users');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Userss
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('users_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'users'), 'success');
        } else {
            set_message(cclang('error_delete', 'users'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Userss
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('users_view');

		$this->data['users'] = $this->model_users->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Users Detail');
		$this->render('backend/standart/administrator/users/users_view', $this->data);
	}
	
	/**
	* delete Userss
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$users = $this->model_users->find($id);

		
		
		return $this->model_users->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('users_export');

		$this->model_users->export('users', 'users');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('users_export');

		$this->model_users->pdf('users', 'users');
	}
}


/* End of file users.php */
/* Location: ./application/controllers/administrator/Users.php */