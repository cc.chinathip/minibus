<?= $this->load->view('section/header'); ?>
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li> 
		    <li class="breadcrumb-item active"><span>โบรชัวร์</span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">โบรชัวร์</h2>
	</div>
</div> 

<div class="section section-banner-buyingcar">
	<div class="img-banner-group">
		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1.jpg') ?>" alt="">
        <img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1-2.png') ?>" alt="">
	</div>

	 
	<div class="banner-caption"> 
		<div class="caption-wrap">
			<div class="inner">
				<span class="f1">NEW</span>
				<span class="f2">Panthera Motors</span>
				<span class="f3">safety for YOUR journey</span>
			</div>
		</div><!--caption-wrap-->

		<div class="caption-promotion">
			<div class="row1">
				<h3>
					<span class="f1">ขับเคลื่อนอย่างมั่นใจ ประหยัด</span>
					<span class="f2">ปลอดภัย นั่งสบายทุกการเดินทาง</span>
				</h3>
			</div>
			<div class="row2">
				<h5>ดอกเบี้ยพิเศษ <span class="bold">1.99%</span>  |  ฟรี รับประกันคุณภาพนาน 5 ปี</h5>
			</div>
		</div><!--caption-promotion--> 
	</div><!--banner-caption--> 
</div><!--section-banner-buyingcar-->
  
<div class="section section-buyingcar wow fadeIn">
	<div class="container">
		
		<div class="buyingcar-contactus brochure">
			<div class="row">
				<div class="col-12">
					<div class="choose-car">
						<div class="hgroup">
							<h3>เลือกรุ่นที่คุณต้องการ<span class="star">*</span></h3>
						</div>
						<div class="buttons">
							<button class="btn btn-blue typecars desale"  data-id="รุ่นเครื่องยนต์ดีเซล"><span>เครื่องยนต์ดีเซล</span></button>
							<button class="btn btn-black typecars ngv" data-id="รุ่นเครื่องยนตร์ ngv"><span>เครื่องยนต์ NGV</span></button>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="hgroup pb-0">
						<h3>ข้อมูลสำหรับติดต่อ<span class="star">*</span></h3>
						<p>กรุณากรอกข้อมูลให้ถูกต้อง เพื่อความสะดวกในการรับบริการ</p>
					</div>

					<div class="row">
						<div class="col-xl-3 col-md-6">
							<div class="input-block">
								<span class="input-text">ชื่อ-นามสกุล</span>
								<input type="text" class="form-control" name="name" id="name" placeholder="กรอกชื่อ-นามสกุล">
							</div><!--input-block-->
						</div>
						<div class="col-xl-3 col-md-6">
							<div class="input-block">
								<span class="input-text">อีเมล์</span>
								<input type="text" class="form-control" name="email" id="email" placeholder="กรอกอีเมล์">
							</div><!--input-block-->
						</div>
						<div class="col-xl-3 col-md-6">
							<div class="input-block">
								<span class="input-text">เบอร์โทรศัพท์ติดต่อ</span>
								<input type="text" class="form-control" name="tel" id="tel" placeholder="กรอกเบอร์โทรศัพท์">
							</div><!--input-block-->
						</div>
						<div class="col-xl-3 col-md-6">
							<input type="hidden" class="typecar_title"><!-- product_id -->
							<input type="hidden" class="car_price"><!-- price -->
							

							<button class="btn btn-red has-arrow regis" type="submit">
								<span class="text">ดาวน์โหลด .PDF</span>
								<span class="icon"><span class="icons icon-pdf"></span></span>
							</button>
						</div>
					</div><!--row-->
				</div><!--col-12-->
			</div><!--row-->
		</div><!--buyingcar-contactus-->
	</div><!--container-->
</div><!--section-buyingcar-->
 
<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
	
		pickTypeCar(api_url,apikey)

		add_order(api_url, apikey, 'โบรชัวร์')
			

			
		
	});

  </script>
