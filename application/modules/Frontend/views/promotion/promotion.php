<?= $this->load->view('section/header'); ?>

<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li>
		    <li class="breadcrumb-item active"><span>โปรโมชั่น</span></li> 
	  	</ol>
	</div>
</nav>

<div class="section section-banner">
	<div class="swiper-container swiper-banner">
        <div class="swiper-wrapper">
        	<div class="swiper-slide">
        		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1.jpg') ?>" alt="">
        		<img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1-2.png') ?>" alt="">

        		<div class="banner-caption"> 
    				<div class="caption-wrap element2">
    					<div class="inner">
	    					<span class="f1">NEW</span>
	    					<span class="f2">Panthera Motors</span>
	    					<span class="f3">safety for YOUR journey</span>
	    				</div>
    				</div><!--caption-wrap-->

    				<div class="caption-promotion">
    					<div class="row1 element2">
    						<h3>
    							<span class="f1">ขับเคลื่อนอย่างมั่นใจ ประหยัด</span>
								<span class="f2">ปลอดภัย นั่งสบายทุกการเดินทาง</span>
    						</h3>
    					</div>
    					<div class="row2 element3">
    						<h5>ดอกเบี้ยพิเศษ <span class="bold">1.99%</span>  |  ฟรี รับประกันคุณภาพนาน 5 ปี</h5>
    					</div>
    				</div><!--caption-promotion-->
	        		 
	        	</div><!--banner-caption-->
        	</div><!--swiper-slide-->

        	<div class="swiper-slide">
        		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1.jpg') ?>" alt="">
        		<img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1-2.png') ?>" alt="">

        		<div class="banner-caption"> 
    				<div class="caption-wrap element2">
    					<div class="inner">
	    					<span class="f1">NEW</span>
	    					<span class="f2">Panthera Motors</span>
	    					<span class="f3">safety for YOUR journey</span>
	    				</div>
    				</div><!--caption-wrap-->

    				<div class="caption-promotion">
    					<div class="row1 element2">
    						<h3>
    							<span class="f1">ขับเคลื่อนอย่างมั่นใจ ประหยัด</span>
								<span class="f2">ปลอดภัย นั่งสบายทุกการเดินทาง</span>
    						</h3>
    					</div>
    					<div class="row2 element3">
    						<h5>ดอกเบี้ยพิเศษ <span class="bold">1.99%</span>  |  ฟรี รับประกันคุณภาพนาน 5 ปี</h5>
    					</div>
    				</div><!--caption-promotion-->
	        		 
	        	</div><!--banner-caption-->
        	</div><!--swiper-slide-->
 

        </div><!--swiper-wrapper-->

        <div class="swiper-pagination banner"></div>
        <!-- Add Arrows -->
      	<div class="swiper-button swiper-button-next banner"></div>
      	<div class="swiper-button swiper-button-prev banner"></div>

    </div><!--swiper-banner-->
</div><!--section-banner-->

<div class="section section-news main pt-0">
	<div class="page-header wow fadeIn">
		<div class="container">
			<h2 class="title-xl">โปรโมชั่น</h2>
		</div>
	</div> 
 	<div class="page-header-info wow fadeIn">
		<div class="container">
			<p>
				โปรดตรวจสอบโปรโมชั่นกับผู้จำหน่าย<br>
				บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทราบล่วงหน้า
			</p>
		</div>
	</div> 

	<div class="container">
		 
		<div class="row space-0 wow fadeIn show_promotion">

		
			<!-- <div class="col-lg-4 col-sm-6 show_promotion" > -->
				<!-- <div class="card card-info">
					<div class="card-photo">
						<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(img/thumb/photo-800x525--4.jpg);" href="promotion-detail.html">
							<img src="img/thumb/photo-100x66--blank.png" alt="">
						</a>
					</div>

					<div class="card-body">
						<h2 class="title-sm"><a href="promotion-detail.html"><div class="name-sm"></div></a></h2>
						<p class="date">25 July 2019</p>

						<div class="row align-items-center">
							<div class="col-6">
								<a class="btn btn-red has-arrow" href="promotion-detail.html">
									<span class="text">รายละเอียด</span>
									<span class="icon">
										<span class="arrow-right"></span>
									</span>
								</a>
							</div>

							<div class="col-6">
								<div class="share-wrap">
									<span class="icons icon-share"></span>
									<span class="text">แชร์ไปยัง : </span>
									<a class="icons icon-share-facebook" href="#"></a>
									<a class="icons icon-share-line" href="#"></a>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			<!-- </div> -->
			
			


			
		</div><!--row-->
	</div><!--container-->
</div><!--section-news-->
 
<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Promotion/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.promotion)
					console.log('total',data.total)
					//console.log('datasssss',data.data.promotion[0].status)
					var html = '';
					 $.each(data.data.promotion, function(index,value ) {
					
						console.log('index',parseInt(index+1))
					
							var number_at = parseInt(index+1);

							html += '<div class="block_promotion col-lg-4 col-sm-6 '+(number_at > 6 ? 'Img_more' : '') +' " >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/promotion/') ?>'+value.img_promotion+');" href="<?= base_url('/frontend/Promotion/promotion_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/promotion/') ?>'+value.img_promotion+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/Promotion/promotion_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-6">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/Promotion/promotion_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

											html += '<div class="col-6">'
												html += '<div class="share-wrap">'
												html += '<span class="icons icon-share"></span>'
												html += '<span class="text">แชร์ไปยัง : </span>'
												html += '<a class="icons icon-share-facebook" href="#"></a> '
												html += '<a class="icons icon-share-line" href="#"></a>'
												html += '</div>'
											html += '</div>'

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'

							 
						//}
						
						
					}); 
					html += '<div class="col-md-12">'
					html += '<div class="buttons pt-4 pb-0 wow fadeIn">'
					html += '	<button class="btn btn-blue btn_more">'
					html += '	<span class="text">แสดงเพิ่มเติม</span>'
					html += '	</button>'
					html += '</div>'
					html += '</div>'

					html += '<div class="col-md-12">'
					html += '<div class="buttons pt-4 pb-0 wow fadeIn">'
					html += '	<button class="btn btn-blue btn_less">'
					html += '	<span class="text">แสดงน้อยลง</span>'
					html += '	</button>'
					html += '</div>'
					html += '</div>'
					
					
					$('.show_promotion').append(html);
					$('.btn_less').hide()

					if($('.block_promotion').hasClass('Img_more')){

						$('.Img_more').hide()
					}

					$(".btn_more").click(function() {
						
						$('.Img_more').show()
						$('.btn_more').hide()
						$('.btn_less').show()
					});

					$(".btn_less").click(function() {
						
						$('.Img_more').hide()
						$('.btn_more').show()
						$('.btn_less').hide()
						
					});

					

					
					
				
					
				}
			}); 

			
		
	});

  </script>
