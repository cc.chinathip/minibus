<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Users extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_users');
		$this->load->model('Model_users');
	}

	/**
	 * @api {get} /users/all Get all userss.
	 * @apiVersion 0.1.0
	 * @apiName AllUsers 
	 * @apiGroup users
	 * @apiHeader {String} X-Api-Key Userss unique access-key.
	 * @apiPermission Users Cant be Accessed permission name : api_users_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Userss.
	 * @apiParam {String} [Field="All Field"] Optional field of Userss : id, name, surname, username, password, email, address, tel, facebook_id, line_id, type_user.
	 * @apiParam {String} [Start=0] Optional start index of Userss.
	 * @apiParam {String} [Limit=10] Optional limit data of Userss.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of users.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataUsers Users data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */


	public function userbyId_post(){

		if(isset($_POST['user_id'])){

			$data = $this->Model_users->userbyId_data($_POST['user_id']);
			if($data){

				$message = $data;

			}else{

				$message = "not found data";
			}
			

		}else{
			$message = "not found params";
		
		}

		$this->response([
			'data' 	=> $message,

		], API::HTTP_OK);
		

	}


	public function data_user_all_post(){

		if(isset($_POST['type_user'])){

			$data = $this->Model_users->user_all_data($_POST['type_user'],null);
			$total = $this->Model_users->user_all_data($_POST['type_user'],'total');

		}else{
			$data = "not found data";
			$total = "not found data";

		}

		$this->response([
			'data' 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
		

	}


	public function username_password_post(){

		if(isset($_POST['username']) AND isset($_POST['password']) AND isset($_POST['type_user'])){

			$data = $this->Model_users->username_password_data($_POST['username'],$_POST['password'],$_POST['type_user'],null);
			$total = $this->Model_users->username_password_data($_POST['username'],$_POST['password'],$_POST['type_user'],'total');


		}else{
			$data = "not found data";
			$total = "not found data";

		}

		$this->response([
			'data' 	=> $data,
			'total' 	=> $total,

			//'total' 	=> $this->session->set_userdata('user_id', 'myId'),
		], API::HTTP_OK);
		

	}


	public function all_get()
	{
		//$this->is_allowed('api_users_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name', 'surname', 'username', 'password', 'email', 'address', 'tel', 'facebook_id', 'line_id', 'type_user'];
		$userss = $this->model_api_users->get($filter, $field, null, $start, $select_field);
		$total = $this->model_api_users->count_all($filter, $field);

		$data['users'] = $userss;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Users',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	
	public function insert_users_post(){

		if(isset($_POST['type']) AND $_POST['type'] == 'user'){

			$save_data = [
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'address' => $this->input->post('address'),
				'tel' => $this->input->post('tel'),
				'facebook_id' => $this->input->post('facebook_id'),
				'line_id' => $this->input->post('line_id'),
				'type_user' => 'user',
			
			];

		}else{

			$save_data = [
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'type_user' => 'admin',
			];
		}
		
		
		$save_users = $this->model_api_users->store($save_data);
	
		if ($save_users) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Your data has been successfully stored into the database'
			], API::HTTP_OK);
	
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> cclang('data_not_change')
			], API::HTTP_NOT_ACCEPTABLE);
		}
	


	}



	
	
	/**
	 * @api {get} /users/detail Detail Users.
	 * @apiVersion 0.1.0
	 * @apiName DetailUsers
	 * @apiGroup users
	 * @apiHeader {String} X-Api-Key Userss unique access-key.
	 * @apiPermission Users Cant be Accessed permission name : api_users_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Userss.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of users.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError UsersNotFound Users data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_users_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name', 'surname', 'username', 'password', 'email', 'address', 'tel', 'facebook_id', 'line_id', 'type_user'];
		$data['users'] = $this->model_api_users->find($id, $select_field);

		if ($data['users']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Users',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Users not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /users/add Add Users.
	 * @apiVersion 0.1.0
	 * @apiName AddUsers
	 * @apiGroup users
	 * @apiHeader {String} X-Api-Key Userss unique access-key.
	 * @apiPermission Users Cant be Accessed permission name : api_users_add
	 *
 	 * @apiParam {String} Name Mandatory name of Userss. Input Name Max Length : 50. 
	 * @apiParam {String} Surname Mandatory surname of Userss. Input Surname Max Length : 50. 
	 * @apiParam {String} Username Mandatory username of Userss. Input Username Max Length : 50. 
	 * @apiParam {String} Password Mandatory password of Userss. Input Password Max Length : 50. 
	 * @apiParam {String} Email Mandatory email of Userss. Input Email Max Length : 50. 
	 * @apiParam {String} Address Mandatory address of Userss.  
	 * @apiParam {String} Tel Mandatory tel of Userss. Input Tel Max Length : 10. 
	 * @apiParam {String} Facebook_id Mandatory facebook_id of Userss.  
	 * @apiParam {String} Line_id Mandatory line_id of Userss.  
	 * @apiParam {String} Type_user Mandatory type_user of Userss. Input Type User Max Length : 30. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_users_add', false);

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('surname', 'Surname', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('facebook_id', 'Facebook Id', 'trim|required');
		$this->form_validation->set_rules('line_id', 'Line Id', 'trim|required');
		$this->form_validation->set_rules('type_user', 'Type User', 'trim|required|max_length[30]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'address' => $this->input->post('address'),
				'tel' => $this->input->post('tel'),
				'facebook_id' => $this->input->post('facebook_id'),
				'line_id' => $this->input->post('line_id'),
				'type_user' => $this->input->post('type_user'),
			];
			
			$save_users = $this->model_api_users->store($save_data);

			if ($save_users) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /users/update Update Users.
	 * @apiVersion 0.1.0
	 * @apiName UpdateUsers
	 * @apiGroup users
	 * @apiHeader {String} X-Api-Key Userss unique access-key.
	 * @apiPermission Users Cant be Accessed permission name : api_users_update
	 *
	 * @apiParam {String} Name Mandatory name of Userss. Input Name Max Length : 50. 
	 * @apiParam {String} Surname Mandatory surname of Userss. Input Surname Max Length : 50. 
	 * @apiParam {String} Username Mandatory username of Userss. Input Username Max Length : 50. 
	 * @apiParam {String} Password Mandatory password of Userss. Input Password Max Length : 50. 
	 * @apiParam {String} Email Mandatory email of Userss. Input Email Max Length : 50. 
	 * @apiParam {String} Address Mandatory address of Userss.  
	 * @apiParam {String} Tel Mandatory tel of Userss. Input Tel Max Length : 10. 
	 * @apiParam {String} Facebook_id Mandatory facebook_id of Userss.  
	 * @apiParam {String} Line_id Mandatory line_id of Userss.  
	 * @apiParam {String} Type_user Mandatory type_user of Userss. Input Type User Max Length : 30. 
	 * @apiParam {Integer} id Mandatory id of Users.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */

	public function update_password_admin_post(){

		

			$save_data = [
				'password' => $this->input->post('new_password'),
			];
			
			$save_users = $this->model_api_users->change($this->post('id'), $save_data);
			
			if ($save_users || $save_data) { // ถ้ามีข้อมูลแล้วก็ให้ผ่าน
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

	
	}

	public function update_profile_admin_post(){


			$save_data = [
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'username' => $this->input->post('username'),
				//'password' => $this->input->post('password'),
				
			];
			
			$save_users = $this->model_api_users->change($this->post('id'), $save_data);
			
			if ($save_users || $save_data) { // ถ้ามีข้อมูลแล้วก็ให้ผ่าน
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		/* } else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		} */
	}

	public function update_post()
	{
		$this->is_allowed('api_users_update', false);

		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('surname', 'Surname', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('facebook_id', 'Facebook Id', 'trim|required');
		$this->form_validation->set_rules('line_id', 'Line Id', 'trim|required');
		$this->form_validation->set_rules('type_user', 'Type User', 'trim|required|max_length[30]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'address' => $this->input->post('address'),
				'tel' => $this->input->post('tel'),
				'facebook_id' => $this->input->post('facebook_id'),
				'line_id' => $this->input->post('line_id'),
				'type_user' => $this->input->post('type_user'),
			];
			
			$save_users = $this->model_api_users->change($this->post('id'), $save_data);

			if ($save_users) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /users/delete Delete Users. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteUsers
	 * @apiGroup users
	 * @apiHeader {String} X-Api-Key Userss unique access-key.
	 	 * @apiPermission Users Cant be Accessed permission name : api_users_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Userss .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_users_delete', false);

		$users = $this->model_api_users->find($this->post('id'));

		if (!$users) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Users not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_users->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Users deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Users not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Users.php */
/* Location: ./application/controllers/api/Users.php */