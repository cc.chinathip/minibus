
<?= $this->load->view('section/header'); ?>
  <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขผู้จำหน่าย</h1>
              </div>
              <form class="user" id="dealerSubmit"  action="<?= base_url('backend/dealer/update_dealer'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อผู้จำหน่าย (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อผู้จำหน่ายภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ชื่อผู้จำหน่าย (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อผู้จำหน่ายภาษาอังกฤษ" >
                  </div>
                </div>
               <!--  <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                              
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="dealer_image"  accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
				</div> -->
				
				<div class="form-group row">
					<div class="col-sm-6">
					<label for="sel1">เบอร์โทร</label>
						<input type="text" class="form-control form-control-user" id="tel" name="tel" placeholder="เบอร์โทร" required maxlength="10" onkeypress="allowNumbersOnly(event)">
					</div>
				</div>

				<div class="form-group row">
                  <div class="col-sm-12">
                  <label for="sel1">ที่อยู่ (ภาษาไทย)</label>
				 	 <textarea placeholder="ที่อยู่ภาษาไทย..." class="form-control form-control-address" name="address_thai" id="address_thai" ></textarea>
                  </div>
                </div>

				<div class="form-group row">
				<div class="col-sm-12">
                  <label for="sel1">ที่อยู่ (ภาษาอังกฤษ)</label>
				  	<textarea placeholder="ที่อยู่ภาษาอังกฤษ..." class="form-control form-control-address" name="address_eng" id="address_eng" ></textarea>
                  </div>
				</div>

								<input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="update" >
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
                                <input type="hidden"  id="old_image"  name="old_image" >

								<div class="form-group row">
                  <div class="col-sm-4 mb-3 mb-sm-0">
											<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
                  </div>
                  <div class="col-sm-4">
											<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
                  </div>
                </div>
									


              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		 CKEDITOR.replace("address_thai");
		 CKEDITOR.replace("address_eng");

	  $(document).ready(function() {
      
			

    
    $("#imgInp").change(function(){
        preview_image_one(this);

       
		});

		var apikey =  "<?= API_KEY; ?>"; 
		    var api_url = "<?= API_URL; ?>";

        $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Dealer/detail',
                data:"id="+'<?= $_GET['id'] ?>',
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.dealer)
                    $('#name_thai').val(data.data.dealer.name_thai)
                    $('#name_eng').val(data.data.dealer.name_eng)
                    $('#target').attr('src','<?= base_url('/assets/images/dealer/') ?>'+data.data.dealer.img_dealer);
					$('#address_thai').val(data.data.dealer.address_thai);
					$('#address_eng').val(data.data.dealer.address_eng);
					$('#tel').val(data.data.dealer.tel);
                    //console.log('data.data.dealer.dealer_end',data.data.dealer.dealer_end)
                    $('#old_image').val(data.data.dealer.img_dealer);
				}
			});
		
		
		
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
