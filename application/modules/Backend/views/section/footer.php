
  <script src="<?php echo base_url('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url('/assets/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url('/assets/js/sb-admin-2.min.js') ?>"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url('/assets/vendor/chart.js/Chart.min.js') ?>"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url('/assets/js/demo/chart-area-demo.js') ?>"></script>
	<script src="<?php echo base_url('/assets/js/demo/chart-pie-demo.js') ?>"></script>
	<script src="<?php echo base_url('/assets/vendor/datatables/jquery.dataTables.min.js') ?>"></script><!-- จัดการ datatable -->
  <script src="<?php echo base_url('/assets/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>
	<!-- Page level custom scripts -->
	<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script><!-- switch button -->
  <script src="<?php echo base_url('/assets/js/demo/datatables-demo.js') ?>"></script><!-- custom datatable -->
  

  <script src="<?php echo base_url('/assets/js/function-custom-footer.js') ?>"></script>
  <script src="<?php echo base_url('/assets/js/upload-video.js') ?>"></script>
  </body>

</html>
