<?= $this->load->view('section/header'); ?>
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li> 
		    <li class="breadcrumb-item active"><span>ติดต่อเรา</span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">ติดต่อเรา</h2>
	</div>
</div> 

<div class="section section-contactus wow fadeIn">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 left">
				<div id="map" class="googlemap"></div>
				<div class="company-info">
					<h3 class="title-sm">บริษัท แพนเทอรา มอเตอร์ส จำกัด</h3>
					<p>355 ถนนบอนด์สตรีท ตำบลบางพูด อำเภอปากเกร็ด จังหวัดนนทบุรี 11120</p>

					<p class="blue">
						โทรศัพท์ : 0 2503 4116 - 21<br>
						โทรสาร  : 0 2503 4400<br>
						เวลาทำการ : 08.30 - 17.00 น. <span class="norwrap f-small">(เว้นวันหยุดนักขัตฤกษ์)</span>
					</p>
				</div>
			</div><!--col-lg-6-->

			<div class="col-lg-5 right">
				<form class="form-contact">
					<div class="hgroup">
						<h3 class="title-md">ข้อมูลสำหรับติดต่อ</h3>
						<p>กรุณากรอกข้อมูลให้ถูกต้อง เพื่อความสะดวกในการรับบริการ</p>
					</div>

					<div class="input-block">
						<span class="input-text">ชื่อ-นามสกุล</span>
						<input type="text" class="form-control" name="" placeholder="กรอกชื่อ-นามสกุล">
					</div>

					<div class="input-block">
						<span class="input-text">อีเมล์</span>
						<input type="text" class="form-control" name="" placeholder="อีเมล์">
					</div>

					<div class="input-block">
						<span class="input-text">เบอร์โทรศัพท์ติดต่อ</span>
						<input type="text" class="form-control" name="" placeholder="กรอกเบอร์โทรศัพท์">
					</div>

					<div class="input-block">
						<span class="input-text">ข้อความ</span>
						<textarea class="form-control" placeholder="ระบุข้อความ..."></textarea>
					</div>

					<div class="input-block">
						<button class="btn btn-red has-arrow" type="submit">
							<span class="text">ลงทะเบียน</span>
							<span class="icon">
								<span class="arrow-right"></span>
							</span>
						</button>
					</div>
				</form>
			</div><!--col-lg-6-->
		</div><!--row-->
	</div><!--container-->
</div><!--section-contactus-->
<?= $this->load->view('section/footer'); ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap"></script>
<script> 
function initMap() { 
  var uluru = {lat: -25.344, lng: 131.036}; 
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: uluru}); 
  var marker = new google.maps.Marker({position: uluru, map: map});
}
</script>
