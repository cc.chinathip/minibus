<?= $this->load->view('section/header'); ?>

<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li> 
		    <li class="breadcrumb-item active"><span>ศูนย์บริการ</span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">ศูนย์บริการ</h2>
	</div>
</div> 

<div class="section section-service-province">
	<div class="container">
		<div class="row mb-4 mb-lg-5">
			<div class="col-12">
				<h2 class="title-md">โปรดเลือกจังหวัด และ เขตหรืออำเภอ <span class="star">*</span></h2>
				<button class="btn btn-blue">
					<span>
						<span class="icon-white w22"><img class="svg-js" src=""></span> ใช้ตำแหน่งปัจจุบันของคุณ
					</span>
				</button>
			</div>
		</div><!--row-->

		<div class="row align-items-end">
			<div class="col-lg-4 col-md-6">
				<div class="input-block">
					<span class="input-text">เลือกจังหวัด</span>
					<div class="select-block">
						<select class="custom-select">
							<option>กรุงเทพฯ</option>
						</select>
					</div>
				</div><!--input-block-->
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="input-block">
					<span class="input-text">เลือกเขต/อำเภอ</span>
					<div class="select-block">
						<select class="custom-select">
							<option>วังทองหลาง</option>
						</select>
					</div>
				</div><!--input-block-->
			</div>

			<div class="col-lg-4 col-md-6">
				<button class="btn btn-submit btn-red has-arrow " type="submit">
					<span class="text">ลงทะเบียน</span>
					<span class="icon">
						<span class="arrow-right"></span>
					</span>
				</button>
			</div>
		</div><!--row-->
	</div><!--container-->
</div><!--section-service-province-->

<div class="section section-service-center">
	<div class="container show_dealer">
	
		
	</div><!--container-->
</div><!--section-service-center-->
<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Dealer/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.dealer)
					console.log('total',data.total)
					//console.log('datasssss',data.data.dealer[0].status)
					var html = '';
					 $.each(data.data.dealer, function(index,value ) {
						
						console.log('index',parseInt(index+1))
						
							var number_at = parseInt(index+1);
							html += '<ul class="service-center align-items-center wow fadeIn">'
							html += '<li class="number"><span>'+parseInt(index+1)+'</span></li>'
							html += '<li class="info">'
							html += '<h3 class="title-sm"><span class="number">'+parseInt(index+1)+'.</span>'+value.name_thai+'</h3>'
							html += '<p>'+value.address_thai+'</p>'
							html += '<div class="list">'
							html += '<div><span class="icons"></span>ผู้จำหน่าย</div>'

											html += '<div><span class="icons"></span>ศูนย์บริการทั่วไป</div>'
											html += '</div>'
											html += '</li>'

											html += '<li class="contact">'
											html += '<a class="item align-items-center" href="#">'
												html += '<span class="group">'
												html += '<img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-phone.svg') ?>" alt="">'
												html += '<p>โทรศัพท์</p>'
												html += '</span>'
											html += '</a>'

											html += '<a class="item last align-items-center" href="#">'
												html += '<span class="group">'
												html += '<img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-googlemap.svg') ?>" alt="">'
												html += '<p>เส้นทาง</p>'
												html += '</span>'
											html += '</a>'	 
												html += '</li>'
											
						    html += '</ul>'

						
					}); 
					
				
					$('.show_dealer').append(html);
					

					

					
					
				
					
				}
			}); 

			
		
	});

  </script>
