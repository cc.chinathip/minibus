<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function service_all(){	
		$data['title'] = 'บริการหลังการขายทั้งหมด';
		//$this->load->view('index');
		$this->load->view('service/service_all');
	}
	
	public function add_service(){	
		$data['title'] = 'เพิ่มบริการหลังการขาย';
	
		if(isset($_POST['insert'])){

			

			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('service_detail'),
				'detail_eng' => $this->input->post('service_detail_eng'),
				'img_service' =>  Rename_file($_FILES['service_image']['name']),
				//'video' => Rename_file($_FILES['service_video']['name']),
				'link_youtube' => $this->input->post('link_youtube'),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);
			/* echo $_FILES['service_video']['name'];
		exit(); */

			$data = $this->db->insert('service',$data);

			
			//$this->load->helper('string');

				/* $path_vdo = array(
					'upload_path' => './assets/videos/service/',
					'name_video' => Rename_file($_FILES['service_video']['name']),
					'name_input_video' => 'service_video'
				);
				check_upload_video($path_vdo,'insert'); */
			
				$path = array(
					'upload_path' => './assets/images/service/',
					'redirect_path1' => 'service/add_service_form',
					'redirect_path2' => 'service/service_all',
					'name_image' =>  Rename_file($_FILES['service_image']['name']),
					'name_input_image' => 'service_image',
				);
				check_upload($path,'insert');

			
				

			
			

			


		}else{

			$this->load->view('service/add_service_form');

		}
		
	}

	public function update_service(){

		$data['title'] = 'หน้าแก้ไขบริการหลังการขาย';
		$this->load->helper('function_custom');
		
				if(isset($_POST['update'])){
					
					if($_FILES['service_image']['name'] !== ''){

						$image = Rename_file($_FILES['service_image']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					}

					/* if($_FILES['service_video']['name'] !== ''){

						$video = Rename_file($_FILES['service_video']['name']); 

		
					}else{
						$video = $this->input->post('old_video'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					} */
					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
						'detail' => $this->input->post('service_detail'),
						'detail_eng' => $this->input->post('service_detail_eng'),
						'img_service' =>  $image,
						//'video' => $video,
						'link_youtube' => $this->input->post('link_youtube'),
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						//'status' => 'on',
					);

				
					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('service',$data);
					


				/* 	$path_vdo = array(
						'upload_path' => './assets/videos/service/',
						'name_video' => $video,
						'name_input_video' => 'service_video'
					);
					check_upload_video($path_vdo,'update'); */

					$path = array(
						'upload_path' => './assets/images/service/',
						'redirect_path1' => 'service/service_all',
						'redirect_path2' => 'service/service_all',
						'name_image' =>  $image,
						'name_input_image' => 'service_image'
					);
					check_upload($path,'update');

					
				
					
			}else{
				$this->load->view('service/update_service_form');
			}
	}
   
}
?>
