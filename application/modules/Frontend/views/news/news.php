<?= $this->load->view('section/header'); ?>
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Users/index'); ?>">หน้าหลัก</a></li>
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/News/news_list'); ?>">ข่าวสาร</a></li>
		    <li class="breadcrumb-item active"><span>ข่าวประชาสัมพันธ์</span></li> 
	  	</ol>
	</div>
</nav>
 
<div class="section section-news main pt-0">
	<div class="page-header wow fadeIn">
		<div class="container">
			<h2 class="title-xl">ข่าวสาร</h2>
		</div>
	</div> 
 	<div class="page-header-info wow fadeIn">
		<div class="container">
			<p>
				อัพเดตเรื่องราว บทความต่างๆที่น่าสนใจ ความเคลื่อนไหวของเรา<br>
				ให้คุณได้ทราบทั้งหมด
			</p>
		</div>
	</div>  
  
	<div class="container "> 
		<div class="row title-group">
			<div class="col-12 left"><h2 class="title-md">ข่าวประชาสัมพันธ์</h2></div> 
		</div>
		<div class="row space-0 wow fadeIn show_news">
		
        </div><!--row-->
       
	</div>
 
</div><!--section-news-->



<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		

		//dataSource: function(done) {
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/News/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					/* console.log('data',data.data.news)
					console.log('total',data.total) */
					
					var container = $(".show_news");//positon show form
					var sources = (function() {
						var result = [];
								for (var i = 1; i < data.data.news.length; i++) {
												result.push(data.data.news[i]);
												
								}
								
								return result;
					})();

					var options = {
						dataSource: sources,
						pageSize: 6, // total per page
						callback: function(response, pagination) {
						window.console && console.log(response, pagination);
						var html = '';				
						$.each(response, function(index,value ) {
							
							//console.log('index',parseInt(index+1))
							//console.log('value',value)
								html += '<div class="block_news col-lg-4 col-sm-6" >'
								html += '<div class="card card-info">'

									html += '<div class="card-photo">'
										html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/news/') ?>'+value.img_news+');" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+' ">'
										html += '<img class="image_size" src="<?= base_url('/assets/images/news/') ?>'+value.img_news+'" alt="">'
										html += '</a>'
										html += '</div>'

										html += '<div class="card-body">'
										
											html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
											html += '<p class="date">'+value.created_at+'</p>'

											html += '<div class="row align-items-center">'

												html += '<div class="col-6">'
													html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'
													html += '<span class="text">รายละเอียด</span>'
													html += '<span class="icon">'
														html += '<span class="arrow-right"></span>'
													html += '</span>'
													html += '</a>'
												html += '</div>'

												html += '<div class="col-6">'
													html += '<div class="share-wrap">'
													html += '<span class="icons icon-share"></span>'
													html += '<span class="text">แชร์ไปยัง : </span>'
													html += '<a class="icons icon-share-facebook" href="#"></a> '
													html += '<a class="icons icon-share-line" href="#"></a>'
													html += '</div>'
												html += '</div>'

											html += '</div>'

										html += '</div>'
									html += '</div>'

								html += '</div>'
								html += '</div>'

						}); 
						container.prev().html(html);
				  }

				};

									container.addHook("beforeInit", function() {
										window.console && console.log("beforeInit...");
									});
									container.pagination(options);

									container.addHook("beforePageOnClick", function() {
										window.console && console.log("beforePageOnClick...");
									//return false
									});
					
					
					//$('.show_news').append(html);

									
					
				}
			}); 
		//}

			
		
	});

  </script>
