
$(window).on("load scroll",function(e){   
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $("body").addClass("scrolling has-scroll"); 
    } else {
        $("body").removeClass("scrolling");
    }
});
  
$(document).ready(function(){
  /*------------[Start]  header------------*/    

  $('<div class="page-blocker"></div>').appendTo('body'); 
  $(".navbar-toggle").click(function () {
      $("html").toggleClass("menu-opened");
      $("html").addClass("closing");
  });

  $(".page-blocker").click(function () {
      $("html").toggleClass("menu-opened");
      setTimeout($.proxy(function(){
          $('html').removeClass("closing");
      },this),700);
  });
 

  $(function(){
    $('.menu-pc-hover').hover(function() {
      $('html').addClass('menu-pc-opened');
    }, function() {
      $('html').removeClass('menu-pc-opened');
    })
  });
   
  /*------------[Start] change color of SVG image using CSS ------------*/

  $('img.svg-js').each(function() {
      var $img = jQuery(this);
      var imgURL = $img.attr('src');
      var attributes = $img.prop("attributes");

      $.get(imgURL, function(data) {
          // Get the SVG tag, ignore the rest
          var $svg = jQuery(data).find('svg');

          // Remove any invalid XML tags
          $svg = $svg.removeAttr('xmlns:a');

          // Loop through IMG attributes and apply on SVG
          $.each(attributes, function() {
              $svg.attr(this.name, this.value);
          });

          // Replace IMG with SVG
          $img.replaceWith($svg);
      }, 'xml');
  });

});

  
$(window).on("load", function() {
  $("html").addClass("page-loaded");

  $('.preload').fadeOut();
 
  var isMobile = {
      Android: function() {
          return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function() {
          return navigator.userAgent.match(/IEMobile/i);
      },
      any: function() {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
  };

  if(isMobile.any()) {
      $("html").addClass("device");
  }else{
      $("html").addClass("pc");     
  }
  
});
 