
<!doctype html>
<html lang="th">
<head> 
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes"/> 

<!--link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" /-->

<title>บริษัท แพนเทอรา มอเตอร์ส จำกัด | Panthera Motors Minibus </title> 
<link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap&subset=thai" rel="stylesheet">
<link href="<?php echo base_url('assets/frontend/fonts/fontface.css') ?>" rel="stylesheet">   
<link href="<?php echo base_url('assets/frontend/css/bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/frontend/css/animate.css') ?>" rel="stylesheet">  
<link href="<?php echo base_url('assets/frontend/css/swiper.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/frontend/css/jquery.fancybox.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/frontend/css/global.css') ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/function-custom-header.js') ?>"></script>
<script src="<?php echo base_url('assets/frontend/js/jquery-1.12.4.min.js') ?>"></script>
<!-- <script type="text/javascript" src="<?php echo base_url('/assets/js/AutoProvince.js') ?>"></script> -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> <!-- alert form -->

<style>
	img.style_logo{
		width:183px;
	}
	
</style>
</head>
<body> 

<div class="preload"></div>
 
<div class="page wow fadeIn"> 

<!--=====================[Start]====================-->

<div class="section section-nav-panel">
	<div class="container">
		<div class="nav-panel"> 
			<div class="nav-header">
				<div class="navbar-brand"><a href="<?= base_url('frontend/Users/index'); ?>"><img class="style_logo"  src="<?php echo base_url('/assets/frontend/img/now_logo.png') ?>" alt=""></a></div>
				<div class="nav-close navbar-toggle"></div>
			</div>
			<ul class="nav nav-menu">
				<li class="nav-item <?= $this->uri->segment(3) === '' ? 'active' : '' ||  $this->uri->segment(3) === 'index' ? 'active' : '' ?>"><a href="<?= base_url('frontend/Users/index'); ?>"><span>หน้าหลัก</span></a></li>
				<li class="nav-item <?= $this->uri->segment(3) === 'type' ? 'active' : '' ?>"><a href="<?= base_url('frontend/Product/type'); ?>"><span>รุ่นรถ</span></a></li>
				<li class="nav-item <?= $this->uri->segment(3) === 'promotion' ? 'active' : '' ?>"><a href="<?= base_url('frontend/Promotion/promotion'); ?>"><span>โปรโมชั่น</span></a></li>
				<li class="nav-item <?= $this->uri->segment(3) === 'service' ? 'active' : '' ?>"><a href="<?= base_url('frontend/Service/service'); ?>"><span>บริการหลังการขาย</span></a></li>
				<li class="nav-item <?= $this->uri->segment(3) === 'news' ? 'active' : '' ?>"><a href="<?= base_url('frontend/News/news_list'); ?>"><span>ข่าวสาร</span></a></li>

				<li class="nav-item <?= $this->uri->segment(3) === 'aboutus' ? 'active' : '' ?> link_aboutus"></li>

				<li class="nav-item <?= $this->uri->segment(3) === 'contact' ? 'active' : '' ?>"><a href="<?= base_url('frontend/Contact/contact'); ?>"><span>ติดต่อเรา</span></a></li>
			</ul>
		</div><!--nav-panel-->
	</div><!--container-->
</div><!--section-nav-panel-->

<!--=====================[End]====================-->
<header class="header">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-3 col-sm-1">
				<div class="navbar-brand"><a href="<?= base_url('frontend/Users/index'); ?>"><img class="style_logo"    src="<?php echo base_url('/assets/frontend/img/now_logo.png') ?>" alt=""></a></div>
			</div><!--col-1-->
			<div class="col-9 col-sm-11">
				<div class="navbar-main align-items-center">
					
					<ul class="nav nav-followus">
						<li class="nav-item"><a href="#" target="_blank"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-facebook.svg') ?>" alt=""></a></li>
						<li class="nav-item"><a href="#" target="_blank"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-lineid.svg') ?>" alt=""></a></li>
						<li class="nav-item"><a href="#" target="_blank"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-youtube.svg') ?>" alt=""></a></li>
					</ul>

					<button class="btn btn-icon navbar-toggle" type="button">
	                  <span class="bar"></span> 
	                </button>
				</div><!--navbar-main-->
			</div><!--col-11-->
		</div><!--row-->
	</div><!--container-->
</header>

<ul class="shotcut-menu-sidebar">
	<li>
		<a href="<?= base_url('frontend/Product/buying'); ?>">
			<div class="group">
				<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-car.svg') ?>" alt=""></div>
				<h4>ซื้อรถ</h4>
			</div>
		</a>
	</li>
	<li>
		<a href="<?= base_url('frontend/Product/test_drive'); ?>">
			<div class="group">
				<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-test-drive.svg') ?>" alt=""></div>
				<h4>ทดลองขับ</h4>
			</div>
		</a>
	</li>
	<li>
		<a href="<?= base_url('frontend/Product/brochure'); ?>">
			<div class="group">
				<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-brochure.svg') ?>" alt=""></div>
				<h4>โบรชัวร์</h4>
			</div>
		</a>
	</li>
	<li>
		<a href="<?= base_url('frontend/Product/booking'); ?>">
			<div class="group">
				<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-booknow.svg') ?>" alt=""></div>
				<h4>จองวันนี้</h4>
			</div>
		</a>
	</li>
	<li>
		<a href="<?= base_url('frontend/Product/leasing'); ?>">
			<div class="group">
				<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-leasing.svg') ?>" alt=""></div>
				<h4>ลิสซิ่ง</h4>
			</div>
		</a>
	</li>
	<li>
		<a href="<?= base_url('frontend/Dealer/service_center'); ?>">
			<div class="group">
				<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-service-center.svg') ?>" alt=""></div>
				<h4>ศูนย์บริการ</h4>
			</div>
		</a>
	</li>
	</ul>
	
	<style>
	img.image_size {
   	 height: 250px;
	}
	.row.space-0.wow.fadeIn.show_news,.row.space-0.wow.fadeIn.show_activity,.row.space-0.wow.fadeIn.show_article{
    justify-content: center;
	}
	.image_color{
		display: block;
		width: 50px;
		height: 50px;
		background-color: #fff;
		border-radius: 50%;
		border: 1px solid transparent;
		cursor: pointer;
	}
	</style>
	
	<script>
	
(function($) {
  $.fn.AutoProvince = function(options) {
    var Setting = $.extend(
      {
        PROVINCE: "#province", // select div สำหรับรายชื่อจังหวัด
        AMPHUR: "#amphur", // select div สำหรับรายชื่ออำเภอ
        DISTRICT: "#district", // select div สำหรับรายชื่อตำบล
        POSTCODE: "#postcode", // input field สำหรับรายชื่อรหัสไปรษณีย์
        arrangeByName: false // กำหนดให้เรียงตามตัวอักษร
      },
      options
    );

    return this.each(function() {
      var xml;
      var dataUrl = "<?php echo base_url('/assets/js/thailand.xml') ?>";

      $(function() {
        initialize();
      });

      function initialize() {
        $.ajax({
          type: "GET",
          url: dataUrl,
          dataType: "xml",
          success: function(xmlDoc) {
            xml = $(xmlDoc);

            _loadProvince();
            addEventList();
          },
          error: function() {
            console.log("Failed to get xml");
          }
        });
      }

      function _loadProvince() {
        var list = [];
        xml.find("table").each(function(index) {
          if ($(this).attr("name") == Setting.PROVINCE.split("#")[1]) {
            var PROVINCE_ID = $(this)
              .children()
              .eq(0)
              .text();
            var PROVINCE_NAME = $(this)
              .children()
              .eq(2)
              .text();
            if (PROVINCE_ID)
              list.push({ id: PROVINCE_ID, name: PROVINCE_NAME });
          }
        });
        if (Setting.arrangeByName) {
          AddToView(list.sort(SortByName), Setting.PROVINCE);
        } else {
          AddToView(list, Setting.PROVINCE);
        }
      }

      function _loadAmphur(PROVINCE_ID_SELECTED) {
        var list = [];
        var isFirst = true;
        $(Setting.AMPHUR).empty();
        xml.find("table").each(function(index) {
          if ($(this).attr("name") == Setting.AMPHUR.split("#")[1]) {
            var AMPHUR_ID = $(this)
              .children()
              .eq(0)
              .text();
            var AMPHUR_NAME = $(this)
              .children()
              .eq(2)
              .text();
            var POSTCODE = $(this)
              .children()
              .eq(3)
              .text();
            var PROVINCE_ID = $(this)
              .children()
              .eq(5)
              .text();
            if (PROVINCE_ID_SELECTED == PROVINCE_ID && AMPHUR_ID) {
              if (isFirst) _loadDistrict(AMPHUR_ID);
              isFirst = false;
              list.push({
                id: AMPHUR_ID,
                name: AMPHUR_NAME,
                postcode: POSTCODE
              });
              $(Setting.POSTCODE).val(POSTCODE);
            }
          }
        });
        if (Setting.arrangeByName) {
          AddToView(list.sort(SortByName), Setting.AMPHUR);
        } else {
          AddToView(list, Setting.AMPHUR);
        }
      }

      function _loadDistrict(AMPHUR_ID_SELECTED) {
        var list = [];
        $(Setting.DISTRICT).empty();
        xml.find("table").each(function(index) {
          if ($(this).attr("name") == Setting.DISTRICT.split("#")[1]) {
            var DISTRICT_ID = $(this)
              .children()
              .eq(0)
              .text();
            var DISTRICT_NAME = $(this)
              .children()
              .eq(2)
              .text();
            var AMPHUR_ID = $(this)
              .children()
              .eq(3)
              .text();
            if (AMPHUR_ID_SELECTED == AMPHUR_ID && DISTRICT_ID) {
              list.push({ id: DISTRICT_ID, name: DISTRICT_NAME });
            }
          }
        });
        if (Setting.arrangeByName) {
          AddToView(list.sort(SortByName), Setting.DISTRICT);
        } else {
          AddToView(list, Setting.DISTRICT);
        }
      }

      function addEventList() {
        $(Setting.PROVINCE).change(function(e) {
          var PROVINCE_ID = $(this).val();
          _loadAmphur(PROVINCE_ID);
        });
        $(Setting.AMPHUR).change(function(e) {
          var AMPHUR_ID = $(this).val();
          $(Setting.POSTCODE).val(
            $(this)
              .find("option:selected")
              .attr("POSTCODE")
          );
          _loadDistrict(AMPHUR_ID);
        });
      }
      function AddToView(list, key) {
        for (var i = 0; i < list.length; i++) {
          if (key != Setting.AMPHUR) {
            $(key).append(
              "<option value='" + list[i].id + "'>" + list[i].name + "</option>"
            );
          } else {
            $(key).append(
              "<option value='" +
                list[i].id +
                "' POSTCODE='" +
                list[i].postcode +
                "'>" +
                list[i].name +
                "</option>"
            );
          }
        }
      }

      function SortByName(a, b) {
        var aName = a.name.toLowerCase();
        var bName = b.name.toLowerCase();
        return aName < bName ? -1 : aName > bName ? 1 : 0;
      }
    });
  };
})(jQuery);


	$('body').AutoProvince({
			PROVINCE:		'#province', // select div สำหรับรายชื่อจังหวัด
			AMPHUR:			'#amphur', // select div สำหรับรายชื่ออำเภอ
			arrangeByName:		false // กำหนดให้เรียงตามตัวอักษร
		});


	$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  '<?= API_URL ?>'+'api/About/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': '<?= API_KEY ?>',
				},
				success: function(data) {
				
					var html = '';
					 $.each(data.data.about, function(index,value ) {
						if(value.type === 'ประวัติองค์กร'){
							html += '<a href="<?= base_url('frontend/Aboutus/aboutus/'); ?>'+value.id+'" ><span>เกี่ยวกับเรา</span></a>'
						}
					}); 
					
					$('.link_aboutus').append(html);
					
				}
	}); 


	function color_car(api_url,apikey,color_product){

								$.ajax({
										//cache: true,
										type:'POST',
										async:false,
										url:  api_url+'api/Color/NameById',
										data:{color_id:color_product},
										xhrFields: {
											withCredentials: false
										},
										headers: {
											'X-Api-Key': apikey,
										},
										success: function(data) {

											//console.log('name color',data.data)
											
											var array_color_name = [];
											var img_color_value = [];
											$.each(data.data, function(index,value ) {

												//console.log('name color',value.img_color)
												array_color_name.push(value.name_thai);
												img_color_value.push('<li><span><img class="image_color" data-name='+value.name_thai+' src="<?php echo base_url('/assets/images/color/') ?>'+value.img_color+'" alt=""></span></li>')
												name_color = value.name_thai
												
											});
											
											$('.color-list').html(img_color_value)
										}

									})
									$('.text-color').html(name_color)
									$(".image_color").click(function(){ 
										console.log('name id',$(this).data('name'))
										$('.text-color').html($(this).data('name'))

										if($(this).data('name') === 'White'){

											$(this).addClass('white active')
										}
									})

	}
	
	$.fn.digits = function(){ 
		return this.each(function(){ 
			$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
		})
	}

	function detail_car(api_url,apikey,type_car){

$.ajax({
			//cache: true,
			type:'POST',
			async:false,
			url:  api_url+'api/Product/detail_ByType',
			data: {type_product:type_car},
			xhrFields: {
				withCredentials: false
			},
			headers: {
				'X-Api-Key': apikey,
			},
			success: function(data) {
				//console.log('data detail_ByType',data.data[0].id)
				
				
				/*  $.each(data.data.product, function(index,value ) {

				});  */
				$('.image_car').html('<img class="img-car wow fadeInRight" data-wow-delay="0.5s" src="<?php echo base_url('/assets/images/product/') ?>'+data.data[0].img_product+'" alt="">')
				$('.typecar_title').html(data.data[0].type_product)
				$('.car_price').html('ราคา '+data.data[0].price+' บาท')
				$('.car_price').digits()
				$('.detail_Exterior').html(data.data[0].Exterior)
				$('.detail_Utility').html(data.data[0].Utility)
				$('.detail_Performance').html(data.data[0].Performance)
				$('.detail_Safety').html(data.data[0].Safety)
				
				$('.car_price').attr('data-price',data.data[0].price);
				$('.typecar_title').attr('data-id',data.data[0].id)
				
				color_car(api_url,apikey,data.data[0].color_product)
							
							
			}
		}); 
}

function pickTypeCar(api_url,apikey){


		detail_car(api_url,apikey,'รุ่นเครื่องยนต์ดีเซล') // show default detail

		
$.ajax({
			//cache: true,
			type:'POST',
			async:false,
			url:  api_url+'api/Product/detail_ByType',
			data: {type_product:'รุ่นเครื่องยนต์ดีเซล'},
			xhrFields: {
				withCredentials: false
			},
			headers: {
				'X-Api-Key': apikey,
			},
			success: function(data) {
				//console.log('data detail_ByType',data.data[0].id)
				
				
				/*  $.each(data.data.product, function(index,value ) {

				});  */
				$('.image_car').html('<img class="img-car wow fadeInRight" data-wow-delay="0.5s" src="<?php echo base_url('/assets/images/product/') ?>'+data.data[0].img_product+'" alt="">')
				$('.typecar_title').html(data.data[0].type_product)
				$('.car_price').html('ราคา '+data.data[0].price+' บาท')
				$('.car_price').digits()
				$('.detail_Exterior').html(data.data[0].Exterior)
				$('.detail_Utility').html(data.data[0].Utility)
				$('.detail_Performance').html(data.data[0].Performance)
				$('.detail_Safety').html(data.data[0].Safety)
				
				$('.car_price').attr('data-price',data.data[0].price);
				$('.typecar_title').attr('data-id',data.data[0].id)
				
				color_car(api_url,apikey,data.data[0].color_product)
							
							
			}
		}); 


	$(".typecars").click(function() {

		// switch button type car
		if($(this).data("id") === 'รุ่นเครื่องยนตร์ ngv'){
			//console.log('posss')
			$('button.btn.btn-black.typecars.ngv').removeClass('btn-black').addClass('btn-blue')
			$('button.btn.btn-blue.typecars.desale').removeClass('btn-blue').addClass('btn-black')
			
		}else{
			
			$('button.btn.btn-black.typecars.desale').removeClass('btn-black').addClass('btn-blue')
			$('button.btn.btn-blue.typecars.ngv').removeClass('btn-blue').addClass('btn-black')
			
		}
		

		detail_car(api_url,apikey,$(this).data("id")) // show  detail when clicked

		
	});
	
}

	
		
</script>
	
