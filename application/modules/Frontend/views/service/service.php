<?= $this->load->view('section/header'); ?>

<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li>
		    <li class="breadcrumb-item active"><span>บริการหลังการขาย</span></li> 
	  	</ol>
	</div>
</nav>

<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">บริการหลังการขาย</h2>
	</div>
</div> 

<div class="section section-testimonial pt-0">
	
	<div class="container">
		<div class="section-header">
			<h2 class="title-lg">“ ประสบการณ์จริง จากผู้ใช้งาน ”</h2>
		</div>

		<div class="row space-0 wow fadeIn show_service">

			<!-- <div class="col-lg-4 col-sm-6">
				<div class="card card-info">
					<div class="card-photo video">
						<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-800x525--8.jpg') ?>);"  data-fancybox href="https://www.youtube.com/watch?v=VkfMKWY1aT0">
							<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-100x66--blank.png') ?>" alt="">
						</a>
					</div>

					<div class="card-body">
						<h2 class="title-sm"><a data-fancybox href="https://www.youtube.com/watch?v=VkfMKWY1aT0">ประสบการณ์จากผู้ใช้จริง : คุณสุรสีห์ เสือคง อาชีพวิศวกร</a></h2>

						<div class="row align-items-center">
							<div class="col-6">
								<p class="date">25 July 2019</p>
							</div>

							<div class="col-6">
								<div class="share-wrap">
									<span class="icons icon-share"></span>
									<span class="text">แชร์ไปยัง : </span>
									<a class="icons icon-share-facebook" href="#"></a>
									<a class="icons icon-share-line" href="#"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> -->
			
		
		</div><!--row-->
	</div><!--container-->
</div><!--section-testimonial-->

<div class="section section-service">
	<div class="container">
		<div class="row space-20">
			<div class="col-xl-6">
				<div class="card card-service md">
					<div class="background"> 
						<div class="b1 wow fadeInRight" data-wow-delay="0.2s"></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.4s"></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.7s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-genuine-service.png') ?>);"></div>
					</div>
					<div class="caption wow fadeIn" data-wow-delay="0.5s">
						<h4>Genuine Service</h4>
						<p>คุณภาพสินค้า และบริการที่ดี พร้อมบริการช่วยเหลือฉุกเฉินตลอด 24 ชม</p>
					</div>
				</div><!--card-service-->
			</div><!--col-xl-6-->

			<div class="col-xl-6">
				<div class="card card-service md">
					<div class="background"> 
						<div class="b1 wow fadeInRight" data-wow-delay="0.2s"></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.4s"></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.7s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-genuine-parts.png') ?>);"></div>
					</div>
					<div class="caption wow fadeIn" data-wow-delay="0.5s">
						<h4>Genuine Parts</h4>
						<p>อะไหล่แท้ที่ตอบโจทย์ และเป็นศูนย์กลางอะไหล่ ช่วยลดทอนเวลาในการรอ พร้อมทั้งการคุ้มครองชิ้นส่วนในระยะยาว</p>
					</div>
				</div><!--card-service-->
			</div><!--col-xl-6-->

			<div class="col-xl-6">
				<div class="card card-service md">
					<div class="background"> 
						<div class="b1 wow fadeInRight" data-wow-delay="0.2s"></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.4s"></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.7s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-genuine-technician.png') ?>);"></div>
					</div>
					<div class="caption wow fadeIn" data-wow-delay="0.5s">
						<h4>Genuine Technician</h4>
						<p> 
							ความเชี่ยวชาญที่เป็นของจริงของช่างที่ผ่านการฝึกฝนจาก Education Academy</p>
					</div>
				</div><!--card-service-->
			</div><!--col-xl-6-->

			<div class="col-xl-6">
				<div class="card card-service md">
					<div class="background"> 
						<div class="b1 wow fadeInRight" data-wow-delay="0.2s"></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.4s"></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.7s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-genuine-performance.png') ?>);"></div>
					</div>
					<div class="caption wow fadeIn" data-wow-delay="0.5s">
						<h4>Genuine Performance</h4>
						<p>
							การดูแลและเข้าใจในสินค้าและการบริการ เพื่อให้ได้ที่สุดของความพึงพอใจ และคุณภาพของรถยนต์ที่ยาวนาน</p>
					</div>
				</div><!--card-service-->
			</div><!--col-xl-6-->

			<div class="col-xl-6 offset-xl-3">
				<div class="card card-service md mb-0">
					<div class="background"> 
						<div class="b1 wow fadeInRight" data-wow-delay="0.2s"></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.4s"></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.7s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-genuine-accessibility.png') ?>);"></div>
					</div>
					<div class="caption wow fadeIn" data-wow-delay="0.5s">
						<h4 class="twoline">Our Genuine Network & Accessibility</h4>
						<p>
							ที่สุดของความง่ายในการเข้าถึงผ่าน Mobile App และศูนย์บริการที่มีเครือข่ายกว่า 200 แห่ง ทั่วประเทศ</p>
					</div>
				</div><!--card-service-->
			</div><!--col-xl-6-->
		</div><!--row-->
	</div><!--container-->
</div><!--section-service-->
 
<?= $this->load->view('section/footer'); ?>

<script>
	   $(document).ready(function() {
	
		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log('api_url ',api_url,'apikey ',apikey)
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Service/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.service)
					console.log('total',data.total)
					//console.log('datasssss',data.data.service[0].status)
					var html = '';
					 $.each(data.data.service, function(index,value ) {
						
						console.log('index',parseInt(index+1))
						
							var number_at = parseInt(index+1);

							html += '<div class="block_service col-lg-4 col-sm-6 '+(number_at > 3 ? 'Img_more' : '') +' " >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo video">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/service/') ?>'+value.img_service+');" data-fancybox href="'+value.link_youtube+'">'
									html += '<img class="image_service" src="<?= base_url('/assets/images/service/') ?>'+value.img_service+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a data-fancybox href="'+value.link_youtube+'">'+value.name_thai+'</a></h2>'
										
										html += '<div class="row align-items-center">'

												html += '<div class="col-6">'
												html += '<p class="date">'+value.created_at+'</p>'
												html += '</div>'

												html += '<div class="col-6">'
												html += '<div class="share-wrap">'
												html += '<span class="icons icon-share"></span>'
												html += '<span class="text">แชร์ไปยัง : </span>'
												html += '<a class="icons icon-share-facebook" href="#"></a> '
												html += '<a class="icons icon-share-line" href="#"></a>'
												html += '</div>'
												html += '</div>'

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'

							 
						//}
						
						
					}); 
					html += '<div class="col-md-12">'
					html += '<div class="buttons pt-4 pb-0 wow fadeIn">'
					html += '	<button class="btn btn-blue btn_more">'
					html += '	<span class="text">แสดงเพิ่มเติม</span>'
					html += '	</button>'
					html += '</div>'
					html += '</div>'

					html += '<div class="col-md-12">'
					html += '<div class="buttons pt-4 pb-0 wow fadeIn">'
					html += '	<button class="btn btn-blue btn_less">'
					html += '	<span class="text">แสดงน้อยลง</span>'
					html += '	</button>'
					html += '</div>'
					html += '</div>'
					
					
					$('.show_service').append(html);
					$('.btn_less').hide()

					if($('.block_service').hasClass('Img_more')){

						$('.Img_more').hide()
					}

					$(".btn_more").click(function() {
						
						$('.Img_more').show()
						$('.btn_more').hide()
						$('.btn_less').show()
					});

					$(".btn_less").click(function() {
						
						$('.Img_more').hide()
						$('.btn_more').show()
						$('.btn_less').hide()
						
					});

					

					
					
				
					
				}
			}); 

			
		
	});

  </script>
