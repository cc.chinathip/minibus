
<?= $this->load->view('section/header'); ?>
  <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขกิจกรรม</h1>
              </div>
              <form class="user" id="activitySubmit"  action="<?= base_url('backend/activity/update_activity'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อกิจกรรม (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อกิจกรรมภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ชื่อกิจกรรม (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อกิจกรรมภาษาอังกฤษ" >
                  </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="activity_image">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
								</div>

								<div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
											<label for="inputGroupFile01">วันที่เริ่มกิจกรรม</label>
											<input type="datetime" class="form-control form-control-user" id="activity_start" name="activity_start" required>
                  </div>
                  <div class="col-sm-6">
									<label for="inputGroupFile01">วันที่สิ้นสุดกิจกรรม</label>
											<input type="datetime" class="form-control form-control-user" id="activity_end" name="activity_end" required>
                  </div>
                </div>

                <div class="form-group">
                <label for="sel1">รายละเอียดกิจกรรม (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียดกิจกรรม..." class="form-control form-control-address" name="activity_detail" id="activity_detail" required></textarea>
								</div>

								<div class="form-group">
                <label for="sel1">รายละเอียดกิจกรรม (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดกิจกรรม..." class="form-control form-control-address" name="activity_detail_eng" id="activity_detail_eng" required></textarea>
								</div>
								
                                <input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
																<input type="hidden"  name="update" >
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
                                <input type="hidden"  id="old_image"  name="old_image" >
									<div class="form-group row">
												<div class="col-sm-4 mb-3 mb-sm-0">
														<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
												</div>
												<div class="col-sm-4">
														<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
												</div>
										</div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
			CKEDITOR.replace("activity_detail");
			CKEDITOR.replace("activity_detail_eng");
			
	  $(document).ready(function() {
      
			

     
    $("#imgInp").change(function(){
        preview_image_one(this);

       
		});
    
		$( "#activity_start" ).datepicker({
            dateFormat: 'yy-mm-dd' 
        });
		$( "#activity_end" ).datepicker({
            dateFormat: 'yy-mm-dd' 
        });

        var apikey =  "<?= API_KEY; ?>"; 
		    var api_url = "<?= API_URL; ?>";

        $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Activity/detail',
                data:"id="+'<?= $_GET['id'] ?>',
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.activity)
                    $('#name_thai').val(data.data.activity.name_thai)
                    $('#name_eng').val(data.data.activity.name_eng)
                    $('#target').attr('src','<?= base_url('/assets/images/activity/') ?>'+data.data.activity.img_activity);
										$('#activity_detail').val(data.data.activity.detail);
										$('#activity_detail_eng').val(data.data.activity.detail_eng);
                    $('#activity_start').val(data.data.activity.activity_start)
                    $('#activity_end').val(data.data.activity.activity_end)
                    console.log('data.data.activity.activity_end',data.data.activity.activity_end)
                    $('#old_image').val(data.data.activity.img_activity);
				}
			});

	});


	</script>
  <!-- Bootstrap core JavaScript-->
