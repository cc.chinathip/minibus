
<?= $this->load->view('section/header'); ?>
  <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page">
						<?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลบริษัท</h1>
              </div>
              <form class="user" id="aboutSubmit"  action="<?= base_url('backend/About/add_about'); ?>" enctype="multipart/form-data" method="post">
              
                <div class="form-group">
              <label for="sel1">ประเภทหัวข้อ</label>
              <select class="form-control" id="typeSelect" name="typeSelect" required>
              <option value=''>---เลือกประเภทหัวข้อ---</option>
              <option value='ประวัติองค์กร'>ประวัติองค์กร</option>
              <option value='นโยบายและการบริหาร'>นโยบายและการบริหาร</option>
              </select>
            </div>
              
              <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0 name_thai">
                  <label for="sel1">ชื่อหัวข้อ (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อหัวข้อภาษาไทย" required>
                  </div>
                  <div class="col-sm-6 name_eng">
                  <label for="sel1">ชื่อหัวข้อ (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อหัวข้อภาษาอังกฤษ" >
                  </div>
                </div>
                
                <div class="form-group about_image">
                <label for="sel1">ภาพหน้าปกหัวข้อ</label>
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="about_image" required accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
								</div>
								<div class="form-group about_detail">
                <label for="sel1">รายละเอียดข้อมูล (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียด..." class="form-control form-control-address" name="about_detail" required id="about_detail"></textarea>
								</div>

								<div class="form-group about_detail_eng">
									<label for="sel1">รายละเอียดข้อมูล (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียด..." class="form-control form-control-address" name="about_detail_eng" required id="about_detail_eng"></textarea>
                </div>
                
                <div class="form-group detail_vision">
                <label for="sel1">รายละเอียดหัวข้อวิสัยทัศน์ <!-- (ภาษาไทย) --></label>
									<textarea placeholder="รายละเอียดวิสัยทัศน์..." class="form-control form-control-address" name="detail_vision"  id="detail_vision"></textarea>
								</div>

							<!-- 	<div class="form-group detail_vision_eng">
									<label for="sel1">รายละเอียดวิสัยทัศน์ (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดวิสัยทัศน์..." class="form-control form-control-address" name="detail_vision_eng"  id="detail_vision_eng"></textarea>
								</div> -->


								<div class="form-group detail_more_image">
                <label for="sel1">ภาพหัวข้อรายละเอียดเพิ่มเติม (1)</label>
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="detail_more_imgInp"
                                aria-describedby="inputGroupFileAddon01" name="detail_more_image"  accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="detail_more_target" src="#"/>
                    </div>
								</div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0 name_thai">
                  <label for="sel1">ชื่อหัวข้อรายละเอียดเพิ่มเติม (1)</label>
                    <input type="text" class="form-control form-control-user" id="name_detail_more" name="name_detail_more" placeholder="ชื่อหัวข้อรายละเอียดเพิ่มเติม (1)" >
                  </div>
                </div>

								<div class="form-group detail_more">
									<label for="sel1">รายละเอียดเพิ่มเติม (1)</label>
									<textarea  class="form-control form-control-address" name="detail_more"  id="detail_more"></textarea>
								</div>


								<div class="form-group detail_more_image2">
                <label for="sel1">ภาพหัวข้อรายละเอียดเพิ่มเติม (2)</label>
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="detail_more_imgInp2"
                                aria-describedby="inputGroupFileAddon01" name="detail_more_image2"  accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="detail_more_target2" src="#"/>
                    </div>
								</div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0 name_thai">
                  <label for="sel1">ชื่อหัวข้อรายละเอียดเพิ่มเติม (2)</label>
                    <input type="text" class="form-control form-control-user" id="name_detail_more2" name="name_detail_more2" placeholder="ชื่อหัวข้อรายละเอียดเพิ่มเติม (2)" >
                  </div>
                </div>

								<div class="form-group detail_more2">
									<label for="sel1">รายละเอียดเพิ่มเติม (2)</label>
									<textarea  class="form-control form-control-address" name="detail_more2"  id="detail_more2"></textarea>
								</div>



								<div class="form-group detail_more_image3">
                <label for="sel1">ภาพหัวข้อรายละเอียดเพิ่มเติม (3)</label>
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="detail_more_imgInp3"
                                aria-describedby="inputGroupFileAddon01" name="detail_more_image3"  accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="detail_more_target3" src="#"/>
                    </div>
								</div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0 name_thai">
                  <label for="sel1">ชื่อหัวข้อรายละเอียดเพิ่มเติม (3)</label>
                    <input type="text" class="form-control form-control-user" id="name_detail_more3" name="name_detail_more3" placeholder="ชื่อหัวข้อรายละเอียดเพิ่มเติม (3)" >
                  </div>
                </div>

								<div class="form-group detail_more3">
									<label for="sel1">รายละเอียดเพิ่มเติม (3)</label>
									<textarea  class="form-control form-control-address" name="detail_more3"  id="detail_more3"></textarea>
								</div>
								
						
								
							
								<input type="hidden"  name="created_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="insert" >
			
									<div class="form-group row">
											<div class="col-sm-4 mb-3 mb-sm-0">
													<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
											</div>
											<div class="col-sm-4">
													<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
											</div>
									</div>
								
								
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		CKEDITOR.replace("about_detail");
    CKEDITOR.replace("about_detail_eng");
    CKEDITOR.replace("detail_vision");
    CKEDITOR.replace("detail_vision_eng");
		CKEDITOR.replace("detail_more");
    CKEDITOR.replace("detail_more2");
    CKEDITOR.replace("detail_more3");

		setEmptySrcImg('detail_more_target')
		setEmptySrcImg('detail_more_target2')
		setEmptySrcImg('detail_more_target3')

    $('.name_thai').hide()
    $('.name_eng').hide()
    $('.about_image').hide()
    $('.about_detail').hide()
    $('.about_detail_eng').hide()
    $('.detail_vision').hide()
    $('.detail_vision_eng').hide()
		$('.detail_more').hide()
    $('.detail_more2').hide()
		$('.detail_more3').hide()
		$('.detail_more_image').hide()
    $('.detail_more_image2').hide()
    $('.detail_more_image3').hide()
    
	  $(document).ready(function() {
      
			
    $("#typeSelect").change(function(){
        console.log('type selected',$(this).val())
        if($(this).val() === 'ประวัติองค์กร'){
          $('.name_thai').show()
          $('.name_eng').show()
          $('.about_image').show()
          $('.about_detail').show()
          $('.about_detail_eng').show()
          $('.detail_vision').show()
          $('.detail_vision_eng').show()
					$('.detail_more').show()
					$('.detail_more2').show()
					$('.detail_more3').show()
					$('.detail_more_image').show()
					$('.detail_more_image2').show()
					$('.detail_more_image3').show()

        }else{
          $('.name_thai').show()
          $('.name_eng').show()
          $('.about_image').show()
          $('.about_detail').show()
          $('.about_detail_eng').show()
          $('.detail_vision').hide()
          $('.detail_vision_eng').hide()
					$('.detail_more').show()
					$('.detail_more2').show()
					$('.detail_more3').show()
					$('.detail_more_image').show()
					$('.detail_more_image2').show()
					$('.detail_more_image3').show()

        }

       
		});
    
    $("#imgInp").change(function(){
        preview_image_one(this);

       
		});

		$("#detail_more_imgInp").change(function(){
			preview_image_new(this,'detail_more_target');
			//console.log('sd',this)
		});

		$("#detail_more_imgInp2").change(function(){
			preview_image_new(this,'detail_more_target2');
			//console.log('sd',this)
		});


		$("#detail_more_imgInp3").change(function(){
			preview_image_new(this,'detail_more_target3');
			//console.log('sd',this)
		});
    
	
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
