
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
			<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขรหัสผ่าน</h1>
              </div>
              <form class="user" id="update_password_form">
                <div class="form-group row">
                  <div class="col-sm-12 mb-3 mb-sm-0">
				  <label for="sel1">รหัสผ่านเดิม</label>
                    <input type="text" class="form-control form-control-user" id="old_password" name="old_password" placeholder="รหัสผ่านเดิม" required>
                  </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                    <label for="sel1">รหัสผ่านใหม่</label>
                        <input type="password" class="form-control form-control-user" id="new_password" name="new_password" placeholder="รหัสผ่านใหม่" required>
                    </div>
                </div>

                <div class="form-group row">
									<div class="col-sm-12 mb-3 mb-sm-0">
									<label for="sel1">ยืนยันรหัสผ่านใหม่</label>
                  	<input type="password" class="form-control form-control-user" id="confirm_password" name="confirm_password" placeholder="ยืนยันรหัสผ่านใหม่" required >
									</div>
                </div>

                <input type="hidden" id="id" name="id" value="<?= $this->session->userdata("session_userid") ?>">
              
										<div class="form-group row">
												<div class="col-sm-4 mb-3 mb-sm-0">
														<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
												</div>
												<div class="col-sm-4">
														<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
												</div>
										</div>
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>

	  $(document).ready(function() {

		//$('#update_password_form').on("submit", function(e){ 
		//e.preventDefault();
			var apikey =  "<?= API_KEY; ?>"; 
			var api_url = "<?= API_URL; ?>";
            var user_id = "<?= $this->session->userdata("session_userid") ?>";
            console.log('user_id',user_id)
			

                $('#update_password_form').on("submit", function(e){ 
                        e.preventDefault();
                        console.log('$("#update_password_form").serialize()',$("#update_password_form").serialize())

                        
                        $.ajax({
                            //cache: true,
                            type:'GET',
                            async:false,
                            url:   api_url+'api/Users/detail',
                            data: {id:user_id},
                            xhrFields: {
                            withCredentials: false
                            },
                            headers: {
                                'X-Api-Key': apikey,
                    
                            },
                            success: function(data) {
                                console.log('data222',data.data.users.password)

                                if($('#old_password').val() === data.data.users.password && $('#new_password').val() === $('#confirm_password').val()){

                                    $.ajax({
                                            //cache: true,
                                            type:'POST',
                                            async:false,
                                            url:   api_url+'api/Users/update_password_admin',
                                            data: $("#update_password_form").serialize(),
                                            xhrFields: {
                                                withCredentials: false
                                            },
                                            headers: {
                                                'X-Api-Key': apikey,
                                    
                                            },
                                            success: function(data) {
                                                console.log('update_password_form',data)
                                                if(data.status == true){

                                                    Swal.fire({
                                                        title: 'success!',
                                                        text: 'แก้ไขรหัสผ่านสำเร็จ',
                                                        type: 'success',
                                                    })
                                                    setTimeout(function(){ 
                                                        window.location.replace('<?= base_url('backend/Admin/Dashboard'); ?>') 
                                                    }, 2000);
                                                    
                                                }else{
                                                    
                                                    Swal.fire({
                                                        title: 'error!',
                                                        text: 'แก้ไขรหัสผ่านล้มเหลว',
                                                        type: 'error',
                                                    })
                                                }
                                            
                                                
                                            }
                                        }); 

                                }else{

                                       
                                        if($('#new_password').val() !== $('#confirm_password').val()){

                                            Swal.fire({
                                                title: 'error!',
                                                text: 'รหัสผ่านใหม่ไม่ตรงกัน',
                                                type: 'error',
                                            })

                                        }
                                        if($('#old_password').val() !== data.data.users.password){

                                            Swal.fire({
                                                title: 'error!',
                                                text: 'รหัสผ่านเดิมไม่ถูกต้อง',
                                                type: 'error',
                                            })

                                        }

                                }
                                    
                               
                            }
                        }); 
                        
               
                });
		//});
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
