<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function article_all(){	
		$data['title'] = 'บทความทั้งหมด';
		//$this->load->view('index');
		$this->load->view('article/article_all');
	}
	
	public function add_article(){	
		$data['title'] = 'เพิ่มบทความ';
		$this->load->helper('function_custom');
		//$this->load->view('index');
		
		if(isset($_POST['insert'])){

			

			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('article_detail'),
				'detail_eng' => $this->input->post('article_detail_eng'),
				'img_article' =>  Rename_file($_FILES['article_image']['name']),
				//'video' => Rename_file($_FILES['article_video']['name']),
				'link_youtube' => $this->input->post('link_youtube'),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);
			/* echo $_FILES['article_video']['name'];
		exit(); */

			$data = $this->db->insert('article',$data);

			
			//$this->load->helper('string');

				/* $path_vdo = array(
					'upload_path' => './assets/videos/article/',
					'name_video' => Rename_file($_FILES['article_video']['name']),
					'name_input_video' => 'article_video'
				);
				check_upload_video($path_vdo,'insert'); */
			
				$path = array(
					'upload_path' => './assets/images/article/',
					'redirect_path1' => 'article/add_article_form',
					'redirect_path2' => 'article/article_all',
					'name_image' =>  Rename_file($_FILES['article_image']['name']),
					'name_input_image' => 'article_image',
				);
				check_upload($path,'insert');

			
				

			
			

			


		}else{

			$this->load->view('article/add_article_form');

		}
		
	}

	public function update_article(){

		$data['title'] = 'หน้าแก้ไขบทความ';
		$this->load->helper('function_custom');
		
				if(isset($_POST['update'])){
					
					if($_FILES['article_image']['name'] !== ''){

						$image = Rename_file($_FILES['article_image']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					}

					/* if($_FILES['article_video']['name'] !== ''){

						$video = Rename_file($_FILES['article_video']['name']); 

		
					}else{
						$video = $this->input->post('old_video'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					} */
					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
						'detail' => $this->input->post('article_detail'),
						'detail_eng' => $this->input->post('article_detail_eng'),
						'img_article' =>  $image,
						//'video' => $video,
						'link_youtube' => $this->input->post('link_youtube'),
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						//'status' => 'on',
					);

				
					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('article',$data);
					


					/* $path_vdo = array(
						'upload_path' => './assets/videos/article/',
						'name_video' => $video,
						'name_input_video' => 'article_video'
					);
					check_upload_video($path_vdo,'update'); */

					$path = array(
						'upload_path' => './assets/images/article/',
						'redirect_path1' => 'article/article_all',
						'redirect_path2' => 'article/article_all',
						'name_image' =>  $image,
						'name_input_image' => 'article_image'
					);
					check_upload($path,'update');

					
				
					
			}else{
				$this->load->view('article/update_article_form');
			}
	}
   
}
?>
