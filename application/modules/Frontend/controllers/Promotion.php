<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class promotion extends MX_Controller {

    public function __construct()
	{
		parent::__construct();
		//$this->load->model('Users_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function promotion(){	
		$data['title'] = 'โปรโมชัน';

		$this->load->view('promotion/promotion');
		
	}

	public function promotion_detail($promotion_id=null){	
		$data['title'] = 'รายละเอียดโปรโมชัน';
		$data['promotion_id'] = $promotion_id;

		//echo $promotion_id;
		$this->load->view('promotion/promotion-detail',$data);
		
	}
}
