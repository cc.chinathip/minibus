<?= $this->load->view('section/header'); ?>

<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li> 
		    <li class="breadcrumb-item active"><span>ทดลองขับ</span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">ทดลองขับ</h2>
	</div>
</div> 

<div class="section section-banner-buyingcar">
	<div class="img-banner-group">
		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/banner-buying-car--1.jpg') ?>" alt="">
		<img class="img-overlay wow fadeInRight" src="<?php echo base_url('/assets/frontend/img/thumb/banner-buying-car--2.png') ?>" alt="">
	</div>

	<div class="banner-caption">
		<div class="container h-100">
			<p class="text-info">กรุณากรอกข้อมูลด้านล่างให้ครบถ้วน เพื่อให้เจ้าหน้าที่ของเราติดต่อกลับ</p>
			<div class="carinfo-box in-banner">
				<div class="row-01 wow fadeIn d-none d-md-block">
					<div class="inner">
						<p class="text">เลือกรุ่นที่คุณต้องการ<span class="star">*</span></p>
						<div class="buttons">
							<button class="btn btn-blue typecars desale"  data-id="รุ่นเครื่องยนต์ดีเซล"><span>เครื่องยนต์ดีเซล</span></button>
							<button class="btn btn-black typecars ngv" data-id="รุ่นเครื่องยนตร์ ngv"><span>เครื่องยนต์ NGV</span></button>
						</div>
					</div>
				</div><!--row-01-->
				<div class="row-02 wow fadeIn">
					<div class="inner">
						<h2 class="title-xl">Panthera Motors</h2>
						<h3 class="title-lg typecar_title"></h3>
						<p class="title-lg red car_price"></p>
					</div>
				</div><!--row-02-->
			</div><!--carinfo-box-->
		</div><!--container-->
	</div><!--banner-caption-->
</div><!--section-banner-buyingcar-->
  
<div class="section section-buyingcar wow fadeIn">
	<div class="container">
		<div class="carinfo-box in-banner mobile">
			<div class="row-01 wow fadeIn d-block d-md-none">
				<div class="inner">
					<p class="text">เลือกรุ่นที่คุณต้องการ<span class="star">*</span></p>
					<div class="buttons">
						<a class="btn btn-blue" href="#"><span>เครื่องยนต์ดีเซล</span></a>
						<a class="btn btn-black" href="#"><span>เครื่องยนต์ NGV</span></a>
					</div>
				</div>
			</div><!--row-01-->
		</div><!--carinfo-box-->
		
		<div class="buyingcar-contactus">
			<div class="row">
				<div class="col-md-6 col-xl-7 left">
					<div class="hgroup">
						<h3>ค้นหาผู้แทนจำหน่าย<span class="star">*</span></h3>
						<p>โปรดเลือกจังหวัด และเขต/อำเภอ</p>
					</div>

					<div class="row">
						<div class="col-xl-12">
							<button class="btn btn-blue">
								<span>
									<img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-pin.svg') ?>"> ใช้ตำแหน่งปัจจุบันของคุณ
								</span>
							</button>
						</div>
						<div class="col-xl-6">
							<div class="input-block">
								<span class="input-text">เลือกจังหวัด</span>
								<div class="select-block">
									<select class="custom-select"  id="province">
										<option>กรุงเทพฯ</option>
									</select>
								</div>
							</div><!--input-block-->
						</div>

						<div class="col-xl-6">
							<div class="input-block">
								<span class="input-text">เลือกเขต/อำเภอ</span>
								<div class="select-block">
									<select class="custom-select" id="amphur">
										<option>วังทองหลาง</option>
									</select>
								</div>
							</div><!--input-block-->
						</div>

						<div class="col-xl-6">
							<div class="input-block">
								<span class="input-text">เลือกผู้แทนจำหน่าย</span>
								<div class="select-block">
									<select class="custom-select" id="dealerSelect">
										<option>วังทองหลาง มอเตอร์บัส</option>
									</select>
								</div>
							</div><!--input-block-->
						</div>
					</div><!--row-->
				</div><!--col-md-6 col-xl-7-->
				<div class="col-md-6 col-xl-5 right">
					<div class="hgroup pb-sm-0">
						<h3>ข้อมูลสำหรับติดต่อ<span class="star">*</span></h3>
						<p>กรุณากรอกข้อมูลให้ถูกต้อง เพื่อความสะดวกในการรับบริการ</p>
					</div>

					<div class="row">
						<div class="col-xl-8">
							<div class="input-block">
								<span class="input-text">ชื่อ-นามสกุล</span>
								<input type="text" class="form-control" name="name" id="name" placeholder="กรอกชื่อ-นามสกุล">
							</div><!--input-block-->
						</div>
						<div class="col-xl-8">
							<div class="input-block">
								<span class="input-text">อีเมล์</span>
								<input type="text" class="form-control" name="email" id="email" placeholder="กรอกอีเมล์">
							</div><!--input-block-->
						</div>
						<div class="col-xl-8">
							<div class="input-block">
								<span class="input-text">เบอร์โทรศัพท์ติดต่อ</span>
								<input type="text" class="form-control"  name="tel" id="tel" placeholder="กรอกเบอร์โทรศัพท์">
							</div><!--input-block-->
						</div>
						<div class="col-xl-4">
							<button class="btn btn-red has-arrow regis" type="submit">
								<span class="text">ลงทะเบียน</span>
								<span class="icon">
									<span class="arrow-right"></span>
								</span>
							</button>
						</div>
					</div><!--row-->
				</div><!--col-md-6 col-xl-5-->
			</div><!--row-->
		</div><!--buyingcar-contactus-->
	</div><!--container-->
</div><!--section-buyingcar-->
 
<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
	
		pickTypeCar(api_url,apikey)
		getDealler(api_url, apikey)			
		add_order(api_url, apikey, 'ทดลอง')
			

			
		
	});

  </script>
