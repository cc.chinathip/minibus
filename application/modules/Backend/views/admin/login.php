
<link href="<?php echo base_url('/assets/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="<?php echo base_url('/assets/css/sb-admin-2.min.css') ?>" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="<?php echo base_url('/assets/vendor/jquery/jquery.min.js') ?>"></script>


  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-6 col-lg-6 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">เข้าใช้งานระบบหลังบ้าน</h1>
                  </div>
				  <form class="user"  action="<?= base_url('backend/Admin/check_login') ?>" method="post">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="username" name="username" aria-describedby="emailHelp" placeholder="ชื่อผู้ใช้" required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="รหัสผ่าน" required>
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                      </div>
                    </div>
										<input type="hidden" class="form-control form-control-user" id="type_user" name="type_user" value="admin">
										<button type="submit" class="btn btn-primary btn-user btn-block" >เข้าสู่ระบบ</button>
                   
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

	<?= $this->load->view('../section/footer'); ?>

	<!-- <script>

	  $(document).ready(function() {

		$('#LoginSubmit').on("submit", function(e){ 
		e.preventDefault();
			var apikey =  "<?= API_KEY; ?>";
			var api_url = "<?= API_URL; ?>";
			
				$.ajax({
					//cache: true,
					type:'POST',
					async:false,
					url:   api_url+'api/Users/username_password',
					data: $("#LoginSubmit").serialize(),
					xhrFields: {
					withCredentials: false
					},
					headers: {
						'X-Api-Key': apikey,
            
					},
					success: function(data) {
						console.log('data',data.data)
							if(data.data[0] !== '' && data.data[0] !== undefined){

								//console.log('data',data.data[0].username)
								Swal.fire({
									title: 'success!',
									text: 'เข้าสู่ระบบสำเร็จ',
									type: 'success',
								})

								$('.reset_data').click();

								$(".swal2-confirm.swal2-styled").click(function(){
									
									if($(this).data('clicked')) {
										//console.log('44444')
									}else{
									
										//window.location.replace('<?= base_url('backend/Admin/check_login') ?>');

										//$.redirect('<?= base_url('backend/Admin/check_login') ?>', $("#LoginSubmit").serialize());
										$.post("<?= base_url('backend/Admin/check_login') ?>",$("#LoginSubmit").serialize(), function(data) {
											console.log(data)
										location = '<?= base_url('backend/Admin/check_login') ?>';
									});
									}
							});
							
							}else{

								Swal.fire({
									title: 'error!',
									text: 'เข้าสู่ระบบล้มเหลว',
									type: 'error',
								})
							
							}
					}
				}); 
		});
		
	});


	</script> -->
