<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Orders extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_orders');
		$this->load->model('model_api_order_detail');
	}

	/**
	 * @api {get} /orders/all Get all orderss.
	 * @apiVersion 0.1.0
	 * @apiName AllOrders 
	 * @apiGroup orders
	 * @apiHeader {String} X-Api-Key Orderss unique access-key.
	 * @apiPermission Orders Cant be Accessed permission name : api_orders_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Orderss.
	 * @apiParam {String} [Field="All Field"] Optional field of Orderss : id, user_id, name, email, tel, province_id, amphure_id, dealer_id, qty_buy, date_buy, updated_at, updated_by, type_orders.
	 * @apiParam {String} [Start=0] Optional start index of Orderss.
	 * @apiParam {String} [Limit=10] Optional limit data of Orderss.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of orders.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataOrders Orders data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_orders_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'user_id', 'name', 'email', 'tel', 'province_id', 'amphure_id', 'dealer_id', 'qty_buy', 'date_buy', 'updated_at', 'updated_by', 'type_orders'];
		$orderss = $this->model_api_orders->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_orders->count_all($filter, $field);

		$data['orders'] = $orderss;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Orders',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	public function allorder_post()
	{

		$data = $this->model_api_orders->allorder_data($_POST['type_order']);

				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Orders',
			'data'	 	=> $data,
			//'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /orders/detail Detail Orders.
	 * @apiVersion 0.1.0
	 * @apiName DetailOrders
	 * @apiGroup orders
	 * @apiHeader {String} X-Api-Key Orderss unique access-key.
	 * @apiPermission Orders Cant be Accessed permission name : api_orders_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Orderss.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of orders.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError OrdersNotFound Orders data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_orders_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'user_id', 'name', 'email', 'tel', 'province_id', 'amphure_id', 'dealer_id', 'qty_buy', 'date_buy', 'updated_at', 'updated_by', 'type_orders'];
		$data['orders'] = $this->model_api_orders->find($id, $select_field);

		if ($data['orders']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Orders',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Orders not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /orders/add Add Orders.
	 * @apiVersion 0.1.0
	 * @apiName AddOrders
	 * @apiGroup orders
	 * @apiHeader {String} X-Api-Key Orderss unique access-key.
	 * @apiPermission Orders Cant be Accessed permission name : api_orders_add
	 *
 	 * @apiParam {String} User_id Mandatory user_id of Orderss. Input User Id Max Length : 10. 
	 * @apiParam {String} Name Mandatory name of Orderss. Input Name Max Length : 200. 
	 * @apiParam {String} Email Mandatory email of Orderss. Input Email Max Length : 50. 
	 * @apiParam {String} Tel Mandatory tel of Orderss. Input Tel Max Length : 10. 
	 * @apiParam {String} Province_id Mandatory province_id of Orderss. Input Province Id Max Length : 10. 
	 * @apiParam {String} Amphure_id Mandatory amphure_id of Orderss. Input Amphure Id Max Length : 10. 
	 * @apiParam {String} Dealer_id Mandatory dealer_id of Orderss. Input Dealer Id Max Length : 10. 
	 * @apiParam {String} Qty_buy Mandatory qty_buy of Orderss. Input Qty Buy Max Length : 10. 
	 * @apiParam {String} Date_buy Mandatory date_buy of Orderss.  
	 * @apiParam {String} Updated_at Mandatory updated_at of Orderss.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Orderss. Input Updated By Max Length : 50. 
	 * @apiParam {String} Type_orders Mandatory type_orders of Orderss. Input Type Orders Max Length : 20. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	

	public function add_post(){

		$this->is_allowed('api_orders_add', false);

		//$this->form_validation->set_rules('user_id', 'User Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		/* $this->form_validation->set_rules('province_id', 'Province Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('amphure_id', 'Amphure Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('dealer_id', 'Dealer Id', 'trim|required|max_length[10]'); */
		//$this->form_validation->set_rules('qty_buy', 'Qty Buy', 'trim|required|max_length[10]');
		//$this->form_validation->set_rules('date_buy', 'Date Buy', 'trim|required');
		/* $this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]'); */
		$this->form_validation->set_rules('type_orders', 'Type Orders', 'trim|required|max_length[20]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				//'user_id' => $this->input->post('user_id'),
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'tel' => $this->input->post('tel'),
				'province_id' => $this->input->post('province_id'),
				'amphure_id' => $this->input->post('amphure_id'),
				'dealer_id' => $this->input->post('dealer_id'),
				//'qty_buy' => $this->input->post('qty_buy'),
				//'date_buy' => date('Y-m-d H:i:s'),
				/* 'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'), */
				'type_orders' => $this->input->post('type_orders'),
			];
			
			$save_orders = $this->model_api_orders->store($save_data);

			$insert_id = $this->db->insert_id();

			if ($save_orders) {
				
				//$this->form_validation->set_rules('order_id', 'Order Id', 'trim|required|max_length[10]');
				$this->form_validation->set_rules('product_id', 'Product Id', 'trim|required|max_length[10]');
				$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[10]');
				//$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[10]');

				

				if ($this->form_validation->run()) {

					$save_data_order_detail = [
						'order_id' => $insert_id,
						'product_id' => $this->input->post('product_id'),
						//'qty' => $this->input->post('qty'),
						'price' => $this->input->post('price'),
						//'total' => $this->input->post('total'),
					];
					$save_order_detail = $this->model_api_order_detail->store($save_data_order_detail);


					if ($save_order_detail) {
						

						$this->response([
							'status' 	=> true,
							'message' 	=> 'Your data has been successfully stored into the database'
						], API::HTTP_OK);


					}else {
						$this->response([
							'status' 	=> false,
							'message' 	=> cclang('data_not_change')
						], API::HTTP_NOT_ACCEPTABLE);
					}

				} else {
					$this->response([
						'status' 	=> false,
						'message' 	=> validation_errors()
					], API::HTTP_NOT_ACCEPTABLE);
				}


			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /orders/update Update Orders.
	 * @apiVersion 0.1.0
	 * @apiName UpdateOrders
	 * @apiGroup orders
	 * @apiHeader {String} X-Api-Key Orderss unique access-key.
	 * @apiPermission Orders Cant be Accessed permission name : api_orders_update
	 *
	 * @apiParam {String} User_id Mandatory user_id of Orderss. Input User Id Max Length : 10. 
	 * @apiParam {String} Name Mandatory name of Orderss. Input Name Max Length : 200. 
	 * @apiParam {String} Email Mandatory email of Orderss. Input Email Max Length : 50. 
	 * @apiParam {String} Tel Mandatory tel of Orderss. Input Tel Max Length : 10. 
	 * @apiParam {String} Province_id Mandatory province_id of Orderss. Input Province Id Max Length : 10. 
	 * @apiParam {String} Amphure_id Mandatory amphure_id of Orderss. Input Amphure Id Max Length : 10. 
	 * @apiParam {String} Dealer_id Mandatory dealer_id of Orderss. Input Dealer Id Max Length : 10. 
	 * @apiParam {String} Qty_buy Mandatory qty_buy of Orderss. Input Qty Buy Max Length : 10. 
	 * @apiParam {String} Date_buy Mandatory date_buy of Orderss.  
	 * @apiParam {String} Updated_at Mandatory updated_at of Orderss.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Orderss. Input Updated By Max Length : 50. 
	 * @apiParam {String} Type_orders Mandatory type_orders of Orderss. Input Type Orders Max Length : 20. 
	 * @apiParam {Integer} id Mandatory id of Orders.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_orders_update', false);

		
		$this->form_validation->set_rules('user_id', 'User Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[200]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('province_id', 'Province Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('amphure_id', 'Amphure Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('dealer_id', 'Dealer Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('qty_buy', 'Qty Buy', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('date_buy', 'Date Buy', 'trim|required');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('type_orders', 'Type Orders', 'trim|required|max_length[20]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'user_id' => $this->input->post('user_id'),
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'tel' => $this->input->post('tel'),
				'province_id' => $this->input->post('province_id'),
				'amphure_id' => $this->input->post('amphure_id'),
				'dealer_id' => $this->input->post('dealer_id'),
				'qty_buy' => $this->input->post('qty_buy'),
				'date_buy' => $this->input->post('date_buy'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'),
				'type_orders' => $this->input->post('type_orders'),
			];
			
			$save_orders = $this->model_api_orders->change($this->post('id'), $save_data);

			if ($save_orders) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /orders/delete Delete Orders. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteOrders
	 * @apiGroup orders
	 * @apiHeader {String} X-Api-Key Orderss unique access-key.
	 	 * @apiPermission Orders Cant be Accessed permission name : api_orders_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Orderss .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_orders_delete', false);

		$orders = $this->model_api_orders->find($this->post('id'));

		if (!$orders) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Orders not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_orders->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Orders deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Orders not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Orders.php */
/* Location: ./application/controllers/api/Orders.php */