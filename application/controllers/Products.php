<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Controller {

    public function __construct()
	{
		parent::__construct();
		//$this->load->model('Users_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
public function index($cars=null){

		$data['title'] = 'รุ่นรถ';

		$this->load->view('product/car-model');
		
	}
	public function type($cars=null){

		$data['title'] = 'รุ่นรถ';

		$this->load->view('product/car-model');
		
	}

	public function buying(){	
		$data['title'] = 'ซื้อรถ';
		$this->load->view('product/buying-car');
	}

	public function test_drive(){	
		$data['title'] = 'ซื้อรถ';
		$this->load->view('product/test-drive');
	}

	public function booking(){	
		$data['title'] = 'จองรถ';
		$this->load->view('product/booknow');
	}

	public function brochure(){	
		$data['title'] = 'โบว์ชัว';
		$this->load->view('product/brochure');
	}

	public function leasing(){	
		$data['title'] = 'ลิซซิ่ง';
		$this->load->view('product/leasing');
	}


	
}
