<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class About extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_about');
	}

	/**
	 * @api {get} /about/all Get all abouts.
	 * @apiVersion 0.1.0
	 * @apiName AllAbout 
	 * @apiGroup about
	 * @apiHeader {String} X-Api-Key Abouts unique access-key.
	 * @apiPermission About Cant be Accessed permission name : api_about_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Abouts.
	 * @apiParam {String} [Field="All Field"] Optional field of Abouts : id, name_thai, name_eng, detail, img_about, created_by, created_at, updated_by, updated_at, status.
	 * @apiParam {String} [Start=0] Optional start index of Abouts.
	 * @apiParam {String} [Limit=10] Optional limit data of Abouts.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of about.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataAbout About data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_about_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng', 'img_about', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status' ,'type'  ,'detail_vision'  ,'detail_vision_eng','detail_more','detail_more2','detail_more3','image_more','name_detail_more','name_detail_more2','name_detail_more3'];
		$abouts = $this->model_api_about->get($filter, $field, null, $start, $select_field);
		$total = $this->model_api_about->count_all($filter, $field);

		$data['about'] = $abouts;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data About',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /about/detail Detail About.
	 * @apiVersion 0.1.0
	 * @apiName DetailAbout
	 * @apiGroup about
	 * @apiHeader {String} X-Api-Key Abouts unique access-key.
	 * @apiPermission About Cant be Accessed permission name : api_about_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Abouts.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of about.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError AboutNotFound About data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */

	public function type_about_post(){
		
		$this->form_validation->set_rules('type', 'type', 'trim|required|max_length[50]');

		if ($this->form_validation->run()) {

			$data = $this->model_api_about->type_about_data($_POST['type']);

			if ($data) {
				
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Detail About',
					'data'	 	=> $data
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'About not found'
				], API::HTTP_NOT_ACCEPTABLE);
			}
		}else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}


	public function detail_get()
	{
		$this->is_allowed('api_about_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng', 'img_about', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status','type'  ,'detail_vision'  ,'detail_vision_eng','detail_more','detail_more2','detail_more3','image_more','name_detail_more','name_detail_more2','name_detail_more3'];
		$data['about'] = $this->model_api_about->find($id, $select_field);

		if ($data['about']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail About',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'About not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /about/add Add About.
	 * @apiVersion 0.1.0
	 * @apiName AddAbout
	 * @apiGroup about
	 * @apiHeader {String} X-Api-Key Abouts unique access-key.
	 * @apiPermission About Cant be Accessed permission name : api_about_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Abouts. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Abouts. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Abouts.  
	 * @apiParam {String} Img_about Mandatory img_about of Abouts. Input Img About Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Abouts. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Abouts.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Abouts. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Abouts.  
	 * @apiParam {String} Status Mandatory status of Abouts. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_about_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_about', 'Img About', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_about' => $this->input->post('img_about'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_about = $this->model_api_about->store($save_data);

			if ($save_about) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /about/update Update About.
	 * @apiVersion 0.1.0
	 * @apiName UpdateAbout
	 * @apiGroup about
	 * @apiHeader {String} X-Api-Key Abouts unique access-key.
	 * @apiPermission About Cant be Accessed permission name : api_about_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Abouts. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Abouts. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Abouts.  
	 * @apiParam {String} Img_about Mandatory img_about of Abouts. Input Img About Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Abouts. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Abouts.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Abouts. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Abouts.  
	 * @apiParam {String} Status Mandatory status of Abouts. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of About.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */

	public function update_action_off_detail_post(){

		$data_array = array(
            'status' => 'off',
        );
        $this->db->where('type = "'.$_POST['type'].'" ')->where('id != "'.$_POST['id'].'"');
       
		$result = $this->db->update('about',$data_array);
		
		if($result == true){
            
			$this->response([
				'status' 	=> true,
				'data' => 'updated data done',
			], API::HTTP_OK);

		}else{
			$this->response([
				'status' 	=> false,
				'data'	 	=> 'updated data failed',
			], API::HTTP_OK);
		}

	}

	public function update_status_post(){

		$this->is_allowed('api_about_update', false);

		
	
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				
				'status' => $this->input->post('status'),
			];
			
			$save_about = $this->model_api_about->change($this->post('id'), $save_data);

			if ($save_about) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}




	public function update_post()
	{
		$this->is_allowed('api_about_update', false);

		
		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_about', 'Img About', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_about' => $this->input->post('img_about'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_about = $this->model_api_about->change($this->post('id'), $save_data);

			if ($save_about) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /about/delete Delete About. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteAbout
	 * @apiGroup about
	 * @apiHeader {String} X-Api-Key Abouts unique access-key.
	 	 * @apiPermission About Cant be Accessed permission name : api_about_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Abouts .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_about_delete', false);

		$about = $this->model_api_about->find($this->post('id'));

		if (!$about) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'About not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_about->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'About deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'About not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file About.php */
/* Location: ./application/controllers/api/About.php */