<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dealer extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function dealer_all(){	
		$data['title'] = 'ผู้จำหน่ายทั้งหมด';
		//$this->load->view('index');
		$this->load->view('dealer/dealer_all');
	}
	
	public function add_dealer(){	
		$data['title'] = 'เพิ่มผู้จำหน่าย';
	
		if(isset($_POST['insert'])){

			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'address_thai' => $this->input->post('address_thai'),
				'address_eng' => $this->input->post('address_eng'),
                //'img_dealer' =>  Rename_file($_FILES['dealer_image']['name']),
				'tel' => $this->input->post('tel'),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);
			/* echo $_FILES['dealer_video']['name'];
		exit(); */
		//$this->load->helper('string');

			$data = $this->db->insert('dealer',$data);

			if($data){
				$data = array('insert' => 'success');
				$this->load->view('dealer/dealer_all',$data);
			}else{
				$this->load->view('dealer/add_dealer_form');
			}

			
				/* $path = array(
					'upload_path' => './assets/images/dealer/',
					'redirect_path1' => 'dealer/add_dealer_form',
					'redirect_path2' => 'dealer/dealer_all',
					'name_image' =>  Rename_file($_FILES['dealer_image']['name']),
					'name_input_image' => 'dealer_image',
				);
				check_upload($path,'insert'); */

		}else{

			$this->load->view('dealer/add_dealer_form');

		}
		
	}

	public function update_dealer(){

		$data['title'] = 'หน้าแก้ไขผู้จำหน่าย';
		
				if(isset($_POST['update'])){
					
					/* if($_FILES['dealer_image']['name'] !== ''){

						$image = Rename_file($_FILES['dealer_image']['name']); 
						echo 'c';

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
						echo 'cold';
					} */
					//exit();

					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
                        //'img_dealer' =>  $image,
						'address_thai' => $this->input->post('address_thai'),
						'address_eng' => $this->input->post('address_eng'),
						'tel' => $this->input->post('tel'),
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						//'status' => 'on',
					);

				
					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('dealer',$data);
					if($data){
						$data = array('update' => 'success');
						$this->load->view('dealer/dealer_all',$data);
					}else{
						$this->load->view('dealer/add_dealer_form');
					}
					/* $path = array(
						'upload_path' => './assets/images/dealer/',
						'redirect_path1' => 'dealer/dealer_all',
						'redirect_path2' => 'dealer/dealer_all',
						'name_image' =>  $image,
						'name_input_image' => 'dealer_image'
					);
					check_upload($path,'update'); */

					
				
					
			}else{
				$this->load->view('dealer/update_dealer_form');
			}
	}
   
}
?>
