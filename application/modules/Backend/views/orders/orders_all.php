<?= $this->load->view('section/header'); ?>
<?php 
//print_r($upload_data);

if(isset($insert) AND $insert == 'success'){
	echo "<script>
							Swal.fire({
								title: 'success!',
								text: 'เพิ่มสั่งซื้อสินค้าสำเร็จ',
								orders: 'success',
						})</script>"; 
 
	redirect('backend/product/orders_all','refresh');
}

?>

  <!-- Page Wrapper -->
  <div id="wrapper">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">รายการ<?= $type_order ?></h1>
         <!--  <p class="mb-4"><a href="<?= base_url('backend/Product/add_orders'); ?>" class="btn btn-primary btn-user btn-block btn_add_kind" >เพิ่มสั่งซื้อสินค้า</a></p> -->
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
						<thead>
							<tr>
								<th>ลำดับ</th>
								<th>ชื่อ</th>
								<th>อีเมล</th>
								<th>เบอร์โทร</th>
								<?php 
								if($type_order !== 'โบรชัวร์'){

									?>
											<th>ที่อยู่</th>
											<th>ผู้แทนจำหน่าย</th>
										<?php
								}
								?>
								
                                <th>วันที่ลงทะเบียน</th>
                                <th>จัดการ</th>
								
							</tr>
						</thead>
						<tfoot>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ</th>
									<th>อีเมล</th>
									<th>เบอร์โทร</th>
									<th>ที่อยู่</th>
									<?php 
								if($type_order !== 'โบรชัวร์'){

									?>
											<th>ที่อยู่</th>
											<th>ผู้แทนจำหน่าย</th>
										<?php
								}
								?>
                                    <th>จัดการ</th>
									
								</tr>
						</tfoot>
   			 </table>

              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" orders="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" orders="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  
  <script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log('ddddd',"<?= $type_order ?>")
		
			$.ajax({
				//cache: true,
				type:'POST',
				async:false,
				url:  api_url+'api/Orders/allorder',
				data: {type_order:"<?= $type_order ?>"},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data)
				
                    var html = '<tbody><tr>';
                    
					 $.each(data.data, function(index,value ) {

                    
						html += '<td>'+parseInt(index + 1)+'</td>';
						html += '<td>'+value.name+'</td>';
						html += '<td>'+value.email+'</td>';
						html += '<td>'+value.tel+'</td>';
						if("<?= $type_order ?>" !== 'โบรชัวร์'){
						html += '<td>'+(value.PROVINCE_NAME+' '+value.AMPHUR_NAME)+'</td>';
												html += '<td>'+value.name_dealer+'</td>';
						}
                        html += '<td>'+value.date_buy+'</td>';
                        html += "<td>";
						html += '<button class="btn btn-danger del" data-id="'+value.id+'"><i class="fa fa-trash" aria-hidden="true"></i>ลบ</button>';
						html += "</td>";
                        html += "</tr>";
                        
						
					}); 
					html += '</tbody>';
                    $('#dataTable').append(html);

                    
                    
                    /* $.ajax({
                            //cache: true,
                            type:'GET',
                            async:false,
                            url:  api_url+'api/Amphur/detail',
                            data: {AMPHUR_ID:explode_address[1]},
                            xhrFields: {
                                withCredentials: false
                            },
                            headers: {
                                'X-Api-Key': apikey,
                            },
                            success: function(data) {

                                //console.log('AMPHUR_NAME',data.data.amphur.AMPHUR_NAME)
                                $('.amp').html(data.data.amphur.AMPHUR_NAME)
                                //console.log('AMPHUR_NAME',data.data[0].AMPHUR_NAME)
                            }
                        }) */
					

					
				}
			}); 

			

			
			delete_data_row('.del','orders',apikey,api_url)

			

		
	});

  </script>
  <?= $this->load->view('section/footer'); ?>
