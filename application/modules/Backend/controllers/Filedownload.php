<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filedownload extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function filedownload_all(){	
		$data['title'] = 'รายการเอกสาร';
		//$this->load->view('index');
		$this->load->view('filedownload/filedownload_all');
	}
	
	public function add_filedownload(){	
		$data['title'] = 'เพิ่มเอกสาร';
	
		if(isset($_POST['insert'])){

			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'file' =>  Rename_file($_FILES['filedownloadpdf']['name']),
				'type' => $this->input->post('type'),
				'created_by' => $this->input->post('created_by'),
				'status' => 'off',
			);
			/* echo $_filedownloadS['filedownload_video']['name'];
		exit(); */

			$data = $this->db->insert('filedownload',$data);

			
			//$this->load->helper('string');

			
				$path = array(
					'upload_path' => './assets/filedownloads/',
					'redirect_path1' => 'filedownload/add_filedownload_form',
					'redirect_path2' => 'filedownload/filedownload_all',
					'name_image' =>  Rename_file($_FILES['filedownloadpdf']['name']),
					'name_input_image' => 'filedownloadpdf',
				);
				check_upload_pdf($path,'insert');

		
		}else{

			$this->load->view('filedownload/add_filedownload_form');

		}
		
	}

	public function update_filedownload(){

		$data['title'] = 'หน้าแก้ไขเอกสาร';
		
				if(isset($_POST['update'])){
					
					if($_FILES['filedownloadpdf']['name'] !== ''){

						$image = Rename_file($_FILES['filedownloadpdf']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม

					}
					//exit();

					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
						'file' =>  Rename_file($_FILES['filedownloadpdf']['name']),
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						
					);
				
					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('filedownload',$data);
					
					$path = array(
						'upload_path' => './assets/filedownloads/',
						'redirect_path1' => 'filedownload/filedownload_all',
						'redirect_path2' => 'filedownload/filedownload_all',
						'name_image' =>   $image,
						'name_input_image' => 'filedownloadpdf',
					);
					check_upload_pdf($path,'update');

					
				
					
			}else{
				$this->load->view('filedownload/update_filedownload_form');
			}
	}
   
}
?>
