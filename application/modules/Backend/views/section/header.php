
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title></title>

<!-- Custom fonts for this template-->
<link href="<?php echo base_url('/assets/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="<?php echo base_url('/assets/css/sb-admin-2.min.css') ?>" rel="stylesheet"><!-- template admin -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> <!-- alert form -->
<link href="<?php echo base_url('/assets/vendor/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css"
      rel="stylesheet"/><!-- switch button -->

<script src="<?php echo base_url('/assets/vendor/jquery/jquery.min.js') ?>"></script>

<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script><!-- editor textarea -->

<!-- datepicker -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- datepicker -->

<link href="<?php echo base_url('/assets/css/upload-video.css') ?>" rel="stylesheet">

<script src="<?php echo base_url('/assets/js/function-custom-header.js') ?>"></script>

<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
 -->
<script src="<?php echo base_url('/assets/js/jquery.multiselect.js') ?>"></script>
<link href="<?php echo base_url('/assets/css/jquery.multiselect.css') ?>" rel="stylesheet">
<style>
.btn_add_kind{
    width:20%;
}
div#wrapper {
    overflow-x: scroll; 
    
}
label.btn.btn-light.toggle-off{
  background-color:#e74a3b;
  color:white;
}
table{
  text-align:center;
}
.button_back_page{
	position:absolute;
	right:73px;
	top:10px;
}.image_preview{
  width:400px;
}label{
  color:black;
}
.ms-options-wrap > .ms-options{
		position: relative;
	}
	.form-control-sub{
		display: block;
    width: 100% !important;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem !important;
    font-size: 1rem !important;
    font-weight: 400;
    line-height: 1.5;
    color: #6e707e;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #d1d3e2;
    border-radius: .35rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
</style>
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <?=  $this->load->view('section/leftmenu'); ?>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Topbar -->
      <?= $this->load->view('section/topmenu'); ?>
      <?php
      //$login = 'remove swal popup';
      if(!$this->session->userdata("session_userid")){


        echo "<script>
        Swal.fire({
          title: 'error!',
          text: 'เข้าสู่ระบบล้มเหลว',
          type: 'error',
        })</script>";
       // $this->load->view('admin/login');
        redirect('backend/Admin/index','refresh');
    
			}
			
			function button_back_page() {

				echo '	<button
				onclick="window.history.back();"
				type="button"
				class="btn btn-secondary"
			>
				<i class="fa fa-arrow-left" aria-hidden="true"></i>ย้อนกลับ
			</button>';
		
      }
      
      
  
      
      ?>
