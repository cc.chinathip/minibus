<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class news extends MX_Controller {

    public function __construct()
	{
		parent::__construct();
		//$this->load->model('Users_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function news(){	
		$data['title'] = 'ข่าวสาร';

		$this->load->view('news/news');
		//$this->load->view('product/car-model-diesel');
		
    }
    
    public function news_detail($news_id=null){	
		$data['title'] = 'รายละเอียดข่าวสาร';
		$data['news_id'] = $news_id;

		//echo $news_id;
		$this->load->view('news/news-detail',$data);
		
    }

    public function news_list(){	
		$data['title'] = 'ข่าวสาร';

		$this->load->view('news/news-list');
		//$this->load->view('product/car-model-diesel');
		
    }
    
    
}
