
<?= $this->load->view('section/header'); ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
			<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">สร้างรหัสเข้าใช้งานระบบหลังบ้าน</h1>
              </div>
              <form class="user" id="registerSubmit">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
				  <label for="sel1">ชื่อ</label>
                    <input type="text" class="form-control form-control-user" id="exampleFirstName" name="name" placeholder="ชื่อ" required>
                  </div>
                  <div class="col-sm-6">
				  <label for="sel1">นามสกุล</label>
                    <input type="text" class="form-control form-control-user" id="exampleLastName" name="surname" placeholder="นามสกุล" required>
                  </div>
                </div>
                <div class="form-group row">
									<div class="col-sm-6 mb-3 mb-sm-0">
									<label for="sel1">ชื่อผู้ใช้</label>
                  	<input type="text" class="form-control form-control-user" id="exampleInputUser" name="username" placeholder="ชื่อผู้ใช้" required >
									</div>
									<div class="col-sm-6">
									<label for="sel1">รหัสผ่าน</label>
                    <input type="password" class="form-control form-control-user" id="exampleInputPassword" name="password" placeholder="รหัสผ่าน" required >
                  </div>
                </div>
              
										<div class="form-group row">
												<div class="col-sm-4 mb-3 mb-sm-0">
														<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
												</div>
												<div class="col-sm-4">
														<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
												</div>
										</div>
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>

	  $(document).ready(function() {

		$('#registerSubmit').on("submit", function(e){ 
		e.preventDefault();
			var apikey =  "<?= API_KEY; ?>"; 
			var api_url = "<?= API_URL; ?>";
			
				$.ajax({
					//cache: true,
					type:'POST',
					async:false,
					url:   api_url+'api/Users/insert_users',
					data: $("#registerSubmit").serialize(),
					xhrFields: {
					withCredentials: false
					},
					headers: {
						'X-Api-Key': apikey,
            
					},
					success: function(data) {
						//console.log('data',data.status)

						if(data.status == true){

							Swal.fire({
								title: 'success!',
								text: 'สร้างรหัสสำเร็จ',
								type: 'success',
							})

							$('.reset_data').click();

							
						}else{
							console.log('Error')
						}
					}
				}); 
		});
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
