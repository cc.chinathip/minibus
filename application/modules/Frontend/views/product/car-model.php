<?= $this->load->view('section/header'); ?>

<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Users/index'); ?>">หน้าหลัก</a></li>
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Product/type/desel'); ?>">รุ่นรถ</a></li>
		    <li class="breadcrumb-item active typecar_title"><span></span></li> 
	  	</ol>
	</div>
</nav>

<div class="section section-banner">
	<div class="swiper-container swiper-banner">
        <div class="swiper-wrapper">
        	<div class="swiper-slide">
        		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1.jpg') ?>" alt="">
        		<img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1-2.png') ?>" alt="">

        		<div class="banner-caption"> 
    				<div class="caption-wrap element2">
    					<div class="inner">
	    					<span class="f1">NEW</span>
	    					<span class="f2">Panthera Motors</span>
	    					<span class="f3">safety for YOUR journey</span>
	    				</div>
    				</div><!--caption-wrap-->
	        		<div class="video-wrap element3">
	        			<div class="container">
		        			<a class="videothumb" data-fancybox href="https://www.youtube.com/watch?v=VkfMKWY1aT0">
		        				<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-500x330--1.jpg') ?>" alt="">
		        			</a>
		        			<a class="videobar" href="#">
		        				<p>
		        					<span class="inner">
		        						แนะนำมินิบัส BONLUCK ทุกซอกมุม<br>
		        						คุ้มค่าเกินราคา
		        					</span>
		        					<span class="arrow">
		        						<span class="arrow-right"></span>
		        					</span>
		        				</p>
		        			</a>
		        		</div>
	        		</div><!--video-wrap--> 
	        	</div><!--banner-caption-->
        	</div><!--swiper-slide-->

        	<div class="swiper-slide">
        		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1.jpg') ?>" alt="">
        		<img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1-2.png') ?>" alt="">

        		<div class="banner-caption"> 
    				<div class="caption-wrap element2">
    					<div class="inner">
	    					<span class="f1">NEW</span>
	    					<span class="f2">Panthera Motors</span>
	    					<span class="f3">safety for YOUR journey</span>
	    				</div>
    				</div><!--caption-wrap-->
	        		<div class="video-wrap element3">
	        			<div class="container">
		        			<a class="videothumb" data-fancybox href="https://www.youtube.com/watch?v=VkfMKWY1aT0">
		        				<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-500x330--1.jpg') ?>" alt="">
		        			</a>
		        			<a class="videobar" href="#">
		        				<p>
		        					<span class="inner">
		        						แนะนำมินิบัส BONLUCK ทุกซอกมุม<br>
		        						คุ้มค่าเกินราคา
		        					</span>
		        					<span class="arrow">
		        						<span class="arrow-right"></span>
		        					</span>
		        				</p>
		        			</a>
		        		</div>
	        		</div><!--video-wrap--> 
	        	</div><!--banner-caption-->
        	</div><!--swiper-slide-->

        </div><!--swiper-wrapper-->

        <div class="swiper-pagination banner"></div>
        <!-- Add Arrows -->
      	<div class="swiper-button swiper-button-next banner"></div>
      	<div class="swiper-button swiper-button-prev banner"></div>

    </div><!--swiper-banner-->
</div><!--section-banner-->

<div class="section section-car-model p-0">
	<div class="page-header wow fadeIn">
		<div class="container">
			<h2 class="title-xl">รุ่นรถ</h2>
		</div>
	</div> 

	<div class="carinfo-box main">
		<div class="carinfo-texture main">
			<img class="item1 wow fadeInLeft" src="<?php echo base_url('/assets/frontend/img/thumb/carinfo-texture--4.png') ?>" alt="">
			<img class="item2 wow fadeInRight" src="<?php echo base_url('/assets/frontend/img/thumb/carinfo-texture--5.png') ?>" alt="">
		</div>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-xl-6 image_car">
					
				</div><!--col-xl-6-->
				<div class="col-xl-6">
					<div class="info-right">
						<div class="row-01 wow fadeIn">
							<div class="inner">
								<p class="text">เลือกรุ่นที่คุณต้องการ<span class="star">*</span></p>
								<div class="buttons">
									<button class="btn btn-blue typecars desale" data-id="รุ่นเครื่องยนต์ดีเซล"><span>เครื่องยนต์ดีเซล</span></button>
									<button class="btn btn-black typecars ngv" data-id="รุ่นเครื่องยนตร์ ngv"><span>เครื่องยนต์NGV</span></button>
								</div>
							</div>
						</div>
						<div class="row-02 wow fadeIn">
							<div class="inner">
								<h2 class="title-xl">Panthera Motors Minibus</h2>
								<h3 class="title-lg typecar_title"></h3>
								<p class="title-lg red car_price"></p>

								<hr>

								<div class="car-color wow fadeIn">
									<h4 class="title-sm">สีรถ</h4>
									<p class="text-color"></p>

									<ul class="color-list">
										<!-- <li class="white active"><span></span></li>
										<li class="gray"><span></span></li>
										<li class="black"><span></span></li> -->
									</ul>
								</div>
							</div>
						</div><!--row-02-->

					</div><!--info-right-->
				</div><!--col-xl-6-->
			</div><!--row-->
		</div><!--container-->
	</div><!--carinfo-box-->

	<div class="carinfo-row exterior">
		<div class="car-model-article wow fadeInRight">
			<h3>Exterior</h3>
			<div class="detail_Exterior"></div>
		</div>
		<div class="row img-group align-items-end wow fadeIn">
			<div class="colunm c1"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-exterior--1.jpg') ?>" alt=""></div><!--colunm-->
			<div class="colunm c2"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-exterior--2.jpg') ?>" alt=""></div><!--colunm-->
			<div class="colunm c3"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-exterior--3.jpg') ?>" alt=""></div><!--colunm-->
			<div class="colunm c4"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-exterior--4.jpg') ?>" alt=""></div><!--colunm--> 
		</div>
		 
	</div><!--exterior-->

	<div class="carinfo-row utility">
		<div class="carinfo-texture utility">
			<img class="item1 wow fadeInLeft" src="<?php echo base_url('/assets/frontend/img/thumb/carinfo-texture--6.png') ?>" alt=""> 
		</div>
		<div class="row img-group align-items-end">
			<div class="colunm c1">
				<div class="car-model-article wow fadeInRight">
					<h3>Utility</h3>
					<!-- <h5>ห้องโดยสารกว้างขวาง<span class="nowrap">สะดวกสบายตลอดการเดินทาง</span></h5>
					<p>เพื่อให้ทุกการเดินทาง<span class="nowrap">เป็นไปได้อย่างราบรื่น</span></p> -->
					<div class="detail_Utility"></div>
				</div>
			</div><!--colunm-->
			<div class="colunm c2"><img class="wow fadeInRight" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/thumb/photo-utility--1.jpg') ?>" alt=""></div><!--colunm-->
			<div class="colunm c3"><img class="wow fadeInRight" data-wow-delay="0.4s" src="<?php echo base_url('/assets/frontend/img/thumb/photo-utility--2.jpg') ?>" alt=""></div><!--colunm--> 
		</div>
	</div><!--utility--> 

	<div class="carinfo-row performance">
		<div class="carinfo-texture performance">
			<img class="item1 wow fadeInRight" src="<?php echo base_url('/assets/frontend/img/thumb/carinfo-texture--7.png') ?>" alt=""> 
		</div>
		<div class="background"><img class="img-car" src="<?php echo base_url('/assets/frontend/img/thumb/performance-texture.jpg') ?>" alt=""></div>
		<div class="row img-group align-items-center">
			<div class="colunm c1">
				<img class="img-car wow fadeInRight" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/thumb/performance-car.png') ?>" alt="">
			</div><!--colunm-->
			<div class="colunm c2">
				<div class="car-model-article wow fadeInRight" data-wow-delay="0.4s">
					<h3>Performance</h3>
					<div class="detail_Performance"></div>
					<!-- <h5>ขุมพลังแรง<br>สะดวกสบายตลอดการเดินทาง</h5>
					<p>กำลังสูงสุด 100 กิโลวัตต์ (136) <span class="nowrap">แรงม้าที่ 3,000 รอบ/นาที</span><br>แรงบิดสูงสุด 353 นิวตัน-เมตรที่ 1,600 รอบ/นาที</p> -->
				</div>
			</div><!--colunm--> 
		</div>
	</div><!--performance--> 

	<div class="carinfo-row safety"> 
		<div class="car-model-article wow fadeIn">
			<h3>Safety</h3>
			<!-- <h5>เหนือกว่าด้วยระบบความปลอดภัยรอบคัน <span class="nowrap">เพิ่มความมั่นใจสูงสุดในทุกเส้นทาง</span></h5>
			<p>เทคโนโลยีมาตรฐานความปลอดภัย ที่มีคุณภาพ</p> -->
			<div class="detail_Safety"></div>
		</div>

		 
			<div class="swiper-container swiper-safety wow fadeIn" data-wow-delay="0.2s">
		        <div class="swiper-wrapper">
		        	<div class="swiper-slide"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-640x420--1.jpg') ?>" alt=""></div><!--swiper-slide--> 
		        	<div class="swiper-slide"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-640x420--2.jpg') ?>" alt=""></div><!--swiper-slide--> 
		        	<div class="swiper-slide"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-640x420--3.jpg') ?>" alt=""></div><!--swiper-slide--> 
		        	<div class="swiper-slide"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-640x420--4.jpg') ?>" alt=""></div><!--swiper-slide--> 
		        	<div class="swiper-slide"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-640x420--5.jpg') ?>" alt=""></div><!--swiper-slide--> 
		        </div><!--swiper-wrapper-->
		 
		        <!-- Add Arrows -->
		      	<div class="swiper-button swiper-button-next safety"></div>
		      	<div class="swiper-button swiper-button-prev safety"></div>

		    </div><!--swiper-banner-->
		 
	</div><!--performance--> 
</div><!--section-car-model-->

<div class="section section-service pt-15 wow fadeIn"> 
	<div class="container">
		<div class="section-header">
			<h2 class="title-lg">บริการของ BONLUCK</h2>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="card card-service">
					<div class="background">
						<div class="b1 wow fadeIn"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--1.jpg') ?>" alt=""></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.2s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--2.png') ?>" alt=""></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.4s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--3.png') ?>" alt=""></div>
						<div class="b4 wow fadeInRight" data-wow-delay="0.7s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--4.png') ?>" alt=""></div>
					</div>
					<div class="hgroup wow fadeIn" data-wow-delay="0.5s">
						<h2>บริการหลังการขาย</h2>
						<p>Minibus</p>
					</div>

					<div class="buttons wow fadeIn" data-wow-delay="0.5s">
						<a class="btn btn-red has-arrow" href="<?= base_url('frontend/Service/service'); ?>">
							<span class="text">รายละเอียด</span>
							<span class="icon"><span class="arrow-right"></span></span>
						</a>
					</div>
				</div><!--card-service-->
			</div><!--col-sm-6-->

			<div class="col-sm-6">
				<div class="card card-service">
					<div class="background">
						<div class="b1 wow fadeIn"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-leasing--1.jpg') ?>" alt=""></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.2s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--2.png') ?>" alt=""></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.4s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--3.png') ?>" alt=""></div>
						<div class="b4 wow fadeInRight" data-wow-delay="0.7s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-leasing--4.png') ?>" alt=""></div>
					</div>
					<div class="hgroup wow fadeIn" data-wow-delay="0.5s">
						<h2>ลิสซิ่งรถใหม่</h2>
						<p>Minibus</p>
					</div>

					<div class="buttons wow fadeIn" data-wow-delay="0.5s">
						<a class="btn btn-red has-arrow" href="<?= base_url('frontend/Product/leasing'); ?>">
							<span class="text">รายละเอียด</span>
							<span class="icon"><span class="arrow-right"></span></span>
						</a>
					</div>
				</div><!--card-service-->
			</div><!--col-sm-6-->
		</div><!--row-->
	</div><!--container-->
</div><!--section-service-->
 
 
<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
	
		pickTypeCar(api_url,apikey)
			

			
		
	});

  </script>
