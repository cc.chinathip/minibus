<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Promotion extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_promotion');
	}

	/**
	 * @api {get} /promotion/all Get all promotions.
	 * @apiVersion 0.1.0
	 * @apiName AllPromotion 
	 * @apiGroup promotion
	 * @apiHeader {String} X-Api-Key Promotions unique access-key.
	 * @apiPermission Promotion Cant be Accessed permission name : api_promotion_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Promotions.
	 * @apiParam {String} [Field="All Field"] Optional field of Promotions : id, name_thai, name_eng, detail, promotion_start, promotion_end, img_promotion, created_at, created_by, updated_at, updated_by, status.
	 * @apiParam {String} [Start=0] Optional start index of Promotions.
	 * @apiParam {String} [Limit=10] Optional limit data of Promotions.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of promotion.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataPromotion Promotion data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		//$this->is_allowed('api_promotion_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','promotion_start', 'promotion_end', 'img_promotion', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];

		if($this->input->get('orderby')){

			$orderby = $this->input->get('orderby');

		}else{

			$orderby = 'DESC';
		}
		

		$promotions = $this->model_api_promotion->get($filter, $field,null, $start, $select_field ,$orderby);
		$total = $this->model_api_promotion->count_all($filter, $field);

		$data['promotion'] = $promotions;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Promotion',
			'data'	 	=> $data,
			'total' 	=> $total,
			'orderby' => $this->input->get('orderby')
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /promotion/detail Detail Promotion.
	 * @apiVersion 0.1.0
	 * @apiName DetailPromotion
	 * @apiGroup promotion
	 * @apiHeader {String} X-Api-Key Promotions unique access-key.
	 * @apiPermission Promotion Cant be Accessed permission name : api_promotion_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Promotions.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of promotion.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError PromotionNotFound Promotion data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_promotion_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','promotion_start', 'promotion_end', 'img_promotion', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$data['promotion'] = $this->model_api_promotion->find($id, $select_field);

		if ($data['promotion']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Promotion',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Promotion not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /promotion/add Add Promotion.
	 * @apiVersion 0.1.0
	 * @apiName AddPromotion
	 * @apiGroup promotion
	 * @apiHeader {String} X-Api-Key Promotions unique access-key.
	 * @apiPermission Promotion Cant be Accessed permission name : api_promotion_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Promotions. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Promotions. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Promotions.  
	 * @apiParam {String} Promotion_start Mandatory promotion_start of Promotions.  
	 * @apiParam {String} Promotion_end Mandatory promotion_end of Promotions.  
	 * @apiParam {String} Img_promotion Mandatory img_promotion of Promotions. Input Img Promotion Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Promotions.  
	 * @apiParam {String} Created_by Mandatory created_by of Promotions. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Promotions.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Promotions. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Promotions. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_promotion_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('promotion_start', 'Promotion Start', 'trim|required');
		$this->form_validation->set_rules('promotion_end', 'Promotion End', 'trim|required');
		$this->form_validation->set_rules('img_promotion', 'Img Promotion', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'promotion_start' => $this->input->post('promotion_start'),
				'promotion_end' => $this->input->post('promotion_end'),
				'img_promotion' => $this->input->post('img_promotion'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'),
				'status' => $this->input->post('status'),
			];
			
			$save_promotion = $this->model_api_promotion->store($save_data);

			if ($save_promotion) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /promotion/update Update Promotion.
	 * @apiVersion 0.1.0
	 * @apiName UpdatePromotion
	 * @apiGroup promotion
	 * @apiHeader {String} X-Api-Key Promotions unique access-key.
	 * @apiPermission Promotion Cant be Accessed permission name : api_promotion_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Promotions. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Promotions. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Promotions.  
	 * @apiParam {String} Promotion_start Mandatory promotion_start of Promotions.  
	 * @apiParam {String} Promotion_end Mandatory promotion_end of Promotions.  
	 * @apiParam {String} Img_promotion Mandatory img_promotion of Promotions. Input Img Promotion Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Promotions.  
	 * @apiParam {String} Created_by Mandatory created_by of Promotions. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Promotions.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Promotions. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Promotions. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Promotion.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_status_post()
	{
		//$this->is_allowed('api_promotion_update', false);

		
	/* 	$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('promotion_start', 'Promotion Start', 'trim|required');
		$this->form_validation->set_rules('promotion_end', 'Promotion End', 'trim|required');
		$this->form_validation->set_rules('img_promotion', 'Img Promotion', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]'); */
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
			/* 	'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'promotion_start' => $this->input->post('promotion_start'),
				'promotion_end' => $this->input->post('promotion_end'),
				'img_promotion' => $this->input->post('img_promotion'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'), */
				'status' => $this->input->post('status'),
			];
			
			$save_promotion = $this->model_api_promotion->change($this->post('id'), $save_data);

			if ($save_promotion) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /promotion/delete Delete Promotion. 
	 * @apiVersion 0.1.0
	 * @apiName DeletePromotion
	 * @apiGroup promotion
	 * @apiHeader {String} X-Api-Key Promotions unique access-key.
	 	 * @apiPermission Promotion Cant be Accessed permission name : api_promotion_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Promotions .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_promotion_delete', false);

		$promotion = $this->model_api_promotion->find($this->post('id'));

		if (!$promotion) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Promotion not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_promotion->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Promotion deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Promotion not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Promotion.php */
/* Location: ./application/controllers/api/Promotion.php */