
<footer class="footer">
	<div class="container wow fadeIn">
		<div class="row">
			<div class="col-xl-4 col-main-left">
				<div class="footer-info">
					<div class="navbar-brand"><img  class="style_logo"   src="<?php echo base_url('/assets/frontend/img/now_logo.png') ?>" alt=""></div>

					<h3>บริษัท แพนเทอรา มอเตอร์ส จำกัด</h3>
					<p>355 ถนนบอนด์สตรีท ตำบลบางพูด อำเภอปากเกร็ด <br>จังหวัดนนทบุรี 11120</p>

					<p>
						โทรศัพท์ : 0 2503 4116 - 21<br>
						โทรสาร  : 0 2503 4400<br>
						เวลาทำการ : 08.30 - 17.00 น. <span class="font-md">(เว้นวันหยุดนักขัตฤกษ์)</span></p>
				</div><!--footer-info-->


			</div><!--col-xl-4-->
			<div class="col-xl-8 col-main-right">
				<div class="footer-links">
					<div class="row">
						<div class="col-xl-6 col-md-3 col-6">
							<h3>รถมินิบัส</h3>
							<ul class="links">
								<li><a href="<?= base_url('frontend/Product/buying'); ?>">เครื่องยนต์รุ่นดีเซล</a></li>
								<li><a href="<?= base_url('frontend/Product/buying'); ?>">เครื่องยนต์รุ่นNGV</a></li>
							</ul>
						</div>

						<div class="col-xl-6 col-md-3 col-6">
							<h3>บริการต่างๆ</h3>
							<ul class="links">
								<li><a href="<?= base_url('frontend/Service/service'); ?>">บริการหลังการขาย</a></li>
								<li><a href="<?= base_url('frontend/Product/leasing'); ?>">ลิสซิ่ง</a></li>
								<li><a href="<?= base_url('frontend/Dealer/service_center'); ?>">ศูนย์บริการ</a></li>
							</ul>
						</div>

						<div class="col-xl-6 col-md-3 col-6">
							<h3>เกี่ยวกับเรา</h3>
							<ul class="links">
								<li><a class="about_us" href="#">ข้อมูลบริษัท</a></li>
								<li><a class="about_policy" href="#">นโยบายและการบริหาร</a></li> 
							</ul>
						</div>

						<div class="col-xl-6 col-md-3 col-6">
							<h3>อัพเดต</h3>
							<ul class="links">
								<li><a href="<?= base_url('frontend/News/news_list'); ?>">ข่าวสาร</a></li>
								<li><a href="<?= base_url('frontend/Activity/activity'); ?>">กิจกรรมต่างๆ</a></li>
								<li><a href="<?= base_url('frontend/Article/article'); ?>">บทความ และ VDO</a></li> 
							</ul>
						</div>
					</div><!--row-->
				</div><!--footer-links-->
				<ul class="shotcut-menu">
					<li>
						<a href="<?= base_url('frontend/Product/buying'); ?>">
							<div class="group wow fadeIn" data-wow-delay="0.15s">
								<div class="icon wow fadeIn" data-wow-delay="0.15s"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-car.svg') ?>" alt=""></div>
								<h4>ซื้อรถ</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?= base_url('frontend/Product/test_drive'); ?>">
							<div class="group wow fadeIn" data-wow-delay="0.15s">
								<div class="icon wow fadeIn" data-wow-delay="0.15s"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-test-drive.svg') ?>" alt=""></div>
								<h4>ทดลองขับ</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?= base_url('frontend/Product/brochure'); ?>">
							<div class="group wow fadeIn" data-wow-delay="0.15s">
								<div class="icon wow fadeIn" data-wow-delay="0.15s"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-brochure.svg') ?>" alt=""></div>
								<h4>โบรชัวร์</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?= base_url('frontend/Product/booking'); ?>">
							<div class="group wow fadeIn" data-wow-delay="0.15s">
								<div class="icon wow fadeIn" data-wow-delay="0.15s"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-booknow.svg') ?>" alt=""></div>
								<h4>จองวันนี้</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?= base_url('frontend/Product/leasing'); ?>">
							<div class="group wow fadeIn" data-wow-delay="0.15s">
								<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-leasing.svg') ?>" alt=""></div>
								<h4>ลิสซิ่ง</h4>
							</div>
						</a>
					</li>
					<li>
						<a href="<?= base_url('frontend/Dealer/service_center'); ?>">
							<div class="group wow fadeIn" data-wow-delay="0.15s">
								<div class="icon"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-service-center.svg') ?>" alt=""></div>
								<h4>ศูนย์บริการ</h4>
							</div>
						</a>
					</li>
				</ul>
			</div><!--col-xl-4-->
		</div><!--row-->
	</div><!--container-->
</footer>
<div class="footer-secondary">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-6">
				Copyright © 2019 ALL RIGHTS RESERVED.
			</div><!--col-6-->
			<div class="col-sm-6">
				<div class="float-rght">
					<ul class="nav nav-followus">
						<li class="nav-item sitemap"><a href="#">Site map</a></li>
						<li class="nav-item"><a href="#" target="_blank"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-facebook.svg') ?>" alt=""></a></li>
						<li class="nav-item"><a href="#" target="_blank"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-lineid.svg') ?>" alt=""></a></li>
						<li class="nav-item"><a href="#" target="_blank"><img class="svg-js" src="<?php echo base_url('/assets/frontend/img/icons/icon-youtube.svg') ?>" alt=""></a></li>
					</ul>
				</div>
			</div><!--col-6-->
		</div><!--row-->
	</div><!--container-->
</div><!--footer-secondary-->  
</div><!--page-->
 


<script src="<?php echo base_url('assets/frontend/js/popper.min.js') ?>"></script>
<script src="<?php echo base_url('assets/frontend/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/frontend/js/wow.js') ?>"></script>   
<script src="<?php echo base_url('assets/frontend/js/swiper.js') ?>"></script>     
<script src="<?php echo base_url('assets/frontend/js/jquery.fancybox.js') ?>"></script>
<script src="<?php echo base_url('assets/frontend/js/custom.js') ?>"></script>  

<script src="<?php echo base_url('assets/js/pagination.js') ?>"></script> 
<link href="<?php echo base_url('assets/css/pagination.css') ?>" rel="stylesheet">

<script type="text/javascript">
	var swiperBanner = new Swiper('.swiper-banner', {
      speed: 1000,
      loop: true,
      observer: true,
      effect: 'slide',
      observeParents: true,
      autoplay: {
        delay: 5500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination.banner',
        clickable: true,
      },
      navigation: {
          nextEl: '.swiper-button-next.banner',
          prevEl: '.swiper-button-prev.banner',
      }, 
    });

	var swiperOffer = new Swiper('.swiper-offer', {
      speed: 1000,
      loop: true,
      observer: true,
      effect: 'slide',
      observeParents: true,
      autoplay: {
        delay: 5500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination.offer',
        clickable: true,
      },
      navigation: {
          nextEl: '.swiper-button-next.offer',
          prevEl: '.swiper-button-prev.offer',
      }, 
    });

	var swiperInnovation = new Swiper('.swiper-innovation', {
      speed: 1000,
      loop: true,
      observer: true,
      effect: 'slide',
      observeParents: true,
      autoplay: {
        delay: 5500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination.innovation',
        clickable: true,
      },
      navigation: {
          nextEl: '.swiper-button-next.innovation',
          prevEl: '.swiper-button-prev.innovation',
      }, 
    });

	$.ajax({
				//cache: true,
				type:'POST',
				async:false,
				url:  '<?= API_URL ?>'+'api/About/type_about',
				data: {type:'ประวัติองค์กร'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': '<?= API_KEY ?>',
				},
				success: function(data) {

					//console.log('data',data.data[0].id)
					$('.about_us').attr('href','<?= base_url('frontend/Aboutus/aboutus/'); ?>'+data.data[0].id)
				
				}
	}); 

	$.ajax({
				//cache: true,
				type:'POST',
				async:false,
				url:  '<?= API_URL ?>'+'api/About/type_about',
				data: {type:'นโยบายและการบริหาร'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': '<?= API_KEY ?>',
				},
				success: function(data) {

					//console.log('data',data.data[0].id)
					$('.about_policy').attr('href','<?= base_url('frontend/Aboutus/aboutus_policy/'); ?>'+data.data[0].id)
				
				}
	}); 
	
</script>  
 
</body>
</html>
