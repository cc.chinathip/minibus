<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class article extends MX_Controller {

    public function __construct()
	{
		parent::__construct();
		//$this->load->model('Users_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function article(){	
		$data['title'] = 'บทความและVDO';

		$this->load->view('article/article');
		//$this->load->view('product/car-model-diesel');
		
    }
    
    public function article_detail($article_id=null){	
		$data['title'] = 'รายละเอียดบทความและVDO';
		$data['article_id'] = $article_id;

		//echo $article_id;
		$this->load->view('article/article-detail',$data);
		
    }

    
    
}
