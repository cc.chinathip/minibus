
<?php $this->load->view('section/header'); ?>

<div class="section section-banner">
	<div class="swiper-container swiper-banner">
        <div class="swiper-wrapper">
        	<div class="swiper-slide">
        		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1.jpg') ?>" alt="">  <!-- background -->
        		<img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x720--1-2.png') ?>" alt=""> <!-- model car -->

        		<div class="banner-caption"> 
    				<div class="caption-wrap element2">
    					<div class="inner">
	    					<span class="f1">NEW</span>
	    					<span class="f2">Panthera Motors</span>
	    					<span class="f3">safety for YOUR journey</span>
	    				</div>
    				</div><!--caption-wrap-->
	        		<div class="video-wrap element3">
	        			<div class="container">
		        			<a class="videothumb" data-fancybox href="https://www.youtube.com/watch?v=VkfMKWY1aT0">
		        				<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-500x330--1.jpg') ?>" alt="">
		        			</a>
		        			<a class="videobar" href="#">
		        				<p>
		        					<span class="inner">
		        						แนะนำมินิบัส BONLUCK ทุกซอกมุม<br>
		        						คุ้มค่าเกินราคา
		        					</span>
		        					<span class="arrow">
		        						<span class="arrow-right"></span>
		        					</span>
		        				</p>
		        			</a>
		        		</div>
	        		</div><!--video-wrap--> 
	        	</div><!--banner-caption-->
        	</div><!--swiper-slide-->

        	

        </div><!--swiper-wrapper-->

        <div class="swiper-pagination banner"></div>
        <!-- Add Arrows -->
      	<div class="swiper-button swiper-button-next banner"></div>
      	<div class="swiper-button swiper-button-prev banner"></div>

    </div><!--swiper-banner-->
</div><!--section-banner-->

<div class="section section-booknow">
	<div class="container">
		<div class="section-header wow fadeIn">
			<h2 class="title-lg">
				“ขับเคลื่อนอย่างคุ้มค่า <br class="d-block d-sm-none">ประหยัด ปลอดภัย”
			</h2>	
		</div>

		<div class="button">
			<a class="btn btn-red has-arrow" href="<?php base_url('frontend/Product/booking'); ?>">
				<span class="text">จองวันนี้รับข้อเสนอพิเศษทันที</span>
				<span class="icon"><span class="arrow-right"></span></span>
			</a>
		</div>
	</div><!--container-->
</div><!--section-booknow-->

<div class="section section-car-info">
	<div class="background wow fadeInRight"  data-wow-delay="0.3s"></div>
	<div class="container">
		<div class="carinfo-box">
			<div class="carinfo-texture inside">
				<div class="texture">
					<img class="item1 wow fadeInRight" src="<?php echo base_url('/assets/frontend/img/thumb/carinfo-texture--1-2.png') ?>" alt="">
					<img class="item2 wow fadeInRight" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/thumb/carinfo-texture--1.png') ?>" alt="">
				</div> 
			</div>
			<div class="row align-items-center">
				<div class="col-xl-6 image_car">
					
				</div><!--col-xl-6-->
				<div class="col-xl-6">
					<div class="info-right wow fadeIn">
						<div class="row-01">
							<div class="inner">
								<p class="text">เลือกรุ่นที่คุณต้องการ*</p>
								<div class="buttons">
									<button class="btn btn-blue typecars desale" data-id="รุ่นเครื่องยนต์ดีเซล"><span>เครื่องยนต์ดีเซล</span></button>
									<button class="btn btn-black typecars ngv" data-id="รุ่นเครื่องยนตร์ ngv"><span>เครื่องยนต์ NGV</span></button>
								</div>
							</div>
						</div>
						<div class="row-02">
							<div class="inner">
								<h2 class="title-xl">Panthera Motors</h2>
								<h3 class="title-lg">Minibus</h3>
								<p class="title-lg red car_price"></p>
							</div>
						</div>
					</div><!--info-right-->
				</div><!--col-xl-6-->
			</div><!--row-->
		</div><!--carinfo-box-->
	</div><!--container-->
</div><!--section-car-info-->

<div class="section section-offer">
	<div class="swiper-container swiper-offer">
        <div class="swiper-wrapper">
        	<div class="swiper-slide">
        		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x520--1.jpg') ?>" alt="">
        		<img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x520--2.png') ?>" alt="">

        		<div class="offer-caption">
        			<div class="inner">
        				<h2 class="element2">
        					ขับเคลื่อนอย่างมั่นใจ ประหยัด
							<span>ปลอดภัย นั่งสบายทุกการเดินทาง</span>
        				</h2>
        				<div class="element3 has-bg">
        					<p>
        						ดอกเบี้ยพิเศษ <span>1.99%</span><br>
        						ฟรี รับประกันคุณภาพนาน 5 ปี
        					</p>
        				</div>
        			</div>
        		</div><!--offer-caption-->
        	</div><!--swiper-slide-->

        	<div class="swiper-slide">
        		<img class="img-bg" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x520--1.jpg') ?>" alt="">
        		<img class="img-overlay element1" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x520--2.png') ?>" alt="">

        		<div class="offer-caption">
        			<div class="inner">
        				<h2 class="element2">
        					ขับเคลื่อนอย่างมั่นใจ ประหยัด
							<span>ปลอดภัย นั่งสบายทุกการเดินทาง</span>
        				</h2>
        				<div class="element3 has-bg">
        					<p>
        						ดอกเบี้ยพิเศษ <span>1.99%</span><br>
        						ฟรี รับประกันคุณภาพนาน 5 ปี
        					</p>
        				</div>
        			</div>
        		</div><!--offer-caption-->
        	</div><!--swiper-slide-->
       	</div><!--swiper-wrapper-->
       	<div class="swiper-pagination offer"></div>
    </div><!--swiper-offer-->

    <div class="offer-texture">
    	<div class="texture item1"></div>
    	<div class="texture item2"></div>
    	<div class="texture item3"></div>
    </div><!--offer-texture-->
</div><!--section-offer-->

<div class="section section-standard pt-15 wow fadeIn">
	<div class="background"></div>
	<div class="container">
		<div class="section-header wow fadeIn">
			<h2 class="title-lg">ออกแบบถูกต้องตามมาตรฐาน<br class="d-block d-lg-none">กรมขนส่งทางบก</h2>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-6">
				<div class="standard-item wow fadeIn">
					<div class="icon">
						<img class="wow zoomIn" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/icons/icon-speed-​​control.png') ?>" alt="">
					</div>
					<h3 class="title-sm">ระบบควบคุมความเร็ว<br>ไม่ให้เกิน 90 กม. / ชม.</h3>
				</div>
			</div><!--col-md-4-->

			<div class="col-md-4 col-sm-6">
				<div class="standard-item wow fadeIn">
					<div class="icon">
						<img class="wow zoomIn" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/icons/icon-gps.png') ?>" alt="">
					</div>
					<h3 class="title-sm">เพิ่มความปลอดภัย<br>ด้วยระบบ GPS</h3>
				</div>
			</div><!--col-md-4-->

			<div class="col-md-4 col-sm-6">
				<div class="standard-item wow fadeIn">
					<div class="icon">
						<img class="wow zoomIn" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/icons/icon-strong-structure.png') ?>" alt="">
					</div>
					<h3 class="title-sm">โครงสร้างแข็งแรง<br>เหมาะกับถนนเมืองไทย</h3>
				</div>
			</div><!--col-md-4-->

			<div class="col-md-4 col-sm-6">
				<div class="standard-item wow fadeIn">
					<div class="icon">
						<img class="wow zoomIn" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/icons/icon-navigation.png') ?>" alt="">
					</div>
					<h3 class="title-sm">ติดตั้งระบบนำทาง</h3>
				</div>
			</div><!--col-md-4-->

			<div class="col-md-4 col-sm-6">
				<div class="standard-item wow fadeIn">
					<div class="icon">
						<img class="wow zoomIn" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/icons/icon-seat-belt.png') ?>" alt="">
					</div>
					<h3 class="title-sm">มีเข็มขัดนิรภัยทุกที่นั่ง</h3>
				</div>
			</div><!--col-md-4-->

			<div class="col-md-4 col-sm-6">
				<div class="standard-item wow fadeIn">
					<div class="icon">
						<img class="wow zoomIn" data-wow-delay="0.2s" src="<?php echo base_url('/assets/frontend/img/icons/icon-abs.png') ?>" alt="">
					</div>
					<h3 class="title-sm">ระบบเบรก ABS ทุกคัน</h3>
				</div>
			</div><!--col-md-4-->
		</div><!--row-->

		<div class="download-wrap wow fadeIn">
			<hr>
			<h5 class="title-sm white">ข้อมูลและข้อบังคับทางกฎหมาย</h5>
			<a class="btn btn-red has-arrow filedownload" href="#">
				<span class="text">ดาวน์โหลด .PDF</span>
				<span class="icon"><span class="icons icon-pdf"></span></span>
			</a>
		</div>
	</div><!--container-->
</div><!--section-standard-->

<div class="section section-highlight pt-15">
	<div class="container">
		<div class="section-header wow fadeIn">
			<h2 class="title-lg">จุดเด่นของ BONLUCK</h2>
		</div>

		<div class="row space-0">
			<div class="col-lg-3 col-sm-6">
				<div class="card card-info">
					<div class="card-photo">
						<div class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-800x520--1.jpg') ?>);">
							<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-100x66--blank.png') ?>" alt="">
						</div>
					</div>

					<div class="card-body">
						<h2 class="title-sm oneline">ความแข็งแรงทนทาน</h2>
						<p class="info mb-0">
							ช่วงล่างนิ่มนวล <br>เหมาะทั้งขับใกล้และไกล
						</p>
					</div><!--card-body-->
				</div><!--card-news-->
			</div><!--col-lg-3 col-sm-6-->

			<div class="col-lg-3 col-sm-6">
				<div class="card card-info">
					<div class="card-photo">
						<div class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-800x520--2.jpg') ?>);">
							<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-100x66--blank.png') ?>" alt="">
						</div>
					</div>

					<div class="card-body">
						<h2 class="title-sm oneline">ประหยัดเชื้อเพลิง</h2>
						<p class="info mb-0">
							ออกแบบมาเพื่อการพาณิชย์ <br>ให้มีอัตราสิ้นเปลืองเชื้อเพลิงต่ำ
						</p>
					</div><!--card-body-->
				</div><!--card-news-->
			</div><!--col-lg-3 col-sm-6-->

			<div class="col-lg-3 col-sm-6">
				<div class="card card-info">
					<div class="card-photo">
						<div class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-800x520--3.jpg') ?>);">
							<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-100x66--blank.png') ?>" alt="">
						</div>
					</div>

					<div class="card-body">
						<h2 class="title-sm oneline">ขับเคลื่อนดีเยี่ยม</h2>
						<p class="info mb-0">
							ขับง่ายออกตัวดี <br>อัตรเร่งดี ตอบสนองการขับขี่
						</p>
					</div><!--card-body-->
				</div><!--card-news-->
			</div><!--col-lg-3 col-sm-6-->

			<div class="col-lg-3 col-sm-6">
				<div class="card card-info">
					<div class="card-photo">
						<div class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-800x520--4.jpg') ?>);">
							<img src="<?php echo base_url('/assets/frontend/img/thumb/photo-100x66--blank.png') ?>" alt="">
						</div>
					</div>

					<div class="card-body">
						<h2 class="title-sm oneline">ศูนย์บริการระดับสากล</h2>
						<p class="info mb-0">
							ให้คำปรึกษาตรวจเช็คสภาพ และ<br>ซ่อมทุกรายการด้วยทีมช่างมืออาชีพ
						</p>
					</div><!--card-body-->
				</div><!--card-news-->
			</div><!--col-lg-3 col-sm-6-->

			<div class="col-lg-12">
				<div class="buttons wow fadeIn">
					<a class="btn btn-red has-arrow" href="<?php base_url('frontend/Product/booking'); ?>">
						<span class="text">จองวันนี้รับข้อเสนอพิเศษทันที</span>
						<span class="icon"><span class="arrow-right"></span></span>
					</a>

					<a class="btn btn-blue has-arrow" href="<?php base_url('frontend/Contact/contact'); ?>">
						<span class="text">ติดต่อ สอบถามข้อมูล</span>
						<span class="icon"><span class="arrow-right"></span></span>
					</a>
				</div>
			</div><!--col-lg-12-->
		</div>
	</div><!--container-->
</div><!--section-highlight-->

<div class="section section-service pt-15 wow fadeIn"> 
	<div class="container">
		<div class="section-header">
			<h2 class="title-lg">บริการของ BONLUCK</h2>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="card card-service">
					<div class="background">
						<div class="b1 wow fadeIn"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--1.jpg') ?>" alt=""></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.2s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--2.png') ?>" alt=""></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.4s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--3.png') ?>" alt=""></div>
						<div class="b4 wow fadeInRight" data-wow-delay="0.7s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--4.png') ?>" alt=""></div>
					</div>
					<div class="hgroup wow fadeIn" data-wow-delay="0.5s">
						<h2>บริการหลังการขาย</h2>
						<p>Minibus</p>
					</div>

					<div class="buttons wow fadeIn" data-wow-delay="0.5s">
						<a class="btn btn-red has-arrow" href="<?= base_url('frontend/Service/service'); ?>">
							<span class="text">รายละเอียด</span>
							<span class="icon"><span class="arrow-right"></span></span>
						</a>
					</div>
				</div><!--card-service-->
			</div><!--col-sm-6-->

			<div class="col-sm-6">
				<div class="card card-service">
					<div class="background">
						<div class="b1 wow fadeIn"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-leasing--1.jpg') ?>" alt=""></div>
						<div class="b2 wow fadeInRight" data-wow-delay="0.2s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--2.png') ?>" alt=""></div>
						<div class="b3 wow fadeInRight" data-wow-delay="0.4s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-service--3.png') ?>" alt=""></div>
						<div class="b4 wow fadeInRight" data-wow-delay="0.7s"><img src="<?php echo base_url('/assets/frontend/img/thumb/photo-leasing--4.png') ?>" alt=""></div>
					</div>
					<div class="hgroup wow fadeIn" data-wow-delay="0.5s">
						<h2>ลิสซิ่งรถใหม่</h2>
						<p>Minibus</p>
					</div>

					<div class="buttons wow fadeIn" data-wow-delay="0.5s">
						<a class="btn btn-red has-arrow" href="<?= base_url('frontend/Product/leasing'); ?>">
							<span class="text">รายละเอียด</span>
							<span class="icon"><span class="arrow-right"></span></span>
						</a>
					</div>
				</div><!--card-service-->
			</div><!--col-sm-6-->
		</div><!--row-->
	</div><!--container-->
</div><!--section-service-->

<div class="section section-innovation p-0 wow fadeIn">
	<div class="swiper-container swiper-innovation">
        <div class="swiper-wrapper">
        	<div class="swiper-slide"> 
          		<img class="img-main" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x830--1.jpg') ?>">
          		<div class="row innovation-caption">
          			<div class="col-5 col-left">
          				<div class="box top">
          					<div class="textbox">
          						<div class="inner element1">
          							<h4>มาตรฐานการบำรุงรักษาระดับ <br class="d-none d-md-block d-xl-none">สากลเท่ากันทุกศูนย์</h4>
          							<p>อะไหล่แท้ ซ่อมไว พร้อมให้คำแนะนำใน การดูแลรักษา ตรวจสภาพรถได้ด้วยตนเอง</p>
          						</div>
          					</div>
          				</div>
          				<div class="box bottom">
          					<span class="background" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-885x500--1.jpg') ?>);"></span>
          				</div>
          			</div><!--col-5-->
          			<div class="col-7 col-right">
          				<div class="box top"></div>
          				<div class="box bottom">
          					<div class="textbox">
          						<div class="inner element2">
          							<h2>นวัตกรรมสุดคุ้มในราคาสบาย</h2>
          							<h4>ทางเดินกว้าง ทุกคันมี GPS</h4>
          							<p>รับส่งข้อมูลด้วยความเร็วสูง เสียงแจ้งเตือนโดยอัตโนมัติภายในห้องโดยสาร
ตัวเครื่องผลิตจากวัสดุอลูมิเนียม แข็งแรง ทนทานต่อการผุกร่อน โดยเสา
สัญญาณขนาดใหญ่ ออกแบบและพัฒนาเพื่อการส่งข้อมูลที่แม่นยำมากขึ้น
ปุ่มกดแจ้งเตือนฉุกเฉิน ในกรณียานพาหนะชำรุด บกพร่อง / เกิดอุบัติเหตุ
ออกแบบให้สวิทซ์เปิด-ปิด อยู่ภายด้านในตัวเครื่อง เพื่อป้องกันการทุจริต</p>
          						</div>
          					</div>
          				</div>
          			</div><!--col-7-->
          		</div>
          	</div><!--swiper-slide-->

          	<div class="swiper-slide"> 
          		<img class="img-main" src="<?php echo base_url('/assets/frontend/img/thumb/photo-1920x830--1.jpg') ?>">
          		<div class="row innovation-caption">
          			<div class="col-5 col-left">
          				<div class="box top">
          					<div class="textbox">
          						<div class="inner element1">
          							<h4>มาตรฐานการบำรุงรักษาระดับ <br class="d-none d-md-block d-xl-none">สากลเท่ากันทุกศูนย์</h4>
          							<p>อะไหล่แท้ ซ่อมไว พร้อมให้คำแนะนำใน การดูแลรักษา ตรวจสภาพรถได้ด้วยตนเอง</p>
          						</div>
          					</div>
          				</div>
          				<div class="box bottom">
          					<span class="background" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/photo-885x500--1.jpg') ?>);"></span>
          				</div>
          			</div><!--col-5-->
          			<div class="col-7 col-right">
          				<div class="box top"></div>
          				<div class="box bottom">
          					<div class="textbox">
          						<div class="inner element2">
          							<h2>นวัตกรรมสุดคุ้มในราคาสบาย</h2>
          							<h4>ทางเดินกว้าง ทุกคันมี GPS</h4>
          							<p>รับส่งข้อมูลด้วยความเร็วสูง เสียงแจ้งเตือนโดยอัตโนมัติภายในห้องโดยสาร
ตัวเครื่องผลิตจากวัสดุอลูมิเนียม แข็งแรง ทนทานต่อการผุกร่อน โดยเสา
สัญญาณขนาดใหญ่ ออกแบบและพัฒนาเพื่อการส่งข้อมูลที่แม่นยำมากขึ้น
ปุ่มกดแจ้งเตือนฉุกเฉิน ในกรณียานพาหนะชำรุด บกพร่อง / เกิดอุบัติเหตุ
ออกแบบให้สวิทซ์เปิด-ปิด อยู่ภายด้านในตัวเครื่อง เพื่อป้องกันการทุจริต</p>
          						</div>
          					</div>
          				</div>
          			</div><!--col-7-->
          		</div>
          	</div><!--swiper-slide-->
        </div><!--swiper-wrapper-->

        <div class="swiper-pagination innovation"></div>
        <!-- Add Arrows -->
      	<div class="swiper-button swiper-button-next innovation"></div>
      	<div class="swiper-button swiper-button-prev innovation"></div>
    </div><!--swiper-innovation-->	 
</div><!--section-innovation-->

<div class="section section-news pt-0 wow fadeIn">
	<div class="container">
		<div class="section-header">
			<h2 class="title-lg">ข่าวสาร</h2>
		</div>

		<div class="row space-0 wow fadeIn show_news">

		</div><!--row-->
	</div><!--container-->
</div><!--section-news-->


<?php $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";

		
		
		//console.log('api_url ',api_url,'apikey ',apikey)
		
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/News/all',
				data: {filter:'on',field:'status'},
				//dataType: "json",
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data',data.data.news)
					//console.log('total',data.total)
					//console.log('datasssss',data.data.news[0].status)


					var html = '';

					//if(data.data.news !== undefined && data.data.news !== null){
					 $.each(data.data.news, function(index,value ) {
						
						//console.log('index',parseInt(index+1))
						
							var number_at = parseInt(index+1);

							html += '<div class="block_news col-lg-4 col-sm-6 '+(number_at > 3 ? 'Img_more' : '') +' " >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/news/') ?>'+value.img_news+');" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+' ">'
									html += '<img class="image_news image_size" src="<?= base_url('/assets/images/news/') ?>'+value.img_news+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-6">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/news/news_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

											html += '<div class="col-6">'
												html += '<div class="share-wrap">'
												html += '<span class="icons icon-share"></span>'
												html += '<span class="text">แชร์ไปยัง : </span>'
												html += '<a class="icons icon-share-facebook" href="#"></a> '
												html += '<a class="icons icon-share-line" href="#"></a>'
												html += '</div>'
											html += '</div>'

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'

							 
						//}
						
						
					}); 
				
					html += '<div class="col-md-12">'
					html += '<div class="buttons pt-4 pb-0 wow fadeIn">'
					html += '	<button class="btn btn-blue btn_more">'
					html += '	<span class="text">แสดงเพิ่มเติม</span>'
					html += '	</button>'
					html += '</div>'
					html += '</div>'

					html += '<div class="col-md-12">'
					html += '<div class="buttons pt-4 pb-0 wow fadeIn">'
					html += '	<button class="btn btn-blue btn_less">'
					html += '	<span class="text">แสดงน้อยลง</span>'
					html += '	</button>'
					html += '</div>'
					html += '</div>'
					
					
					$('.show_news').append(html);
					$('.btn_less').hide()

					if($('.block_news').hasClass('Img_more')){

						$('.Img_more').hide()
					}

					$(".btn_more").click(function() {
						
						$('.Img_more').show()
						$('.btn_more').hide()
						$('.btn_less').show()
					});

					$(".btn_less").click(function() {
						
						$('.Img_more').hide()
						$('.btn_more').show()
						$('.btn_less').hide()
						
					});

					

					
					
				
					
				}
			}); 

			pickTypeCar(api_url,apikey)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Filedownload/all',
				data: {filter:'on',field:'status'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data file download',data.data)

				
					$('.filedownload').attr('href','<?php echo base_url('/assets/filedownloads/') ?>'+data.data.filedownload[0].file)
					
				}
			})

			
		
	});

  </script>
