<?= $this->load->view('section/header'); ?>
<!-- <?= $promotion_id ?> -->
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Users/index'); ?>">หน้าหลัก</a></li>
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Promotion/promotion'); ?>">โปรโมชั่น</a></li>
		    <li class="breadcrumb-item active"><span>ข้อเสนอสุดพิเศษสำหรับแพนเทอร่า มอเตอร์ ใหม่</span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">โปรโมชั่น</h2>
	</div>
</div> 
	<div class="page-header-info wow fadeIn">
	<div class="container">
		<p>
			โปรดตรวจสอบโปรโมชั่นกับผู้จำหน่าย<br>
			บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทราบล่วงหน้า
		</p>
	</div>
</div> 

<div class="section section-banner">
	<div class="background blur background_img_promotion" ></div>
	<div class="container image_promotion_detail">
		
	</div>
</div><!--section-banner-->

<div class="section section-detail main p-0">
	<div class="container">
		<div class="article wow fadeIn">
			<p><div class="tags">โปรโมชั่น</div></p>
			<h2>ข้อเสนอสุดพิเศษสำหรับแพนเทอร่า มอเตอร์ ใหม่</h2>
			<div class="row mt-3 mb-4 align-items-center">
				<div class="col-4">
					<span class="date">25 July 2019</span>
				</div>
				<div class="col-8">
					<div class="share-wrap">
						<span class="icons icon-share"></span>
						<span class="text">แชร์ไปยัง : </span>
						<a class="icons icon-share-facebook" href="#"></a>
						<a class="icons icon-share-line" href="#"></a>
					</div>
				</div>
			</div>
			
			<ol class="detail_promotion">
			
			</ol>
		</div>

		<div class="article-footer">
			<a class="btn btn-red btn-back has-arrow left" href="<?= base_url('/frontend/Promotion/promotion/') ?>">
				<span class="icon"><span class="arrow-left"></span></span>
				<span class="text">ย้อนกลับ</span>
			</a>
		</div>
	</div><!--container-->
</div><!--section-detail-->


<div class="section section-related">
	<div class="background" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/texture-gray.jpg') ?>);"></div>
	<div class="container">
		<h2 class="title-md">โปรโมชั่นอื่นๆ ที่เกี่ยวข้อง</h2>

		<div class="row space-0 wow fadeIn show_promotion">

			
			
			
		</div><!--row-->
	</div><!--container-->
</div><!--section-related-->

<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Promotion/detail',
				data: {id:<?= $promotion_id ?>},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data',data.data.promotion.created_at)
					$('.date').html(data.data.promotion.created_at)
					$('.detail_promotion').html(data.data.promotion.detail)
					$('.image_promotion_detail').html('<img class="w-100 wow fadeIn" data-wow-delay="0.5s" src="<?= base_url('/assets/images/promotion/') ?>'+data.data.promotion.img_promotion+'" alt="">')
					$('.background_img_promotion').css('background-image', 'url(<?= base_url('/assets/images/promotion/') ?>'+data.data.promotion.img_promotion+')');
				}
			});

			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Promotion/all',
				data: {orderby:'RANDOM'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.promotion)
					console.log('total',data.total)
					//console.log('datasssss',data.data.promotion[0].status)
					var html = '';
					 $.each(data.data.promotion, function(index,value ) {
						 var setNumberImg = 6;
						if(index < 4){
							html += '<div class="block_promotion col-lg-3 col-sm-6" >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/promotion/') ?>'+value.img_promotion+');" href="<?= base_url('/frontend/Promotion/promotion_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/promotion/') ?>'+value.img_promotion+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/Promotion/promotion_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-12">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/Promotion/promotion_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'


										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'
						}

							 
						//}
						
						
					}); 
					
					
					$('.show_promotion').append(html);
					
				
					
				}
			}); 
	   });
</script>
