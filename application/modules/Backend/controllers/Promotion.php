<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function promotion_all(){	
		$data['title'] = 'รุ่นรถทั้งหมด';
		//$this->load->view('index');
		$this->load->view('promotion/promotion_all');
	}
	

	public function add_promotion(){	
		$data['title'] = 'เพิ่มรุ่นรถ';
		//$this->load->view('index');
		if(isset($_POST['insert'])){

			/* echo $this->input->post('promotion_start');
			exit(); */
			
			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('promotion_detail'),
				'detail_eng' => $this->input->post('promotion_detail_eng'),
				'promotion_start' => $this->input->post('promotion_start'),
				'promotion_end' => $this->input->post('promotion_end'),
				'img_promotion' =>Rename_file($_FILES['promotion_image']['name']),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);

			$data = $this->db->insert('promotion',$data);

			
			
			$path = array(
				'upload_path' => './assets/images/promotion/',
				'redirect_path1' => 'promotion/add_promotion_form',
				'redirect_path2' => 'promotion/promotion_all',
				'name_image' =>Rename_file($_FILES['promotion_image']['name']),
				'name_input_image' => 'promotion_image'
			);
			check_upload($path,'insert');

			
		}else{

			$this->load->view('promotion/add_promotion_form');

		}
		
	}

	public function update_promotion(){

		$data['title'] = 'หน้าแก้ไขโปรโมชั่น';
		
				if(isset($_POST['update'])){
					
					if($_FILES['promotion_image']['name'] !== ''){

						$image = Rename_file($_FILES['promotion_image']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม
					}
					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
						'detail' => $this->input->post('promotion_detail'),
						'detail_eng' => $this->input->post('promotion_detail_eng'),
						'promotion_start' => $this->input->post('promotion_start'),
						'promotion_end' => $this->input->post('promotion_end'),
						'img_promotion' => $image,
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						//'status' => 'on',
					);

					/* print_r($data);
					exit(); */

					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('promotion',$data);

					
			
					$path = array(
						'upload_path' => './assets/images/promotion/',
						'redirect_path1' => 'promotion/promotion_all',
						'redirect_path2' => 'promotion/promotion_all',
						'name_image' =>  $image,
						'name_input_image' => 'promotion_image'
					);
					check_upload($path,'update');
			
					
			}else{
				$this->load->view('promotion/update_promotion_form');
			}
	}
   
}
?>
