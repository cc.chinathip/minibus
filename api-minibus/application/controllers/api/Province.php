<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Province extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_province');
	}

	/**
	 * @api {get} /province/all Get all provinces.
	 * @apiVersion 0.1.0
	 * @apiName AllProvince 
	 * @apiGroup province
	 * @apiHeader {String} X-Api-Key Provinces unique access-key.
	 * @apiPermission Province Cant be Accessed permission name : api_province_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Provinces.
	 * @apiParam {String} [Field="All Field"] Optional field of Provinces : PROVINCE_ID, PROVINCE_CODE, PROVINCE_NAME, GEO_ID.
	 * @apiParam {String} [Start=0] Optional start index of Provinces.
	 * @apiParam {String} [Limit=10] Optional limit data of Provinces.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of province.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataProvince Province data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_province_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['PROVINCE_ID', 'PROVINCE_CODE', 'PROVINCE_NAME', 'GEO_ID'];
		$provinces = $this->model_api_province->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_province->count_all($filter, $field);

		$data['province'] = $provinces;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Province',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /province/detail Detail Province.
	 * @apiVersion 0.1.0
	 * @apiName DetailProvince
	 * @apiGroup province
	 * @apiHeader {String} X-Api-Key Provinces unique access-key.
	 * @apiPermission Province Cant be Accessed permission name : api_province_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Provinces.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of province.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ProvinceNotFound Province data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_province_detail', false);

		$this->requiredInput(['PROVINCE_ID']);

		$id = $this->get('PROVINCE_ID');

		$select_field = ['PROVINCE_ID', 'PROVINCE_CODE', 'PROVINCE_NAME', 'GEO_ID'];
		$data['province'] = $this->model_api_province->find($id, $select_field);

		if ($data['province']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Province',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Province not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /province/add Add Province.
	 * @apiVersion 0.1.0
	 * @apiName AddProvince
	 * @apiGroup province
	 * @apiHeader {String} X-Api-Key Provinces unique access-key.
	 * @apiPermission Province Cant be Accessed permission name : api_province_add
	 *
 	 * @apiParam {String} PROVINCE_CODE Mandatory PROVINCE_CODE of Provinces. Input PROVINCE CODE Max Length : 2. 
	 * @apiParam {String} PROVINCE_NAME Mandatory PROVINCE_NAME of Provinces. Input PROVINCE NAME Max Length : 150. 
	 * @apiParam {String} GEO_ID Mandatory GEO_ID of Provinces. Input GEO ID Max Length : 5. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_province_add', false);

		$this->form_validation->set_rules('PROVINCE_CODE', 'PROVINCE CODE', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('PROVINCE_NAME', 'PROVINCE NAME', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('GEO_ID', 'GEO ID', 'trim|required|max_length[5]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'PROVINCE_CODE' => $this->input->post('PROVINCE_CODE'),
				'PROVINCE_NAME' => $this->input->post('PROVINCE_NAME'),
				'GEO_ID' => $this->input->post('GEO_ID'),
			];
			
			$save_province = $this->model_api_province->store($save_data);

			if ($save_province) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /province/update Update Province.
	 * @apiVersion 0.1.0
	 * @apiName UpdateProvince
	 * @apiGroup province
	 * @apiHeader {String} X-Api-Key Provinces unique access-key.
	 * @apiPermission Province Cant be Accessed permission name : api_province_update
	 *
	 * @apiParam {String} PROVINCE_CODE Mandatory PROVINCE_CODE of Provinces. Input PROVINCE CODE Max Length : 2. 
	 * @apiParam {String} PROVINCE_NAME Mandatory PROVINCE_NAME of Provinces. Input PROVINCE NAME Max Length : 150. 
	 * @apiParam {String} GEO_ID Mandatory GEO_ID of Provinces. Input GEO ID Max Length : 5. 
	 * @apiParam {Integer} PROVINCE_ID Mandatory PROVINCE_ID of Province.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_province_update', false);

		
		$this->form_validation->set_rules('PROVINCE_CODE', 'PROVINCE CODE', 'trim|required|max_length[2]');
		$this->form_validation->set_rules('PROVINCE_NAME', 'PROVINCE NAME', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('GEO_ID', 'GEO ID', 'trim|required|max_length[5]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'PROVINCE_CODE' => $this->input->post('PROVINCE_CODE'),
				'PROVINCE_NAME' => $this->input->post('PROVINCE_NAME'),
				'GEO_ID' => $this->input->post('GEO_ID'),
			];
			
			$save_province = $this->model_api_province->change($this->post('PROVINCE_ID'), $save_data);

			if ($save_province) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /province/delete Delete Province. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteProvince
	 * @apiGroup province
	 * @apiHeader {String} X-Api-Key Provinces unique access-key.
	 	 * @apiPermission Province Cant be Accessed permission name : api_province_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Provinces .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_province_delete', false);

		$province = $this->model_api_province->find($this->post('PROVINCE_ID'));

		if (!$province) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Province not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_province->remove($this->post('PROVINCE_ID'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Province deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Province not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Province.php */
/* Location: ./application/controllers/api/Province.php */