/* var _CANVAS = document.querySelector("#video-canvas"),
	_CTX = _CANVAS.getContext("2d"), */
_VIDEO = document.querySelector("#main-video");

// Upon click this should should trigger click on the #file-to-upload file input element
// This is better than showing the not-good-looking file input element

/* document.querySelector("#videoInp").addEventListener("click", function() {
	document.querySelector("#file-to-upload").click();
}); */

// When user chooses a MP4 file

$("#file-to-upload").change(function() {
	$("#main-video source").attr(
		"src",
		URL.createObjectURL(document.querySelector("#file-to-upload").files[0])
	);
	$("#video-demo-container-update").css("display", "none");
	//console.log("URL.", URL.createObjectURL(document.querySelector("#file-to-upload").files[0]));
	//console.log("files[0]", $("#file-to-upload").files[0]);

	// Load the video and show it
	_VIDEO.load();
	_VIDEO.style.display = "inline";

	/* document.querySelector("#get-thumbnail").addEventListener('click', function() {
    _CTX.drawImage(_VIDEO, 0, 0, _VIDEO.videoWidth, _VIDEO.videoHeight);

	document.querySelector("#get-thumbnail").setAttribute('href', _CANVAS.toDataURL());
	document.querySelector("#get-thumbnail").setAttribute('download', 'thumbnail.png');
}); */
});
