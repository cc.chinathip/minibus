<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function banner_all(){	
		$data['title'] = 'แบนเนอร์ทั้งหมด';
		//$this->load->view('index');
		$this->load->view('banner/banner_all');
	}
	
	public function add_banner(){	
		$data['title'] = 'เพิ่มแบนเนอร์';
	
		if(isset($_POST['insert'])){

			$data = array(
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
                'img_banner' =>  Rename_file($_FILES['banner_image']['name']),
				'link_img' => $this->input->post('link_img'),
				'position' => $this->input->post('position'),
				'created_by' => $this->input->post('created_by'),
				'status' => 'on',
			);
			/* echo $_FILES['banner_video']['name'];
		exit(); */

			$data = $this->db->insert('banner',$data);

			
			//$this->load->helper('string');

			
				$path = array(
					'upload_path' => './assets/images/banner/',
					'redirect_path1' => 'banner/add_banner_form',
					'redirect_path2' => 'banner/banner_all',
					'name_image' =>  Rename_file($_FILES['banner_image']['name']),
					'name_input_image' => 'banner_image',
				);
				check_upload($path,'insert');

		
		}else{

			$this->load->view('banner/add_banner_form');

		}
		
	}

	public function update_banner(){

		$data['title'] = 'หน้าแก้ไขแบนเนอร์';
		
				if(isset($_POST['update'])){
					
					if($_FILES['banner_image']['name'] !== ''){

						$image = Rename_file($_FILES['banner_image']['name']); 

					}else{
						$image = $this->input->post('old_image'); // กรณีไม่ได้อัปรูปใหม่ใช้รูปเดิม

					}
					//exit();

					$data = array(
						'name_thai' => $this->input->post('name_thai'),
						'name_eng' => $this->input->post('name_eng'),
                        'img_banner' =>  $image,
						'link_img' => $this->input->post('link_img'),
						'position' => $this->input->post('position'),
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => $this->input->post('updated_by'),
						//'status' => 'on',
					);

				
					$this->db->where('id',$this->input->post('id'));
					$data = $this->db->update('banner',$data);
					
					$path = array(
						'upload_path' => './assets/images/banner/',
						'redirect_path1' => 'banner/banner_all',
						'redirect_path2' => 'banner/banner_all',
						'name_image' =>  $image,
						'name_input_image' => 'banner_image'
					);
					check_upload($path,'update');

					
				
					
			}else{
				$this->load->view('banner/update_banner_form');
			}
	}
   
}
?>
