<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Order_detail extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_order_detail');
	}

	/**
	 * @api {get} /order_detail/all Get all order_details.
	 * @apiVersion 0.1.0
	 * @apiName AllOrderdetail 
	 * @apiGroup order_detail
	 * @apiHeader {String} X-Api-Key Order details unique access-key.
	 * @apiPermission Order detail Cant be Accessed permission name : api_order_detail_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Order details.
	 * @apiParam {String} [Field="All Field"] Optional field of Order details : id, order_id, product_id, qty, price, total.
	 * @apiParam {String} [Start=0] Optional start index of Order details.
	 * @apiParam {String} [Limit=10] Optional limit data of Order details.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of order_detail.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataOrder detail Order detail data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_order_detail_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'order_id', 'product_id', 'qty', 'price', 'total'];
		$order_details = $this->model_api_order_detail->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_order_detail->count_all($filter, $field);

		$data['order_detail'] = $order_details;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Order detail',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /order_detail/detail Detail Order detail.
	 * @apiVersion 0.1.0
	 * @apiName DetailOrder detail
	 * @apiGroup order_detail
	 * @apiHeader {String} X-Api-Key Order details unique access-key.
	 * @apiPermission Order detail Cant be Accessed permission name : api_order_detail_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Order details.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of order_detail.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Order detailNotFound Order detail data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_order_detail_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'order_id', 'product_id', 'qty', 'price', 'total'];
		$data['order_detail'] = $this->model_api_order_detail->find($id, $select_field);

		if ($data['order_detail']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Order detail',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Order detail not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /order_detail/add Add Order detail.
	 * @apiVersion 0.1.0
	 * @apiName AddOrder detail
	 * @apiGroup order_detail
	 * @apiHeader {String} X-Api-Key Order details unique access-key.
	 * @apiPermission Order detail Cant be Accessed permission name : api_order_detail_add
	 *
 	 * @apiParam {String} Order_id Mandatory order_id of Order details. Input Order Id Max Length : 10. 
	 * @apiParam {String} Product_id Mandatory product_id of Order details. Input Product Id Max Length : 10. 
	 * @apiParam {String} Qty Mandatory qty of Order details. Input Qty Max Length : 10. 
	 * @apiParam {String} Price Mandatory price of Order details. Input Price Max Length : 10. 
	 * @apiParam {String} Total Mandatory total of Order details. Input Total Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_order_detail_add', false);

		$this->form_validation->set_rules('order_id', 'Order Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('product_id', 'Product Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'order_id' => $this->input->post('order_id'),
				'product_id' => $this->input->post('product_id'),
				'qty' => $this->input->post('qty'),
				'price' => $this->input->post('price'),
				'total' => $this->input->post('total'),
			];
			
			$save_order_detail = $this->model_api_order_detail->store($save_data);

			if ($save_order_detail) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /order_detail/update Update Order detail.
	 * @apiVersion 0.1.0
	 * @apiName UpdateOrder detail
	 * @apiGroup order_detail
	 * @apiHeader {String} X-Api-Key Order details unique access-key.
	 * @apiPermission Order detail Cant be Accessed permission name : api_order_detail_update
	 *
	 * @apiParam {String} Order_id Mandatory order_id of Order details. Input Order Id Max Length : 10. 
	 * @apiParam {String} Product_id Mandatory product_id of Order details. Input Product Id Max Length : 10. 
	 * @apiParam {String} Qty Mandatory qty of Order details. Input Qty Max Length : 10. 
	 * @apiParam {String} Price Mandatory price of Order details. Input Price Max Length : 10. 
	 * @apiParam {String} Total Mandatory total of Order details. Input Total Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Order Detail.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_order_detail_update', false);

		
		$this->form_validation->set_rules('order_id', 'Order Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('product_id', 'Product Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'order_id' => $this->input->post('order_id'),
				'product_id' => $this->input->post('product_id'),
				'qty' => $this->input->post('qty'),
				'price' => $this->input->post('price'),
				'total' => $this->input->post('total'),
			];
			
			$save_order_detail = $this->model_api_order_detail->change($this->post('id'), $save_data);

			if ($save_order_detail) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /order_detail/delete Delete Order detail. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteOrder detail
	 * @apiGroup order_detail
	 * @apiHeader {String} X-Api-Key Order details unique access-key.
	 	 * @apiPermission Order detail Cant be Accessed permission name : api_order_detail_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Order details .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_order_detail_delete', false);

		$order_detail = $this->model_api_order_detail->find($this->post('id'));

		if (!$order_detail) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Order detail not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_order_detail->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Order detail deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Order detail not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Order detail.php */
/* Location: ./application/controllers/api/Order detail.php */