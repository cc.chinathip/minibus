
<?= $this->load->view('section/header'); ?>
  <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">เพิ่มเอกสาร</h1>
              </div>
              <form class="user" id="fileSubmit"  action="<?= base_url('backend/Filedownload/add_filedownload'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อเอกสาร (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อเอกสารภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ชื่อเอกสาร (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อเอกสารภาษาอังกฤษ" >
                  </div>
								</div>
								
								<div class="form-group">
              <label for="sel1">ประเภทเอกสาร</label>
              <select class="form-control" id="type" name="type" required>
              <option value=''>---เลือกประเภทหัวข้อ---</option>
              <option value='ข้อมูลรถ'>ข้อมูลรถ</option>
              <option value='ข้อมูลและข้อบังคับทางกฎหมาย'>ข้อมูลและข้อบังคับทางกฎหมาย</option>
              </select>
            </div>
								

                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="filedownloadpdf" required accept=".pdf" >
                                <label class="custom-file-label" for="inputGroupFile01">เลือกเอกสารนามสกุล pdf</label>
                            </div>
                    </div><p>
                    <div class="name_file"></div>
                   <!--  <div class="input-group show_img">
                    <img class="image_preview" id="target" src="#"/>
                    </div> -->
				</div>
				

								<input type="hidden"  name="created_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="insert" >

								<div class="form-group row">
                  <div class="col-sm-4 mb-3 mb-sm-0">
											<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
                  </div>
                  <div class="col-sm-4">
											<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
                  </div>
                </div>
              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
	

	  $(document).ready(function() {
      
			$("#imgInp").change(function(){
        //preview_image_one(this,'.input-group.show_img');
            preview_image_one(this,'usepdf')
				
      });
      
     
			
		
		
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
