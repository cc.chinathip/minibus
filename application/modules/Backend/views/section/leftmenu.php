<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin<sup>2</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
     <!--  <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('backend/Admin/Dashboard'); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li> -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>


			
			<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>จัดการผู้ใช้งานระบบ</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
						<a class="collapse-item" href="<?= base_url('backend/Admin/data_all'); ?>">ข้อมูลแอดมิน</a>
            <a class="collapse-item" href="<?= base_url('backend/Users/data_all'); ?>">ข้อมูลสมาชิก</a>
          </div>
        </div>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBanner" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>จัดการแบนเนอร์</span>
        </a>
        <div id="collapseBanner" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
						<a class="collapse-item" href="<?= base_url('backend/Banner/banner_all'); ?>">แบนเนอร์</a>
          </div>
        </div>
      </li>
			 -->
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>จัดการข้อมูลสินค้า</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
						<a class="collapse-item" href="<?= base_url('backend/Product/type_all'); ?>">ประเภทสินค้า</a>
						<a class="collapse-item" href="<?= base_url('backend/Product/color_all'); ?>">สีสินค้า</a>
						<a class="collapse-item" href="<?= base_url('backend/Product/product_all'); ?>">สินค้า</a>
          </div>
        </div>
			</li>
			
			<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePromotion" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>จัดการโปรโมชั่น</span>
        </a>
        <div id="collapsePromotion" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
						<a class="collapse-item" href="<?= base_url('backend/Promotion/promotion_all'); ?>">โปรโมชั่น</a>
          </div>
        </div>
      </li>
			

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('backend/Service/service_all'); ?>">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>บริการหลังการขาย</span></a>
			</li>
			
			<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseNews" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>ข่าวสาร</span>
        </a>
        <div id="collapseNews" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
						<a class="collapse-item" href="<?= base_url('backend/News/news_all'); ?>">รายการข่าวสาร</a>
						<a class="collapse-item" href="<?= base_url('backend/Activity/activity_all'); ?>">กิจกรรม</a>
						<a class="collapse-item" href="<?= base_url('backend/Article/article_all'); ?>">บทความ</a>
          </div>
        </div>
      </li>

  

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('backend/About/about_all'); ?>">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>ข้อมูลเกี่ยวกับบริษัท</span></a>
			</li>



      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBuy" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>รายการสั่งซื้อ</span>
        </a>
        <div id="collapseBuy" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?= base_url('backend/Orders/orders_all/1'); ?>">รายการสั่งซื้อ</a>
            <a class="collapse-item" href="<?= base_url('backend/Orders/orders_all/2'); ?>">รายการทดลองขับ</a>
            <a class="collapse-item" href="<?= base_url('backend/Orders/orders_all/3'); ?>">รายการจอง</a>
						<a class="collapse-item" href="<?= base_url('backend/Orders/orders_all/4'); ?>">รายการลิซซิ่ง</a>
						<a class="collapse-item" href="<?= base_url('backend/Orders/orders_all/5'); ?>">รายการดาวโหลดโบว์ชัว</a>
          </div>
        </div>
      </li>


      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('backend/Dealer/dealer_all'); ?>">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>ผู้จำหน่าย</span></a>
			</li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('backend/Filedownload/filedownload_all'); ?>">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>เอกสาร</span></a>
			</li>


  

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
     <!--  <div class="sidebar-heading">
        Addons
      </div> -->

      <!-- Nav Item - Pages Collapse Menu -->
     <!--  <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Login</a>
            <a class="collapse-item" href="register.html">Register</a>
            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="404.html">404 Page</a>
            <a class="collapse-item" href="blank.html">Blank Page</a>
          </div>
        </div>
      </li> -->

      <!-- Nav Item - Charts -->
      <!-- <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li> -->

      <!-- Nav Item - Tables -->
      <!-- <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li> -->

      <!-- Divider -->
     <!--  <hr class="sidebar-divider d-none d-md-block"> -->

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
