
<?= $this->load->view('section/header'); ?>
<?php
/* if($this->session->flashdata('upload_data') == 'success'){
	echo "<script>
							Swal.fire({
								title: 'success!',
								text: 'แก้ไขสินค้าสำเร็จ',
								type: 'success',
						})</script>"; 
 
	redirect('Backend/product/update_product?id='.$this->session->flashdata('id'),'refresh');

}
$updated = 'remove swal popup'; */
?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
			<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขสินค้า</h1>
              </div>
              <form class="user" id="UpdateProductSubmit"  action="<?= base_url('backend/product/update_product'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
				  <label for="sel1">ชื่อสินค้า (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อสินค้าภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
				  <label for="sel1">ชื่อสินค้า  (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อสินค้าภาษาอังกฤษ" >
                  </div>
				</div>

					<div class="form-group">
						<label for="sel1">ประเภทสินค้า</label>
						<select class="form-control" id="typeSelect" name="type_product" required>
						</select>
					</div>

					<div class="form-group">
						<label for="sel1">สีสินค้า</label>
						<select class="form-control" id="colorSelect" name="product_color[]" multiple="multiple" >
						</select>
					</div>
				

					<div class="form-group row">
						<div class="col-sm-6 mb-3 mb-sm-0">
						<label for="sel1">จำนวนสินค้า</label>
							<input type="number" class="form-control form-control-user" id="qty" name="qty"  required min="1"  placeholder="จำนวนสินค้า">
						</div>
						<div class="col-sm-6">
						<label for="sel1">ราคาสินค้า</label>
							<input type="text" class="form-control form-control-user" id="price" name="price" placeholder="ราคา" required>
						</div>
					</div>

                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="product_image"  accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
								</div>
								<!-- <div class="form-group">
								<label for="sel1">รายละเอียดสินค้า (ภาษาไทย)</label>
									<textarea placeholder="รายละเอียดสินค้า..." class="form-control form-control-address" name="product_detail" id="product_detail"></textarea>
								</div>
								
								<div class="form-group">
								<label for="sel1">รายละเอียดสินค้า (ภาษาอังกฤษ)</label>
									<textarea placeholder="รายละเอียดสินค้า..." class="form-control form-control-address" name="product_detail_eng" id="product_detail_eng"></textarea>
								</div> -->

								<div class="form-group">
								<label for="sel1">Exterior</label>
									<textarea placeholder="รายละเอียดข้อมูล..." class="form-control form-control-address" name="Exterior" required id="Exterior"></textarea>
								</div>
								
								<div class="form-group">
								<label for="sel1">Utility</label>
									<textarea placeholder="รายละเอียดข้อมูล..." class="form-control form-control-address" name="Utility" required id="Utility"></textarea>
								</div>
								
								<div class="form-group">
								<label for="sel1">Performance</label>
									<textarea placeholder="รายละเอียดข้อมูล..." class="form-control form-control-address" name="Performance" required id="Performance"></textarea>
								</div>
								
								<div class="form-group">
								<label for="sel1">Safety</label>
									<textarea placeholder="รายละเอียดข้อมูล..." class="form-control form-control-address" name="Safety" required id="Safety"></textarea>
               					 </div>

								<input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="update" >
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
                                <input type="hidden"  id="old_image"  name="old_image" >

								<div class="form-group row">
									<div class="col-sm-4 mb-3 mb-sm-0">
											<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
									</div>
                  					<div class="col-sm-4">
											<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
                  					</div>
                				</div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		/* CKEDITOR.replace("product_detail");
		CKEDITOR.replace("product_detail_eng"); */
		CKEDITOR.replace("Exterior");
		CKEDITOR.replace("Utility");
		CKEDITOR.replace("Performance");
		CKEDITOR.replace("Safety");

	  $(document).ready(function() {
      
			//

    
        $("#imgInp").change(function(){
            preview_image_one(this);

            var dataId = $('#target').data("name");
            console.log('dataId',dataId)
		});

		/* if($('#target').attr('src') == ''){

			console.log('555')

			$("#imgInp").prop('required',true);

		}else{

			console.log('777')
		} */
	
    
	
		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Type/all',
				//data: {type_user:'admin'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.type)
					var opt="<option value='0' selected=\"selected\">---เลือกประเภทสินค้า---</option>";
					 $.each(data.data.type, function(index,value ) {
						 
						opt +="<option value='"+ value.name_thai +"'>"+value.name_thai+"</option>"
						//console.log(value.name_thai)
					}); 

					$("#typeSelect").html( opt );//แก้ไขค่าลงใน Select สินค้า

					
				}
			}); 

			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Color/all',
				//data: {type_user:'admin'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.type)
					var opt = '';
					 $.each(data.data.color, function(index,value ) {
						 
						opt +="<option value='"+ value.id +"'>"+value.name_thai+"</option>"
						//console.log('option',value.name_thai)
					}); 

					$("#colorSelect").html( opt );//เพิ่มค่าลงใน Select สินค้า

	
					
				}
			}); 

            $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Product/detail',
                data:"id="+'<?= $_GET['id'] ?>',
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data',data.data.product)
					/* var explode_color = data.data.product.color_product.split(",");
					console.log('res',explode_color) */

					//console.log('color --',data.data.product.color_product)

                    $('#name_thai').val(data.data.product.name_thai)
                    $('#name_eng').val(data.data.product.name_eng)
                    $('#qty').val(data.data.product.qty)
                    $('#price').val(data.data.product.price)
					$('select#typeSelect').val(data.data.product.type_product);

					/* 
					var explode_color = data.data.product.color_product.split(",");
					console.log('res',explode_color) */
					
					
                    $('#target').attr('src','<?= base_url('/assets/images/product/') ?>'+data.data.product.img_product);
					$('#product_detail').val(data.data.product.detail);
					$('#product_detail_eng').val(data.data.product.detail_eng);
					$('#old_image').val(data.data.product.img_product);
					$('#Exterior').val(data.data.product.Exterior);
					$('#Utility').val(data.data.product.Utility);
					$('#Performance').val(data.data.product.Performance);
					$('#Safety').val(data.data.product.Safety);

					
					color_id = data.data.product.color_product
				}
			});

			$('#colorSelect').multiselect({

				//maxWidth:550,
				selectAll: true,
				placeholder: 'เลือกสีสินค้า',
				minHeight:100,
				
			});
			//console.log('val dropdown',$("input[type='checkbox']").val())

			//console.log('color_id',color_id)
					$.ajax({
							//cache: true,
							type:'POST',
							async:false,
							url:  api_url+'api/Color/NameById',
							data:{color_id:color_id},
							xhrFields: {
								withCredentials: false
							},
							headers: {
								'X-Api-Key': apikey,
							},
							success: function(data) {

								//console.log('NameById',data.data.name_thai)
								var array_color_name = [];
								$.each(data.data, function( index, value ) {

									array_color_name.push(value.name_thai);
									
									var array_color_join = array_color_name.join();

									$('span.text_selected').text(array_color_join)

									//console.log('id===',value.id)
									
									$("input[type='checkbox']").each(function(i){

										
											if($('#ms-opt-'+parseInt(i+1)).val() === value.id){
												
												this.checked = true;
												//$('li').addClass( value.id )
												//console.log('data-search-term',$('li').attr('data-search-term'))
												$('div#ms-list-1').addClass( "ms-has-selections" )
												
											}
									});

									$('select#colorSelect > option').each(function(i) { 

										//console.log('compare',parseInt(i+1))
										if($('#ms-opt-'+parseInt(i+1)).val() === value.id){

											this.selected = true;
										}
										
									})

								

								});
							

							}
				});

			
			

			
			//$('#colorSelect').css('display','block')

			

			
   
	});


	</script>
  <!-- Bootstrap core JavaScript-->
