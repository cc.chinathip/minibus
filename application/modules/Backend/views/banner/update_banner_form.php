
<?= $this->load->view('section/header'); ?>
  <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
         <!--  <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-12">
            <div class="p-5">
						<div class="form-group row button_back_page"><?= button_back_page() ?></div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">แก้ไขแบนเนอร์</h1>
              </div>
              <form class="user" id="bannerSubmit"  action="<?= base_url('backend/banner/update_banner'); ?>" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ชื่อแบนเนอร์ (ภาษาไทย)</label>
                    <input type="text" class="form-control form-control-user" id="name_thai" name="name_thai" placeholder="ชื่อแบนเนอร์ภาษาไทย" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ชื่อแบนเนอร์ (ภาษาอังกฤษ)</label>
                    <input type="text" class="form-control form-control-user" id="name_eng" name="name_eng" placeholder="ชื่อแบนเนอร์ภาษาอังกฤษ" >
                  </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-prepend">
                               <!--  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span> -->
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgInp"
                                aria-describedby="inputGroupFileAddon01" name="banner_image"  accept="image/jpg,image/jpeg,image/png">
                                <label class="custom-file-label" for="inputGroupFile01">เลือกรูปภาพ</label>
                            </div>
                    </div><p>
                    <div class="input-group">
                        <img class="image_preview" id="target" src="#"/>
                    </div>
				</div>
				<div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                  <label for="sel1">ลิงค์แบนเนอร์</label>
                    <input type="text" class="form-control form-control-user" id="link_img" name="link_img" placeholder="ลิงค์แบนเนอร์" required>
                  </div>
                  <div class="col-sm-6">
                  <label for="sel1">ตำแหน่งแบนเนอร์</label>
                    <input type="text" class="form-control form-control-user" id="position" name="position" placeholder="ตำแหน่งแบนเนอร์" >
                  </div>
                </div>

								<input type="hidden"  name="updated_by" value="<?=	$this->session->userdata("session_name_admin") ?>">
								<input type="hidden"  name="update" >
                                <input type="hidden"  name="id" value="<?= $_GET['id'] ?>">
                                <input type="hidden"  id="old_image"  name="old_image" >

								<div class="form-group row">
                  <div class="col-sm-4 mb-3 mb-sm-0">
											<button type="submit" class="btn btn-primary btn-user btn-block" >ยืนยัน</button>
                  </div>
                  <div class="col-sm-4">
											<input type="reset" class="btn btn-danger  btn btn-user btn-block reset_data" value="ล้างข้อมูล">
                  </div>
                </div>
									


              </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <?= $this->load->view('section/footer'); ?>
  <script>
		
	$(document).ready(function() {
      
		
    	$("#imgInp").change(function(){
			preview_image_one(this);
			var dataId = $('#target').data("name");
			console.log('dataId',dataId)
		});

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";

        $.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Banner/detail',
                data:"id="+'<?= $_GET['id'] ?>',
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.banner)
                    $('#name_thai').val(data.data.banner.name_thai)
                    $('#name_eng').val(data.data.banner.name_eng)
                    $('#target').attr('src','<?= base_url('/assets/images/banner/') ?>'+data.data.banner.img_banner);
					$('#link_img').val(data.data.banner.link_img)
                    $('#position').val(data.data.banner.position)
					$('#old_image').val(data.data.banner.img_banner);
                   
				}
		});
		
	});


	</script>
  <!-- Bootstrap core JavaScript-->
