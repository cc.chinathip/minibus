<?= $this->load->view('section/header'); ?>
<!-- <?= $activity_id ?> -->
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Users/index'); ?>">หน้าหลัก</a></li>
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/activity/activity'); ?>">กิจกรรม</a></li>
		    <li class="breadcrumb-item active"><span class="link_now"></span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">กิจกรรม</h2>
	</div>
</div> 
	<div class="page-header-info wow fadeIn">
	<div class="container">
		<p>
        อัพเดตเรื่องราว บทความต่างๆที่น่าสนใจ ความเคลื่อนไหวของเรา
ให้คุณได้ทราบทั้งหมด
		</p>
	</div>
</div> 

<div class="section section-banner">
	<div class="background blur background_img_activity" ></div>
	<div class="container image_activity_detail">
		
	</div>
</div><!--section-banner-->

<div class="section section-detail main p-0">
	<div class="container">
		<div class="activity wow fadeIn">
			<p><div class="tags">กิจกรรม</div></p>
			<h2>ข้อเสนอสุดพิเศษสำหรับแพนเทอร่า มอเตอร์ ใหม่</h2>
			<div class="row mt-3 mb-4 align-items-center">
				<div class="col-4">
					<span class="date">25 July 2019</span>
				</div>
				<div class="col-8">
					<div class="share-wrap">
						<span class="icons icon-share"></span>
						<span class="text">แชร์ไปยัง : </span>
						<a class="icons icon-share-facebook" href="#"></a>
						<a class="icons icon-share-line" href="#"></a>
					</div>
				</div>
			</div>
		
			<ol class="detail_activity">
				
			</ol>
		</div>

		<div class="activity-footer">
			<button class="btn btn-red btn-back has-arrow left" onclick="goBack()">
				<span class="icon"><span class="arrow-left"></span></span>
				<span class="text">ย้อนกลับ</span>
            </button>
		</div>
	</div><!--container-->
</div><!--section-detail-->


<div class="section section-related">
	<div class="background" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/texture-gray.jpg') ?>);"></div>
	<div class="container">
		<h2 class="title-md">กิจกรรมอื่นๆ ที่เกี่ยวข้อง</h2>

		<div class="row space-0 wow fadeIn show_activity">

			
			
			
		</div><!--row-->
	</div><!--container-->
</div><!--section-related-->

<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Activity/detail',
				data: {id:<?= $activity_id ?>},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data',data.data.activity.created_at)
					$('.date').html(data.data.activity.created_at)
					$('.detail_activity').html(data.data.activity.detail)
					$('.image_activity_detail').html('<img class="w-100 wow fadeIn" data-wow-delay="0.5s" src="<?= base_url('/assets/images/activity/') ?>'+data.data.activity.img_activity+'" alt="">')
					$('.background_img_activity').css('background-image', 'url(<?= base_url('/assets/images/activity/') ?>'+data.data.activity.img_activity+')');
					$('.link_now').html(data.data.activity.name_thai)
					
				}
			});

			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Activity/all',
				data: {orderby:'RANDOM'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.activity)
					console.log('total',data.total)
					//console.log('datasssss',data.data.activity[0].status)
					var html = '';
					 $.each(data.data.activity, function(index,value ) {
						 var setNumberImg = 6;
						if(index < 4){
							html += '<div class="block_activity col-lg-3 col-sm-6" >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/activity/') ?>'+value.img_activity+');" href="<?= base_url('/frontend/activity/activity_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/activity/') ?>'+value.img_activity+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/activity/activity_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-12">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/activity/activity_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'


										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'
						}

							 
						//}
						
						
					}); 
					
					
					$('.show_activity').append(html);
					
				
					
				}
			}); 
	   });
</script>
