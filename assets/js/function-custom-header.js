/* function allowNumbersOnly(e) {
	var code = e.which ? e.which : e.keyCode;
	if (code > 31 && (code < 48 || code > 57)) {
		e.preventDefault();
	}
}
 */

function getDealler(api_url, apikey) {
	$.ajax({
		//cache: true,
		type: "GET",
		async: false,
		url: api_url + "api/Dealer/all",
		data: { filter: "on", field: "status" },
		xhrFields: {
			withCredentials: false
		},
		headers: {
			"X-Api-Key": apikey
		},
		success: function(data) {
			console.log("dealler all", data.data);

			var opt =
				"<option value='' selected=\"selected\">--เลือกผู้แทนจำหน่าย--</option>";
			$.each(data.data.dealer, function(index, value) {
				opt +=
					"<option value='" + value.id + "'>" + value.name_thai + "</option>";
				//console.log(value.name_thai)
			});

			$("#dealerSelect").html(opt); //เพิ่มค่าลงใน Select สินค้า
		}
	});
}
function add_order(api_url, apikey, type_orders) {
	$(".regis").click(function() {
		var province_id = $("#province").val();
		var amphure_id = $("#amphur").val();
		var name = $("#name").val();
		var email = $("#email").val();
		var tel = $("#tel").val();
		var dealer_id = $("#dealerSelect").val();
		var product_id = $(".typecar_title").data("id");
		var price = $(".car_price").data("price");

		console.log("ddd", name, " ", email, " ", tel, " ", product_id, " ", price);

		$.ajax({
			//cache: true,
			type: "POST",
			async: false,
			url: api_url + "api/Orders/add",
			data: {
				name: name,
				email: email,
				tel: tel,
				province_id: province_id,
				amphure_id: amphure_id,
				dealer_id: dealer_id,
				type_orders: type_orders,
				product_id: product_id,
				price: price
			},
			xhrFields: {
				withCredentials: false
			},
			headers: {
				"X-Api-Key": apikey
			},
			success: function(data) {
				console.log("data", data);

				if (data.status === true) {
					Swal.fire({
						title: "success!",
						text: "ลงทะเบียนสำเร็จ",
						type: "success"
					});

					setTimeout(function() {
						location.reload();
					}, 2000);
				} else {
					Swal.fire({
						title: "error!",
						text: "ลงทะเบียนสำเร็จ",
						type: "success"
					});
				}
			}
		});
	});
}
function goBack() {
	window.history.back();
}

function switch_button(data, controller, api_url, apikey) {
	$.each(data, function(index, value) {
		if (value.status == "off") {
			//console.log('status= on')
			$("input#togBtn_" + parseInt(index + 1) + "").prop("checked", false);
		}
		var switchStatus = false;
		$("#togBtn_" + parseInt(index + 1)).on("change", function() {
			if ($(this).is(":checked")) {
				switchStatus = $(this).is(":checked");
				$.ajax({
					//cache: true,
					type: "POST",
					async: false,
					url: api_url + "api/" + controller + "/update_status",
					data: { id: value.id, status: "on" },
					xhrFields: {
						withCredentials: false
					},
					headers: {
						"X-Api-Key": apikey
					},
					success: function(data) {
						//console.log("updated", data);

						if (data.status === true) {
							Swal.fire({
								title: "success!",
								text: "เปิดใช้งาน",
								type: "success"
							});
						}
					}
				});
				//console.log('tid= ',parseInt(index + 1),switchStatus);
			} else {
				switchStatus = $(this).is(":checked");
				$.ajax({
					//cache: true,
					type: "POST",
					async: false,
					url: api_url + "api/" + controller + "/update_status",
					data: { id: value.id, status: "off" },
					xhrFields: {
						withCredentials: false
					},
					headers: {
						"X-Api-Key": apikey
					},
					success: function(data) {
						//console.log("updated", data);

						if (data.status === true) {
							Swal.fire({
								title: "error!",
								text: "ปิดใช้งาน",
								type: "error"
							});
						}
					}
				});
				//console.log('fid= ',parseInt(index + 1),switchStatus);
			}
		});
	});
}

function switch_button_one(data, controller, api_url, apikey, type = null) {
	$.each(data, function(index, value) {
		if (value.status == "off") {
			//console.log('status= on')
			$("input#togBtn_" + parseInt(index + 1) + "").prop("checked", false);
		}
		var switchStatus = false;
		$("#togBtn_" + parseInt(index + 1)).on("change", function() {
			if ($(this).is(":checked")) {
				switchStatus = $(this).is(":checked");
				$.ajax({
					//cache: true,
					type: "POST",
					async: false,
					url: api_url + "api/" + controller + "/update_status",
					data: { id: value.id, status: "on" },
					xhrFields: {
						withCredentials: false
					},
					headers: {
						"X-Api-Key": apikey
					},
					success: function(data) {
						//console.log("updated", data);

						if (data.status === true) {
							Swal.fire({
								title: "success!",
								text: "เปิดใช้งาน",
								type: "success"
							});
							if (type !== null) {
								var type_field = value.type_product;
								//console.log("type_field1111");
							} else {
								var type_field = value.type;
								//console.log("type_field2222");
							}
							$.ajax({
								//cache: true,
								type: "POST",
								async: false,
								url:
									api_url + "api/" + controller + "/update_action_off_detail",
								data: { type: type_field, id: value.id },
								xhrFields: {
									withCredentials: false
								},
								headers: {
									"X-Api-Key": apikey
								},
								success: function(data) {
									//console.log("switch_button_one", data);
									if (data.status === true) {
										setTimeout(function() {
											location.reload();
										}, 2000);
									}
								}
							});
						}
					}
				});
				//console.log('tid= ',parseInt(index + 1),switchStatus);
			} else {
				switchStatus = $(this).is(":checked");
				$.ajax({
					//cache: true,
					type: "POST",
					async: false,
					url: api_url + "api/" + controller + "/update_status",
					data: { id: value.id, status: "off" },
					xhrFields: {
						withCredentials: false
					},
					headers: {
						"X-Api-Key": apikey
					},
					success: function(data) {
						//console.log("updated", data);

						if (data.status === true) {
							Swal.fire({
								title: "error!",
								text: "ปิดใช้งาน",
								type: "error"
							});
						}
					}
				});
				//console.log('fid= ',parseInt(index + 1),switchStatus);
			}
		});
	});
}
