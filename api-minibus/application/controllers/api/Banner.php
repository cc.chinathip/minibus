<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Banner extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_banner');
	}

	/**
	 * @api {get} /banner/all Get all banners.
	 * @apiVersion 0.1.0
	 * @apiName AllBanner 
	 * @apiGroup banner
	 * @apiHeader {String} X-Api-Key Banners unique access-key.
	 * @apiPermission Banner Cant be Accessed permission name : api_banner_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Banners.
	 * @apiParam {String} [Field="All Field"] Optional field of Banners : id, name_thai, name_eng, img_banner, link_img, position, created_by, created_at, updated_by, updated_at, status.
	 * @apiParam {String} [Start=0] Optional start index of Banners.
	 * @apiParam {String} [Limit=10] Optional limit data of Banners.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of banner.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataBanner Banner data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_banner_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'img_banner', 'link_img', 'position', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$banners = $this->model_api_banner->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_banner->count_all($filter, $field);

		$data['banner'] = $banners;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Banner',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /banner/detail Detail Banner.
	 * @apiVersion 0.1.0
	 * @apiName DetailBanner
	 * @apiGroup banner
	 * @apiHeader {String} X-Api-Key Banners unique access-key.
	 * @apiPermission Banner Cant be Accessed permission name : api_banner_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Banners.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of banner.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError BannerNotFound Banner data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_banner_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'img_banner', 'link_img', 'position', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$data['banner'] = $this->model_api_banner->find($id, $select_field);

		if ($data['banner']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Banner',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Banner not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /banner/add Add Banner.
	 * @apiVersion 0.1.0
	 * @apiName AddBanner
	 * @apiGroup banner
	 * @apiHeader {String} X-Api-Key Banners unique access-key.
	 * @apiPermission Banner Cant be Accessed permission name : api_banner_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Banners. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Banners. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Img_banner Mandatory img_banner of Banners. Input Img Banner Max Length : 255. 
	 * @apiParam {String} Link_img Mandatory link_img of Banners. Input Link Img Max Length : 50. 
	 * @apiParam {String} Position Mandatory position of Banners. Input Position Max Length : 50. 
	 * @apiParam {String} Created_by Mandatory created_by of Banners. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Banners.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Banners. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Banners.  
	 * @apiParam {String} Status Mandatory status of Banners. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_banner_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('img_banner', 'Img Banner', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('link_img', 'Link Img', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('position', 'Position', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'img_banner' => $this->input->post('img_banner'),
				'link_img' => $this->input->post('link_img'),
				'position' => $this->input->post('position'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_banner = $this->model_api_banner->store($save_data);

			if ($save_banner) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /banner/update Update Banner.
	 * @apiVersion 0.1.0
	 * @apiName UpdateBanner
	 * @apiGroup banner
	 * @apiHeader {String} X-Api-Key Banners unique access-key.
	 * @apiPermission Banner Cant be Accessed permission name : api_banner_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Banners. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Banners. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Img_banner Mandatory img_banner of Banners. Input Img Banner Max Length : 255. 
	 * @apiParam {String} Link_img Mandatory link_img of Banners. Input Link Img Max Length : 50. 
	 * @apiParam {String} Position Mandatory position of Banners. Input Position Max Length : 50. 
	 * @apiParam {String} Created_by Mandatory created_by of Banners. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Banners.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Banners. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Banners.  
	 * @apiParam {String} Status Mandatory status of Banners. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Banner.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_status_post()
	{
		$this->is_allowed('api_banner_update', false);

		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				
				'status' => $this->input->post('status'),
			];
			
			$save_banner = $this->model_api_banner->change($this->post('id'), $save_data);

			if ($save_banner) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}


	public function update_post()
	{
		$this->is_allowed('api_banner_update', false);

		
		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('img_banner', 'Img Banner', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('link_img', 'Link Img', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('position', 'Position', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'img_banner' => $this->input->post('img_banner'),
				'link_img' => $this->input->post('link_img'),
				'position' => $this->input->post('position'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_banner = $this->model_api_banner->change($this->post('id'), $save_data);

			if ($save_banner) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /banner/delete Delete Banner. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteBanner
	 * @apiGroup banner
	 * @apiHeader {String} X-Api-Key Banners unique access-key.
	 	 * @apiPermission Banner Cant be Accessed permission name : api_banner_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Banners .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_banner_delete', false);

		$banner = $this->model_api_banner->find($this->post('id'));

		if (!$banner) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Banner not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_banner->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Banner deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Banner not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Banner.php */
/* Location: ./application/controllers/api/Banner.php */