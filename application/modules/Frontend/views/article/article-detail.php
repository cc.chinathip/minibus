<?= $this->load->view('section/header'); ?>
<!-- <?= $article_id ?> -->
<nav class="section section-breadcrumb">
	<div class="container">
	  	<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/Users/index'); ?>">หน้าหลัก</a></li>
		    <li class="breadcrumb-item"><a href="<?= base_url('frontend/article/article'); ?>">บทความและVDO</a></li>
		    <li class="breadcrumb-item active"><span class="link_now"></span></li> 
	  	</ol>
	</div>
</nav>
<div class="page-header wow fadeIn">
	<div class="container">
		<h2 class="title-xl">บทความและVDO</h2>
	</div>
</div> 
	<div class="page-header-info wow fadeIn">
	<div class="container">
		<p>
        อัพเดตเรื่องราว บทความต่างๆที่น่าสนใจ ความเคลื่อนไหวของเรา
ให้คุณได้ทราบทั้งหมด
		</p>
	</div>
</div> 

<div class="section section-banner">
	<div class="background blur background_img_article" ></div>
	<div class="container image_article_detail">
		
	</div>
</div><!--section-banner-->

<div class="section section-detail main p-0">
	<div class="container">
		<div class="article wow fadeIn">
			<p><div class="tags">บทความและVDO</div></p>
			<h2>ข้อเสนอสุดพิเศษสำหรับแพนเทอร่า มอเตอร์ ใหม่</h2>
			<div class="row mt-3 mb-4 align-items-center">
				<div class="col-4">
					<span class="date">25 July 2019</span>
				</div>
				<div class="col-8">
					<div class="share-wrap">
						<span class="icons icon-share"></span>
						<span class="text">แชร์ไปยัง : </span>
						<a class="icons icon-share-facebook" href="#"></a>
						<a class="icons icon-share-line" href="#"></a>
					</div>
				</div>
			</div>

			
			<ol class="detail_article">
				
			</ol>
		</div>

		<div class="article-footer">
			<button class="btn btn-red btn-back has-arrow left" onclick="goBack()">
				<span class="icon"><span class="arrow-left"></span></span>
				<span class="text">ย้อนกลับ</span>
            </button>
		</div>
	</div><!--container-->
</div><!--section-detail-->


<div class="section section-related">
	<div class="background" style="background-image: url(<?php echo base_url('/assets/frontend/img/thumb/texture-gray.jpg') ?>);"></div>
	<div class="container">
		<h2 class="title-md">บทความและVDOอื่นๆ ที่เกี่ยวข้อง</h2>

		<div class="row space-0 wow fadeIn show_article">

			
			
			
		</div><!--row-->
	</div><!--container-->
</div><!--section-related-->

<?= $this->load->view('section/footer'); ?>
<script>
	   $(document).ready(function() {

		var apikey =  "<?= API_KEY; ?>"; 
		var api_url = "<?= API_URL; ?>";
		
		console.log(api_url)
		
			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/article/detail',
				data: {id:<?= $article_id ?>},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					//console.log('data',data.data.article.created_at)
					$('.date').html(data.data.article.created_at)
					$('.detail_article').html(data.data.article.detail)
					$('.image_article_detail').html('<img class="w-100 wow fadeIn video" data-wow-delay="0.5s" src="<?= base_url('/assets/images/article/') ?>'+data.data.article.img_article+'" alt="">')
					$('.background_img_article').css('background-image', 'url(<?= base_url('/assets/images/article/') ?>'+data.data.article.img_article+')');
					$('.link_now').html(data.data.article.name_thai)
				}
			});

			$.ajax({
				//cache: true,
				type:'GET',
				async:false,
				url:  api_url+'api/Article/all',
				data: {orderby:'RANDOM'},
				xhrFields: {
					withCredentials: false
				},
				headers: {
					'X-Api-Key': apikey,
				},
				success: function(data) {
					console.log('data',data.data.article)
					console.log('total',data.total)
					//console.log('datasssss',data.data.article[0].status)
					var html = '';
					 $.each(data.data.article, function(index,value ) {
						 var setNumberImg = 6;
						if(index < 4){
							html += '<div class="block_article col-lg-3 col-sm-6" >'
						 	html += '<div class="card card-info">'

								html += '<div class="card-photo">'
									html += '<a class="photo wow fadeIn" data-wow-delay="0.15s" style="background-image: url(<?= base_url('/assets/images/article/') ?>'+value.img_article+');" href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+' ">'
									html += '<img class="image_size" src="<?= base_url('/assets/images/article/') ?>'+value.img_article+'" alt="">'
									html += '</a>'
									html += '</div>'

									html += '<div class="card-body">'
									
										html += '<h2 class="title-sm"><a href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+'">'+value.name_thai+'</a></h2>'
										html += '<p class="date">'+value.created_at+'</p>'

										html += '<div class="row align-items-center">'

											html += '<div class="col-12">'
												html += '<a class="btn btn-red has-arrow" href="<?= base_url('/frontend/article/article_detail/') ?>'+value.id+'">'
												html += '<span class="text">รายละเอียด</span>'
												html += '<span class="icon">'
													html += '<span class="arrow-right"></span>'
												html += '</span>'
												html += '</a>'
											html += '</div>'

											

										html += '</div>'

									html += '</div>'
								html += '</div>'

						 	html += '</div>'
							 html += '</div>'
						}

							 
						//}
						
						
					}); 
					
					
					$('.show_article').append(html);
					
				
					
				}
			}); 
	   });
</script>
